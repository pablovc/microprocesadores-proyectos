

/*
 * Programa que implementa la lectura de un Encoder Rotativo
 * para tener control sobre alguna variable con requerimientos de
 * incremento/decremento (por ejemplo Velocidad, Vol�men de audio
 * etc.)
 * La deteccion del cambio de estado se realiza detectando por Interrupci�n
 * ya sea cambio de nivel o detectando ambos flancos.
 *
 * En la ISR de la interrupci�n se detecta el sentido de Giro del Encoder
 * Si es en sentido de las manecillas, se incrementa la variable que se quiere controlar
 * Si es sentido contrario, se decrementa.
 * Otra variable se considera para contar el numero de veces que la ISR se ha ejecutado
 *
 */

#include "tm4c1294ncpdt.h"

unsigned char g_ucValEncoder;
void ISR_GPIO_Encoder(void);
unsigned char g_last;     /* estado anterior del encoder*/

void main(void)
{

    /*
     * Habilitar reloj para GPIOs
     * puerto A y K
     */
    SYSCTL_RCGCGPIO_R = ;
    while(!(SYSCTL_PRGPIO_R & ));    // esperar a que los relojes sean estables

    /* Puerto para entrada de  Encoder PA1,PA0 */
    GPIO_PORTA_DIR_R = ;
    GPIO_PORTA_DEN_R = ;
    GPIO_PORTA_IM_R = ;
    GPIO_PORTA_IS_R = ;
    GPIO_PORTA_IBE_R = ;
    g_last = GPIO_PORTA_DATA_R &  ;   // obtiene valor solo de los bits A1,A0

    /* Puerto de despliegue del valor en LEDS, 8 bits del PTO K*/
    GPIO_PORTK_DIR_R = ;
    GPIO_PORTK_DEN_R = ;
    GPIO_PORTK_DATA_R = ;

    /* Habilitar en NVIC la Interrupcion PTOA */
    NVIC_EN0_R = ;
    NVIC_PRI0_R = ;

    g_ucValEncoder = 0;   //valor inicial del encoder

    while(1){
        GPIO_PORTK_DATA_R = g_ucValEncoder;     // actualiza estado en LEDS asignando valor de variable global
    }


}

void ISR_GPIO_Encoder(void)
{
    unsigned char current;  /* estado actual del encoder*/
//    unsigned int counter;

    current = GPIO_PORTA_DATA_R & 0x03;     // 
    if((g_last & )^(current>>1))
    {
        if(g_ucValEncoder<)
            g_ucValEncoder ++; /* C.W., Incrementar variable*/
        else
            g_ucValEncoder = ;
    }
    else
    {
        if(g_ucValEncoder>)
            g_ucValEncoder--; /* C.C.W., Decrementar variable*/
        else
            g_ucValEncoder = ;
    }
    g_last = current;

    GPIO_PORTA_ICR_R = ;

}




















