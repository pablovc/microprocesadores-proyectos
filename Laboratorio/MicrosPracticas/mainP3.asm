
;--------------------------------------------------------------------------------------------------
        .data            ; valores en RAM

                         ; datos inicializados
count	.word  0xFF2233  ; dato de 32 bits
i       .byte  0x10      ; dato de 1 byte
        .byte  0x20      ; dato de 1 byte
        .byte  0x30      ; dato de 1 byte
        .byte  0xAA      ; dato de 1 byte
j	.word  0x80
msg     .string  "hola mundo" ; cadena de texto
;---------------------------------------------------------------------------------------------------
                         ; variables no inicializadas - .bss s�mbolo, tama�o en bytes
        .bss M, 4        ; M - 4 bytes reservados en RAM
;---------------------------------------------------------------------------------------------------
	.global main     ;
        .text            ; programa principal
ptM	.field  M,32     ; ptM = direcci�n de M (32 bits)
ptC     .field  count,32
ptStr   .field  msg,32   ; ptStr = direccion de inicio de msg (32 bits)

main:

    	MOVW R0, #0x1289  ; movw solo permite 16 bits
	MOVT  R1, #0x1211
	MOV   R1, #0X1239
	;MOV   R1, #0x12121212


	;LDR   R8,
	LDR   R4, ptM     ; R4 = direccion de M (a donde apunta ptM)
	LDR   R3, ptC
	LDR   R2, [R3]    ; R2 = contenido donde apunta R3
	STR   R2, [R4]    ; guarda R2 en direccion apuntada por R4
	LDRH  R5, [R3]    ; carga l6 bits en parte baja de R5
        MOV  R6, #10100001b ; carga usando formato binario

stop	B   stop
        .end
