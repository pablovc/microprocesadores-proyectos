
/*
 *  Programa de muestra para el uso del TIMER n�mero 3.
 *  Se hace uso de compilaci�n condicional para seleccionar entre
 *  configurar ya sea conteo ascendente o descendente.
 */

#include "tm4c1294ncpdt.h"
// las siguientes directivas se emplean para compilaci�n condicional.
                     // Tipo de conteo
#define TMRCNT_DN    // TMRCNT_UP: up
                     // TMRCNT_DN: down


//*********************************************
// CONFIGURACI�N PARA TIMER DE 16 BITS
// La fuente de reloj lo especifica el campo ALTCLK del registro GPTMCC
// Seleccionando por default al SysClk y sin alterar el PIOSC (16MHz)

main(void)
{
    uint32_t ui32Loop;
    SYSCTL_RCGCTIMER_R |= 0x08;     // HABILITA TIMER 3
    SYSCTL_RCGCGPIO_R = 0x1000;     //habilita PORTN
    ui32Loop = SYSCTL_RCGCGPIO_R;   // retardo para establecimiento reloj del PORTN Y TIMER 3

    TIMER3_CTL_R= 0X00000000;   // DESHABILITA TIMER EN LA CONFIGURACION - afecta a Timer A y B
    TIMER3_CFG_R= 0X00000004;   // CONFIGURAR PARA 16 BITS  - afecta a Timer A y B

#if defined(TMRCNT_DN)          // CONFIGURAR PARA MODO PERIODICO, CUENTA HACIA ABAJO �...
    TIMER3_TAMR_R= 0x00000002;
#elif defined(TMRCNT_UP)        // CONFIGURAR PARA MODO PERIODICO, CUENTA HACIA ARRIBA
    TIMER3_TAMR_R= 0x00000012;
#endif

    TIMER3_TAILR_R= 0x00FFFF;      // VALOR DE RECARGA - Cuenta b�sica de 16 bits
    TIMER3_TAPR_R= 0x0F;           // 16 MHZ /16 =1MHz -> 16-1=15 (se escribe valor de prescalador -1)
    TIMER3_ICR_R= 0x00000001 ;     // LIMPIA POSIBLE BANDERA PENDIENTE DE TIMER3-> TIMER A

                                   // Si se usa polling:
    TIMER3_IMR_R &= 0x00000000;    // TODAS LAS INTS. DE TIMER3 DESACTIVADAS
    TIMER3_CTL_R |= 0x00000001;    // HABILITA TIMER A EN LA CONFIGURACION

    GPIO_PORTN_DIR_R = 0x01;       // configura como salida
    GPIO_PORTN_DEN_R = 0x01;       // habilita el bit 0 como digital
    GPIO_PORTN_DATA_R = 0x00;

    while(1)
    {
        // Checa el bit te interrupcion Raw (RIS) por Polling
        while((TIMER3_RIS_R & TIMER_RIS_TATORIS)==0); // espera la cuenta completa
        GPIO_PORTN_DATA_R ^= 0x01;              // complementa bits de puerto (led PN0)
        TIMER3_ICR_R = TIMER_ICR_TATOCINT;      // limpia bandera de TIMER A Interrupt Clear
    }

}



