/*
 * Timer 3, bloque A con Interrupción.
 *
 */

#include "tm4c1294ncpdt.h"
//#include "config_timer.h"

// las siguientes directivas se emplean para compilación condicional.
#define TIMERA       // Tipo de Timer a emplear: A o B
#define TMRCNT_DN    // Tipo de conteo TMRCNT_UP: up, TMRCNT_DN: down; en este modo, no hay diferencia
#define TMRINTEN     // Comentar esta directiva si se emplea Polling en lugar de interrupción.

//*********************************************
// CONFIGURACIÓN PARA TIMER DE 16 BITS
// La fuente de reloj lo especifica el campo ALTCLK del registro GPTMCC
// Seleccionando por default al SysClk que es también por default y sin alterar el PIOSC (16MHz)

main(void) {

    uint32_t ui32Loop;

    SYSCTL_RCGCTIMER_R |= 0x08;     // HABILITA TIMER 3
    ui32Loop = SYSCTL_RCGCGPIO_R;   // retardo para establecimiento reloj del PORTN Y TIMER 3
    SYSCTL_RCGCGPIO_R = SYSCTL_RCGCGPIO_R12;     //habilita PORTN


    TIMER3_CTL_R= 0X00000000;       // DESHABILITA TIMER EN LA CONFIGURACION - afecta a Timer A y B
    TIMER3_CFG_R= 0X00000004;       // CONFIGURAR PARA 16 BITS   - afecta a Timer A y B


#define TIMER_RIS_TORIS 0x00000001   // bit TATORIS del registro TIMER_RIS_R
#define FLAG_IC 0x001                // Bandera Interrupt Clear para Timer A

                                   // CONFIGURAR PARA MODO PERIODICO, CUENTA HACIA ABAJO
    TIMER3_TAMR_R= 0x00000002;

    TIMER3_TAILR_R= 0x00FFFF;      // VALOR DE RECARGA - Cuenta básica de 16 bits
    TIMER3_TAPR_R= 0x0F;           // 16 MHZ /16 =1MHz -> 16-1=15 (se escribe valor de prescalador -1)
    TIMER3_ICR_R= 0x00000001 ;     // LIMPIA POSIBLE BANDERA PENDIENTE DE TIMER3-> TIMER A

                                 // si se usa interrupción:
    TIMER3_IMR_R |= 0x00000001; // ACTIVA INTRRUPCION DE TIMEOUT - TIMER A

                                // es posible programar un valor de prioridad.

    NVIC_EN1_R= 1<<(35-32);     // HABILITA LA INTERRUPCION DE  TIMER3
    TIMER3_CTL_R |= 0x00000001;     // HABILITA TIMER A

    GPIO_PORTN_DIR_R = 0x01;    // configura como salida
    GPIO_PORTN_DEN_R = 0x01;    // habilita el bit 0 como digital
    GPIO_PORTN_DATA_R = 0x00;

     while(1);

}

// función ISR de TIMER
void isrTimerA(void)
{
    TIMER3_ICR_R= 0x00000001; //
    GPIO_PORTN_DATA_R ^= 0x01;
}
