/*
 * mainmyUart01.c
 *
 *  Created on: 04/11/2017
 *  Last Update: 8 nov 2017
 *      Author: M.I. Armando Salom�n Hern�ndez-D.
 *              UNAM
 *
 *      Uso de Modulo UART.
 *      El Modulo usado es el UART0, el cual se encuentra comunicado a trav�z de la interfaz USB-Serial de la propia
 *      tarjeta TIVA. Los JUMPERS JP4 deben estar colocados en la posici�n UART.
 *
 *      Transmision y recepci�n de 8 bits a 115200 bits/sec.
 *      La tasa de transmision se configura con dos registros que sirven como divisor de la frecuencia de Reloj aplicado
 *      al modulo UART. Estos dos registros son:
 *      _IBRD_R  (Integer Baud Rate Divisor)
 *      _FBRD_R  (Fractional )
 *
 *      Adicional a estos valores como divisores, se emplea otro factor que escala a estos divisores por 8 o 16. Por
 *      default es 16; El bit que selecciona estos dos valores es el HSE en el registro _CTL_R
 *      Para el Modulo UART 0, se configura las terminales del puerto GPIO A para su funci�n alterna, especificandola
 *      en el registro del puerto GPIO  _PCTL_R (Revisar Tabla de funciones Alternas de GPIOs).
 *      Los bits configurables del registro de control se pueden consultar con el nombre base UART_CTL_***
 *      donde *** ser� el campo de bits.
 *
 *      El reloj proporcionado al UART ser� el mismo que el reloj del Sistema (Default), en este caso el PIOSC de 16MHz.
 *      Cuando el SysClk es el MOSC o incluso en combinacion con el PLL, �ste reloj (SysClk) puede ser el que se aplica a la UART, o en caso
 *      de requerirlo, una fuente de reloj alterna a seleccionar. La selecion entre SysClk o Clk Alterno se configura con el
 *      registro UART CC (Clock Configuration); para la revision del Hardware actual, solo se dispone de dos opciones:
 *      0-> SysClk (defaul), 5-> AltClk.
 *      Al seleccionar AltClk, la fuente de reloj alterna se hace Configurando el registro SYSCTL_ALTCLKCFG_R.
 *      Para este ultimo, las opciones a elegir son PIOSC, Main Osc., LFIOSC, RTCOSC.
 *
 *      Ejemplo de configuraci�n:
 *      SysClk = 16MHz
 *      Baud Rate = 115200
 *      Factor Divisor = 16 (default)
 *      16000000/(16*115200) = 8.680555
 *      _IBDR_R = 8
 *      _FBDR_R = Parte entera(0.6805*64 + 0.5) = 44
 *
 *      Cuando no se usan FIFOs, en un mismo registro se coloca el dato para enviar o recibido.
 *      Se identifica el tipo de dato (Rx o Tx) en el registro de banderas UART0_FR_R
 *
 *      Funciones de ejemplo:
 *      Recibir caracter
 *      Transmitir caracter
 *      Recibir cadena
 *      Transmitir cadena
 *
 */

#include  "tm4c1294ncpdt.h"

#define LEN_BUFF 21     // longitud de buffer de recepci�n
char d_uint8Dato;
void UART_INI(void);
char UART_Lee_dato(void);
void UART0_Escribe_dato(char);
void UART0_Escribe_cadena(char *);
void UART0_Recibe_cadena(void);

char myMsg[] = "Hola mundo\0";  // cadena con '0' como terminacion
char buffer[LEN_BUFF];          // bufer para recibir cadena

int main(void)
{

    UART_INI();

    while (1)
    {
        UART_Lee_dato();            // espera a recibir un caracter
        UART0_Escribe_dato(d_uint8Dato);    // env�a el caracter recibido
                                    // Algunos caracteres ASCII no visibles
        UART0_Escribe_dato(0x07);   // Bell
        UART0_Escribe_dato(0x0A);   // nueva l�nea
        UART0_Escribe_dato(0x0D);   // Enter
        UART0_Escribe_cadena(myMsg);    // escribe una cadena terminada por '0'
        UART0_Recibe_cadena();          // recibe cadena de caracteres hasta que recibe un "enter", guarda en buffer
        UART0_Escribe_cadena(buffer);   // env�a una cadena ( la cadena recibida)

    }
}



void UART_INI(void)
{
    SYSCTL_RCGCUART_R |=0X0001; // Habilitar Reloj para UART0
    SYSCTL_RCGCGPIO_R |=0X0001; // Habilitar Reloj para PUERTO A
    while(!(SYSCTL_PRUART_R & SYSCTL_PRUART_R0));


    UART0_CTL_R &= ~0x0001;     // DESHABILITAR UART 0
    UART0_IBRD_R = 8 ;          // CONFIGURACION DE BAUD-RATE: IBDR
    UART0_FBRD_R = 44 ;         // CONFIGURACION DE BAUD-RATE: FBRD
    UART0_LCRH_R = 0x0060;      // 8 BITS, DESHABILITAR FIFO (1 localidad de FIFO disponible de 16)
    UART0_CTL_R  = 0x0301 ;     // Tran smision y recepcion de UART Habilitada, factor divisor 16, Habilitar UART


    GPIO_PORTA_PCTL_R = (GPIO_PORTA_PCTL_R&0XFFFFFF00)+0X00000011; // Funci�n UART
    GPIO_PORTA_DEN_R |= 0x03;       // HABILITAR FUNCION I/O DIGITAL
    GPIO_PORTA_AMSEL_R &= ~0x03;    // DESHABILITAR FUNCION ANALOGICA EN PA0-1
    GPIO_PORTA_AFSEL_R |= 0x03;     // HABILITAR FUNCION ALTERNA EN PA0-1
}

/*
 * Funci�n que lee un caracter recibido.
 * Esperar hasta que se reciba un dato y lo guarda en la variable d_uint8Dato
 * La funci�n tambi�n entrega el valor recibido, por ello el tipo de retorno 'char'
 * se espera a que la bandera RX FIFO EMPTY = 0 (No Vac�a -> dato nuevo)
 * Mascara UART_FR_RXFE definida en archivo de cabecera
 *
 */

char UART_Lee_dato(void)
{
    while((UART0_FR_R & 0x0010)!=0); // ESPERAR A QUE RXFE SEA CERO -> dato nuevo recibido
    d_uint8Dato = ((char)(UART0_DR_R & 0xff));  // guarda dato a 8 bits
    return((char)(UART0_DR_R&0xff));
}
/*
 * La funci�n recibe un valor de 8 bits (caracter codificado en ASCII)
 * Espera a que la FIFO de Tx est� vac�a para escribir un nuevo caracter
 * TX FIFO FULL = 1 (FIFO llena -> dato anterior siendo transmitido)
 * Mascara UART_FR_TXFF definida en archivo de cabecera
 */

void UART0_Escribe_dato(char dato)
{
    while   ((UART0_FR_R & 0X0020)!=0); // espera a que TXFF sea cero -> se puede transmitir
    UART0_DR_R=dato;
}

/*
 * Para una cadena predefinida en el programa, env�a cada caracter de hasta encontrar un '0'
 */
void UART0_Escribe_cadena(char *cadena)
{
    char i=0;
    char j=0;
    j = cadena[i];
    while (j!=0)
    {
        UART0_Escribe_dato(j);
        i++;
        j = cadena[i];
    }

}

/*
 * Recibe caracteres si es que recibe un valor diferente de 0x0D = 13 (Carry Return o Enter)
 * y no se excede de la longitud del buffer preestablecida con LEN_BUFF.
 * La cadena recibida se guarda en el buffer con un terminador '0'
 */
void UART0_Recibe_cadena(void)
{
    unsigned char i=0;
    d_uint8Dato = 0;

    do{
        d_uint8Dato = UART_Lee_dato();
        buffer[i] = d_uint8Dato;
        i++;

    }while ( (d_uint8Dato != 0x0D ) && (i<(LEN_BUFF-1)));
    buffer[i] = 0;  // ultimo elemento es caracter NULL '0' (terminador de cadena)
    /*
    UART0_Escribe_dato(0x07);   // Bell
    UART0_Escribe_dato(0x0A);   // nueva l�nea
    UART0_Escribe_dato(0x0D);   // Enter
    */
}
