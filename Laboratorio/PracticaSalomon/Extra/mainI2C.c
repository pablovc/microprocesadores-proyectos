/*
 * main.c
 * Uso de bus I2C0 en puerto B
 * PB2 SCL
 * PB3 SDATA
 *
 * Programa basado en el archivo I2C0.c de J.Valvano
 * implementado para LM4F120/TM4C123
 * NOTA: Verificar funcionalida para TIVA
 */
#include "tm4c1294ncpdt.h"
typedef unsigned int uint16_t;
typedef unsigned char uint8_t;

#define MAXRETRIES              5

// Registros del BMP180
#define BMP180_addr       0x77  // Direcci�n en 7 bits
#define BMP180_Write_addr 0xEE
#define BMP180_Read_addr  0xEF
#define BMP180_ChipID_R   0xD0
#define BMP180_Sco_R      0xF4  //Start of conversion
#define BMP180_SRST_R     0xE0 // soft reset
#define BMP180_MSB_Data_R 0xF6
#define BMP180_LSB_Data_R 0xF7
#define BMP180_XLSB_Data_R 0xF8

#define ctrl_sco_temp  0x2E // valor para inicio de lectura de temperatura

void I2C_Init(void){
  SYSCTL_RCGCI2C_R |= 0x0001;           // activate I2C0
  SYSCTL_RCGCGPIO_R |= 0x0002;          // activate port B
  while((SYSCTL_PRGPIO_R&0x0002) == 0){};// ready?

  GPIO_PORTB_AFSEL_R |= 0x0C;           // 3) enable alt funct on PB2,3
  GPIO_PORTB_ODR_R |= 0x08;             // 4) enable open drain on PB3 only
  GPIO_PORTB_DEN_R |= 0x0C;             // 5) enable digital I/O on PB2,3
                                        // 6) configure PB2,3 as I2C
  GPIO_PORTB_PCTL_R = (GPIO_PORTB_PCTL_R&0xFFFF00FF)+0x00002200;
  GPIO_PORTB_AMSEL_R &= ~0x0C;          // 7) disable analog functionality on PB2,3
  I2C0_MCR_R = I2C_MCR_MFE;      // 9) master function enable
  I2C0_MTPR_R = 24;              // 8) configure for 100 kbps clock
  // 20*(TPR+1)*20ns = 10us, with TPR=24
}

// env�a direcci�n i2c y registro, usado al inicio de una lectura de i2c
uint32_t I2C_Send1(uint8_t slave, uint8_t data1){
  while(I2C0_MCS_R&I2C_MCS_BUSY){};// wait for I2C ready
  I2C0_MSA_R = (slave<<1)&0xFE;    // MSA[7:1] is slave address
  I2C0_MSA_R &= ~0x01;             // MSA[0] is 0 for send
  I2C0_MDR_R = data1&0xFF;         // prepare first byte
  I2C0_MCS_R = (0
                    //   & ~I2C_MCS_ACK     // no data ack (no data on send)
                       | I2C_MCS_STOP     // generate stop
                       | I2C_MCS_START    // generate start/restart
                       | I2C_MCS_RUN);    // master enable
  while(I2C0_MCS_R&I2C_MCS_BUSY){};// wait for transmission done
                                          // return error bits
  return (I2C0_MCS_R&(I2C_MCS_DATACK|I2C_MCS_ADRACK|I2C_MCS_ERROR));
}


// usado para enviar direccion i2c, registro y valor del registro
uint32_t I2C_Send2(uint8_t slave, uint8_t data1, uint8_t data2){
      while(I2C0_MCS_R&I2C_MCS_BUSY){};// wait for I2C ready
      I2C0_MSA_R = (slave<<1)&0xFE;    // MSA[7:1] is slave address
      I2C0_MSA_R &= ~0x01;             // MSA[0] is 0 for send
      I2C0_MDR_R = data1&0xFF;         // prepare first byte
      I2C0_MCS_R = (0
                         //  & ~I2C_MCS_ACK     // no data ack (no data on send)
                        //   & ~I2C_MCS_STOP    // no stop
                           | I2C_MCS_START    // generate start/restart
                           | I2C_MCS_RUN);    // master enable
      while(I2C0_MCS_R & I2C_MCS_BUSY){};// wait for transmission done
                                              // check error bits
      if((I2C0_MCS_R & (I2C_MCS_DATACK|I2C_MCS_ADRACK|I2C_MCS_ERROR)) != 0){
        I2C0_MCS_R = (0                // send stop if nonzero
                         //  & ~I2C_MCS_ACK     // no data ack (no data on send)
                           | I2C_MCS_STOP     // stop
                         //  & ~I2C_MCS_START   // no start/restart
                         //  & ~I2C_MCS_RUN    // master disable
                            );
                                              // return error bits if nonzero
        return (I2C0_MCS_R&(I2C_MCS_DATACK|I2C_MCS_ADRACK|I2C_MCS_ERROR));
      }
      I2C0_MDR_R = data2 & 0xFF;         // prepare second byte
      I2C0_MCS_R = (0
                          // & ~I2C_MCS_ACK     // no data ack (no data on send)
                           | I2C_MCS_STOP     // generate stop
                          // & ~I2C_MCS_START   // no start/restart
                           | I2C_MCS_RUN);    // master enable
      while(I2C0_MCS_R&I2C_MCS_BUSY){};// wait for transmission done
                                              // return error bits
      return (I2C0_MCS_R&(I2C_MCS_DATACK|I2C_MCS_ADRACK|I2C_MCS_ERROR));
}

// usado para recibir un byte de i2c. Antes de usar esta funci�n,
// se debi� haber enviado direccion i2c y registro que se lee
uint8_t I2C_Recv(uint8_t slave){
  int retryCounter = 1;
  do{
    while(I2C0_MCS_R&I2C_MCS_BUSY){};// wait for I2C ready
    I2C0_MSA_R = (slave<<1)&0xFE;    // MSA[7:1] is slave address
    I2C0_MSA_R |= 0x01;              // MSA[0] is 1 for receive
    I2C0_MCS_R = (0
                        // & ~I2C_MCS_ACK     // negative data ack (last byte)
                         | I2C_MCS_STOP     // generate stop
                         | I2C_MCS_START    // generate start/restart
                         | I2C_MCS_RUN);    // master enable
    while(I2C0_MCS_R&I2C_MCS_BUSY){};// wait for transmission done
    retryCounter = retryCounter + 1;        // increment retry counter
  }                                         // repeat if error
  while(((I2C0_MCS_R&(I2C_MCS_ADRACK|I2C_MCS_ERROR)) != 0) && (retryCounter <= MAXRETRIES));
  return (I2C0_MDR_R&0xFF);          // usually returns 0xFF on error
}

// usado para recibir dos bytes de i2c. Antes de usar esta funci�n,
// se debi� haber enviado direccion i2c y registro inicial que se lee
uint16_t I2C_Recv2(uint8_t slave){
  uint8_t data1,data2;
  int retryCounter = 1;
  do{
    while(I2C0_MCS_R&I2C_MCS_BUSY){};// wait for I2C ready
    I2C0_MSA_R = (slave<<1)&0xFE;    // MSA[7:1] is slave address
    I2C0_MSA_R |= 0x01;              // MSA[0] is 1 for receive
    I2C0_MCS_R = (0
                         | I2C_MCS_ACK      // positive data ack
                       //  & ~I2C_MCS_STOP    // no stop
                         | I2C_MCS_START    // generate start/restart
                         | I2C_MCS_RUN);    // master enable
    while(I2C0_MCS_R&I2C_MCS_BUSY){};// wait for transmission done
    data1 = (I2C0_MDR_R&0xFF);       // MSB data sent first
    I2C0_MCS_R = (0
                       //  & ~I2C_MCS_ACK     // negative data ack (last byte)
                         | I2C_MCS_STOP     // generate stop
                       //  & ~I2C_MCS_START   // no start/restart
                         | I2C_MCS_RUN);    // master enable
    while(I2C0_MCS_R&I2C_MCS_BUSY){};// wait for transmission done
    data2 = (I2C0_MDR_R&0xFF);       // LSB data sent last
    retryCounter = retryCounter + 1;        // increment retry counter
  }                                         // repeat if error
  while(((I2C0_MCS_R&(I2C_MCS_ADRACK|I2C_MCS_ERROR)) != 0) && (retryCounter <= MAXRETRIES));
  return (data1<<8)+data2;                  // usually returns 0xFFFF on error
}


// Secuencia de recepci�n de 8 bits del registro indicado en el esclavo indicado
uint8_t recv8bcal(uint8_t slave, uint8_t reg)
{
    uint32_t errTr = 0;
    errTr = I2C_Send1(slave,reg);
    if(errTr)
        return 0;
    else
        return I2C_Recv(slave);
}

// Secuencia de recepci�n de 16 bits del registro indicado en el esclavo indicado

uint16_t recv16bcal(uint8_t slave, uint8_t reg){
    uint32_t errTr = 0;
    errTr = I2C_Send1(slave,reg);
    if(errTr)
        return 0xFFFF;
    else
        return I2C_Recv2(slave);
}



int main(void) {
    uint8_t id_i2c=0;
    uint8_t err=0;
    uint16_t val_cal=0;
    uint16_t UT=0;  // valor leido de temperatura
    uint16_t AC1,AC2,AC3,AC4,AC5,AC6,B1,B2,MB,MC,MD;
    AC1=AC2=AC3=AC4=AC5=AC6=B1=B2=MB=MC=MD=0;

    I2C_Init();
    // I2C_Send2(0x77,0xE0,0xB6);       //comando de Soft Reset
    err=I2C_Send1(0x77,BMP180_ChipID_R);    // leer ID de BMP180

    id_i2c = I2C_Recv(0x77);            // en lectura correcta id_i2c = 0x55

    while(1){
        // lectura de los datos de calibraci�n
        // c�digo por probar
        AC1 = recv16bcal(0x77,0xAA);
        AC2 = recv16bcal(0x77,0xAC);
        AC3 = recv16bcal(0x77,0xAE);
        AC4 = recv16bcal(0x77,0xB0);
        AC5 = recv16bcal(0x77,0xB2);
        AC6 = recv16bcal(0x77,0xB4);
        B1  = recv16bcal(0x77,0xB6);
        B2  = recv16bcal(0x77,0xB8);
        MB =  recv16bcal(0x77,0xBA);
        MC =  recv16bcal(0x77,0xBC);
        MD =  recv16bcal(0x77,0xBE);

    };

	return 0;
}
