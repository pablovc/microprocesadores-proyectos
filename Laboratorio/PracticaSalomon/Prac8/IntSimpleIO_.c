

#include "tm4c1294ncpdt.h"

volatile uint32_t FallingEdges = 0;

void DisableInterrupts(void){
	__asm(	"  CPSID I ");
}                                                                          

void EnableInterrupts(void){
	__asm( "  CPSIE I  ");
}


void EdgeCounteR_Init (void) {
	// Puerto J0 - SW1 , y puerto N, PN1 - Led 1
	SYSCTL_RCGCGPIO_R |= ;// (a) Activa reloj para puertos J y N
	FallingEdges = 0; 	    // (b) inicializa contador
	GPIO_PORTJ_DIR_R   &= ; // (c) terminal PJ0 de entrada (boton integrado SW1)
	GPIO_PORTJ_DEN_R   |= ; // (d)Habilita modo digital PJ0
	GPIO_PORTJ_PUR_R   |= ; // (e)Habilita pull-up en PJ0
	GPIO_PORTJ_IS_R    &= ;// (f) PJ0 sensible en flanco
	GPIO_PORTJ_IBE_R    = ; // (g) PJ0 is not both edges
	GPIO_PORTJ_IEV_R   &= ; // PJ0 falling edge event
	GPIO_PORTJ_ICR_R    = ; // (e) clear flag1
	GPIO_PORTJ_IM_R    |= ; // (f) arm interrupt on PJ0

	NVIC_PRI12_R   = (NVIC_PRI12_R & 0x0) | 0x0; // (g) priority 5
	NVIC_EN1_R    =  ; // (h) enable interrupt 51 in NVIC


        //  Se tiene que habilitar la interrupcion (bit) 51 del conjunto de registros NVIC_EN0_R y NVIC_EN1_R
        //  En el registro NVIC_EN0_R se tienen los bits 0-31
        //  En el registro NVIC_EN1_R se tienen los bits 32-63
        //  51-32 = 19 -> el bit 19 del siguiente registro (NVIC_EN1_R)

        // NVIC_EN1_R = NVIC_EN1_R | ( 1 << 19)
        // NVIC_EN1_R = NVIC_EN1_R | ( 1 << (51-32) );




// Configuracion de LED en PN1-LED1
	GPIO_PORTN_DIR_R   = ;	// salida bit 1
	GPIO_PORTN_DEN_R   = ;

	EnableInterrupts(); 	//
}


void GPIOPORTJ_Handler (void) {
	GPIO_PORTJ_ICR_R =  ; // a<limpia cknowledge flag
	FallingEdges = FallingEdges + 1 ;
	GPIO_PORTN_DATA_R ^=  ;    //toggle LED
}


void main(void){
	EdgeCounteR_Init();

	while(1){
          

        }
}
