#include "tm4c1294ncpdt.h"

// Ejercicio: Completar los valores de configuración para que cumpla la función especificada
// en los comentarios. Este programa usa la tecnica de encuesta.

void main(void){
	SYSCTL_RCGCGPIO_R = ;  // 1) Habilita reloj para Puerto E, K
	while( (SYSCTL_PRGPIO_R & ) ==0);

	GPIO_PORTE_DIR_R =   ;    // 2) PE4 entrada (analógica)
	GPIO_PORTE_AFSEL_R |= ;   // 3) Habilita Función Alterna de PE4
	GPIO_PORTE_DEN_R =   ;    // 4) Deshabilita Función Digital de PE4
	GPIO_PORTE_AMSEL_R |=   ; // 5) Habilita Modo Analógico de PE4

	GPIO_PORTK_DIR_R  = ;    // 5a) puerto K todos los bits salidas digitales
	GPIO_PORTK_DEN_R  = ;    // todos digitales.
	GPIO_PORTK_DATA_R = 0x00;

	SYSCTL_RCGCADC_R  = 0x01;   // 6) Habilita reloj para lógica de ADC0
	while((SYSCTL_PRADC_R&0x01)==0);

	ADC0_PC_R = 0x01;       // 7) Configura para 125Ksamp/s
	ADC0_SSPRI_R = 0x0123;  // 8) SS3 con la más alta prioridad
	ADC0_ACTSS_R = 0x0000;  // 9) Deshabilita SS3 antes de cambiar configuración de registros
	ADC0_EMUX_R = 0x0000;   // 10) iniciar muestreo por software
	ADC0_SSEMUX3_R = 0x00;	// 11)Entradas AIN(15:0)
	ADC0_SSMUX3_R = (ADC0_SSMUX3_R & 0xFFFFFFF0) + 9; // 11a) canal AIN9 (PE4)
	ADC0_SSCTL3_R = 0x0006; // 12) no TS0 D0 , yes IE0 END0
	ADC0_IM_R = 0x0000;     // 13) Deshabilita interrupciones de SS3
	ADC0_ACTSS_R |= 0x0008; // 14) Habilita SS3

	SYSCTL_PLLFREQ0_R |= SYSCTL_PLLFREQ0_PLLPWR;	// 15) encender PLL
	while((SYSCTL_PLLSTAT_R&0x01)==0);		//     espera a que el PLL fije su frecuencia
	SYSCTL_PLLFREQ0_R &= ~SYSCTL_PLLFREQ0_PLLPWR;	//     apagar PLL

	ADC0_ISC_R = 0x0008;				// 16) Se recomienda Limpiar la bandera RIS del ADC0
	for(;;){
	  uint32_t result;
	  ADC0_PSSI_R =0x0008;		        // 17) Inicia conversión del SS3
	  while ((ADC0_RIS_R & 0x08)==0);   // Espera a que SS3 termine conversión (polling)
	  result = (ADC0_SSFIFO3_R & 0xFFF);// Resultado en FIFO3 se asigna a variable "result"
	  ADC0_ISC_R = 0x0008;			// 18) Limpia la bandera RIS del ADC0
	  GPIO_PORTK_DATA_R = (result >> 4);	// 19) despliega en 8 bits resultado en PTO K.
	}
}
