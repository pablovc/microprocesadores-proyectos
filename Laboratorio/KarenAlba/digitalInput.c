
/*Asignatura: Microcontroladores y microprocesadores.   *Semestre: 2019-2.
*Tema 9: Perifericos
*Punto 9.2: El temporizador y sus aplicaciones.
*Práctica: Motor a pasos unipolar.
*Presentan: Dr. Saúl de la Rosa Nieves,  Ing. Jose Eduardo Villa Herrera.
-------------------------------------------------------------------------------
*/
// LIBRERIAS QUE UTILIZA EL PROGRAMA
#include <stdbool.h>
#include <stdint.h>
#include "inc/tm4c1294ncpdt.h"

//VAriables globales de programa

volatile uint32_t Count= 0;
volatile uint32_t Termino= 0;
volatile uint32_t EstadoBoton = 0;
volatile uint32_t cuenta = 0;


// DefiniciOn de apuntadores para direccionar
//#define GPIO_PORTN_DATA_R       (*((volatile unsigned long *)0x4006400C)) // Registro de Datos Puerto N
//#define GPIO_PORTN_DIR_R        (*((volatile unsigned long *)0x40064400)) // Registro de Dirección Puerto N
//#define GPIO_PORTN_DEN_R        (*((volatile unsigned long *)0x4006451C)) // Registro de Habilitación Puerto N

#define SYSCTL_RCGCGPIO_R       (*((volatile uint32_t *)0x400FE608))
#define SYSCTL_PRGPIO_R         (*((volatile unsigned long *)0x400FEA08))

#define GPIO_PORTJ_DIR_R        (*((volatile uint32_t *)0x40060400)) //Registro de Dirección PJ
#define GPIO_PORTJ_DEN_R        (*((volatile uint32_t *)0x4006051C)) //Registro de habilitación PJ
#define GPIO_PORTJ_PUR_R        (*((volatile uint32_t *)0x40060510)) //Registro de pull-up PJ
#define GPIO_PORTJ_DATA_R       (*((volatile uint32_t *)0x40060004)) //Registro de Datos J
#define GPIO_PORTJ_IS_R         (*((volatile uint32_t *)0x40060404)) //Registro de configuración de detección de nivel o flanco
#define GPIO_PORTJ_IBE_R        (*((volatile uint32_t *)0x40060408)) //Registro de configuración de interrupción por ambos flancos
#define GPIO_PORTJ_IEV_R        (*((volatile uint32_t *)0x4006040C)) //Registro de configuración de interrupción por un flanco
#define GPIO_PORTJ_ICR_R        (*((volatile uint32_t *)0x4006041C)) //Registro de limpieza de interrupcion de flanco en PJ


#define GPIO_PORTA_DIR_R        (*((volatile uint32_t *)0x40058400)) //Registro de Dirección PA
#define GPIO_PORTA_DEN_R        (*((volatile uint32_t *)0x4005851C)) //Registro de habilitación PA
#define GPIO_PORTA_PUR_R        (*((volatile uint32_t *)0x40058510)) //Registro de pull-up PA
#define GPIO_PORTA_DATA_R       (*((volatile uint32_t *)0x40058004)) //Registro de Datos A
#define GPIO_PORTA_IS_R         (*((volatile uint32_t *)0x40058404)) //Registro de configuración de detección de nivel o flanco
#define GPIO_PORTA_IBE_R        (*((volatile uint32_t *)0x40058408)) //Registro de configuración de interrupción por ambos flancos
#define GPIO_PORTA_IEV_R        (*((volatile uint32_t *)0x4005840C)) //Registro de configuración de interrupción por un flanco
#define GPIO_PORTA_ICR_R        (*((volatile uint32_t *)0x4005841C)) //Registro de limpieza de interrupcion de flanco en PA

#define GPIO_PORTA_AHB_AFSEL_R  (*((volatile uint32_t *)0x40058420))
#define GPIO_PORTA_AHB_AMSEL_R  (*((volatile uint32_t *)0x40058528))

#define GPIO_PORTJ_AHB_AFSEL_R  (*((volatile uint32_t *)0x40050420))
#define GPIO_PORTJ_AHB_AMSEL_R  (*((volatile uint32_t *)0x40050528))


// RUTINA DE SERVICIO DE INTERRUPCIÓN
void Timer03AIntHandler(void)
{
    //LIMPIA BANDERA
    TIMER3_ICR_R= 0X00000001 ; //LIMPIA BANDERA DE TIMER3

    if(EstadoBoton==1){
        cuenta++;
    }
    /*Termino = Termino + 1;

     if (Termino < 2048) // 32 * 64 = 2048
     {
    Count = Count + 0x01;

    switch (Count&0x0F) {
           case 0x01:
             GPIO_PORTL_DATA_R=0x03; // A,B
             GPIO_PORTN_DATA_R = 0x03; // A,B
             GPIO_PORTF_AHB_DATA_R = 0x00; //
             break;
           case 0x02:
             GPIO_PORTL_DATA_R=0x06; // A',B
             GPIO_PORTN_DATA_R = 0x01; // B
             GPIO_PORTF_AHB_DATA_R = 0x10; // A'
             break;
           case 0x03:
             GPIO_PORTL_DATA_R=0x0C; // A', B'
             GPIO_PORTN_DATA_R = 0x00; //
             GPIO_PORTF_AHB_DATA_R = 0x11; //A', B'
             break;
           case 0x04:
             GPIO_PORTL_DATA_R=0x09; // A, B'
             GPIO_PORTN_DATA_R = 0x02; // A
             GPIO_PORTF_AHB_DATA_R = 0x01; // B'
             Count = 0x0;
              break;
       }
      }*/
    }
/*********************************************
* SE GENERAN LAS SEÑALES DE CONTROL PARA UN MOTOR UNIPOLAR
* PARA LA SINCRONIZACIÓN SE UTILIZA EL TIMER3 EN SU CONFIGURACIÓN DE 32 BITS
*
* LAS SEÑALES DE CONTROL SE MANEJAN POR EL PUERTO L
*
* SE UTILIZAN LOS LEDs CONTECTADOS A LOS PUERTOS F y N
* PARA VISUALIZAR LAS SEÑALES GENERADAS
* PF0, PF4 como salidas (Leds).
* PN0, PN1 como salidas (Leds)
*
*/


//**********FUNCION Inizializa el SysTick *********************
void SysTick_Init(void){
  NVIC_ST_CTRL_R = 0;                   // Desahabilita el SysTick durante la configuración
  NVIC_ST_RELOAD_R = NVIC_ST_RELOAD_M;  // Se establece el valor de cuenta deseado en RELOAD_R
  NVIC_ST_CURRENT_R = 0;                // Se escribe al registro current para limpiarlo

  NVIC_ST_CTRL_R = 0x00000001;         // Se Habilita el SysTick y se selecciona la fuente de reloj
}

//*************FUNCION Tiempo de retardo utilizando wait.***************
// El parametro de retardo esta en unidades del reloj interno/4 = 4 MHz (250 ns)
void SysTick_Wait(uint32_t retardo){
    NVIC_ST_RELOAD_R= retardo-1;   //nUmero de cuentas por esperar
    NVIC_ST_CURRENT_R = 0;
    while((NVIC_ST_CTRL_R&0x00010000)==0){//espera hasta que la bandera COUNT sea valida
    }
   } //

// Tiempo de retardo utilizando wait
// 4000000 igual a 1 s


void PORTJDigitalInput_Init(void){
    SYSCTL_RCGCGPIO_R |= 0x00000100; // (a) activa el reloj para el puerto J

        GPIO_PORTJ_DIR_R &= ~0x01;       // (b) PJ0 dirección entrada - boton SW1
         GPIO_PORTJ_DEN_R |= 0x01;        //     PJ0 se habilita
         GPIO_PORTJ_PUR_R |= 0x01;        //     habilita weak pull-up on PJ1
         GPIO_PORTJ_IS_R &= ~0x01;        // (c) PJ1 es sensible por flanco (p.761)
         GPIO_PORTJ_IBE_R &= ~0x01;       //     PJ1 no es sensible a dos flancos (p. 762)
         GPIO_PORTJ_IEV_R &= ~0x01;       //     PJ1 detecta eventos de flanco de bajada (p.763)
         GPIO_PORTJ_ICR_R = 0x01;         // (d) limpia la bandera 0 (p.769)
}

void PORTADigitalInput_Init(void){
    SYSCTL_RCGCGPIO_R |= 0x00000001; // (a) activa el reloj para el puerto A

            GPIO_PORTA_DIR_R &= ~0x01;       // (b) PA0 dirección entrada - boton SW1
             GPIO_PORTA_DEN_R |= 0x01;        //     PA0 se habilita
             GPIO_PORTA_PUR_R |= 0x01;        //     habilita weak pull-up on PA1
             GPIO_PORTA_IS_R &= ~0x01;        // (c)  es sensible por flanco (p.761)
             GPIO_PORTA_IBE_R &= ~0x01;       //      no es sensible a dos flancos (p. 762)
             GPIO_PORTA_IEV_R &= ~0x01;       //      detecta eventos de flanco de bajada (p.763)
             GPIO_PORTA_ICR_R = 0x01;         // (d) limpia la bandera 0 (p.769)

        }

 main(void) {
     //habilita PORTN, PORTF, PORTL
     // SYSCTL_RCGCGPIO_R |= 0X1420; // RELOJ PARA EL PUERTO F, L y N
     //SYSCTL_RCGCGPIO_R |= 0X1100; // RELOJ PARA EL PUERTO J y N
     SYSCTL_RCGCGPIO_R |= 0X1101; // RELOJ PARA EL PUERTO A, J y N
      SYSCTL_RCGCTIMER_R |= 0X08; //RELOJ Y HABILITA TIMER 3 (p.380)

         //retardo para que el reloj alcance el PORTN Y TIMER 3
         //while ((SYSCTL_PRGPIO_R & 0X1000) == 0){};  // reloj listo?
      while ((SYSCTL_PRGPIO_R & 0X1101) == 0){};  // reloj listo?
         TIMER3_CTL_R=0X00000000; //DESHABILITA TIMER 3 PARA CONFIGURAR (p.986)
         TIMER3_CFG_R= 0X00000000; //CONFIGURA TIMER DE 32 BITS (p. 976)
         //TIMER3_TAMR_R= 0X00000002; //CONFIGURAR PARA MODO PERIODICO CUENTA HACIA ABAJO (p. 977)
         TIMER3_TAMR_R= 0X00000012; //CONFIGURAR PARA MODO PERIODICO CUENTA HACIA ARRIBA (p. 977)
         TIMER3_TAILR_R= 0X00007100; // VALOR DE RECARGA (p.1004)
         //TIMER3_TAILR_R= 0X000F7100; // VALOR DE RECARGA (p.1004)
//         TIMER3_TAILR_R= 0X0004E200; // VALOR DE RECARGA (p.1004)
         TIMER3_TAPR_R= 0X00; // PRESCALADOR DE TIMER A, SOLO PARA MODOS DE 16 BITS (p.1008)
         TIMER3_ICR_R= 0X00000001 ; //LIMPIA POSIBLE BANDERA PENDIENTE DE TIMER3 (p.1002)
         TIMER3_IMR_R |= 0X00000001; //ACTIVA INTRRUPCION DE TIMEOUT (p.993)
         NVIC_EN1_R= 1<<(35-32); //HABILITA LA INTERRUPCION 35 (TIMER3 A)
         TIMER3_CTL_R |= 0X00000001; //HABILITA TIMER 3 (p.986)

          //habilita al Puerto L como salida digital para control de motor
         //PL0,...,PL3 como salidas hacia el ULN2003 (A,A´,B,B´)
         GPIO_PORTL_DIR_R = 0x0F;
         GPIO_PORTL_DEN_R = 0x0F;
        //GPIO_PORTL_DATA_R = 0x00;

         // habilita PN0 y PN1 como salida digital para monitoreo del programa
         //
         GPIO_PORTN_DIR_R = 0x03;
         GPIO_PORTN_DEN_R = 0x03;

         //Botones
         //Botones
                       GPIO_PORTJ_DIR_R = 0x00;    // 2) entrada (Puerto A)
                       GPIO_PORTJ_AHB_AFSEL_R |= 0x7F; // 3) Habilita Función Alterna de (Puerto A)
                       //GPIO_PORTA_AHB_DEN_R = 0x00;    // 4) Deshabilita Función Digital de (Puerto A)
                       GPIO_PORTJ_DEN_R |= 0x7F;    // 4) Habilita FunciOn Digital de (Puerto A)
                       GPIO_PORTJ_AHB_AMSEL_R = 0x00; // 5) Deshabilita FunciOn AnalOgica de (Puerto A)


              GPIO_PORTA_DIR_R = 0x00;    // 2) entrada (Puerto A)
              GPIO_PORTA_AHB_AFSEL_R |= 0x7F; // 3) Habilita Función Alterna de (Puerto A)
              //GPIO_PORTA_AHB_DEN_R = 0x00;    // 4) Deshabilita Función Digital de (Puerto A)
              GPIO_PORTA_DEN_R |= 0x7F;    // 4) Habilita FunciOn Digital de (Puerto A)
              GPIO_PORTA_AHB_AMSEL_R = 0x00; // 5) Deshabilita FunciOn AnalOgica de (Puerto A)

         // habilita PF0 y PF4 como salida digital para monitoreo del programa
         //
//         GPIO_PORTF_AHB_DIR_R = 0x11;
  //       GPIO_PORTF_AHB_DEN_R = 0x11;
         SysTick_Init();
         //DigitalInput_Init();

         //LED0 = LED1 = OFF
        /* GPIO_PORTN_DATA_R=0x00;

         //LED1 = Trigger
         GPIO_PORTN_DATA_R|=0x02;
         //SysCtlDelay(5333333);//retardo de 1s [SysCtlDelay(1)=187.5 ns]
         SysTick_Wait(4000000);//retardo de 1s
         GPIO_PORTN_DATA_R&=~0x02; //PORTN and (negado 0x02) -> PN1 Clear -> PN1=0*/

         PORTJDigitalInput_Init();
                     PORTADigitalInput_Init();

         do{


             if((GPIO_PORTJ_DATA_R&0x01)==0){ // test bit 0 de Puerto J
             //if((GPIO_PORTA_DATA_R&0x01)==0){ // test bit 0 de Puerto A

                 EstadoBoton=1;
                 //EstadoBoton=0;
                  //cuenta++;
              }else{

                  EstadoBoton=0;
                  //EstadoBoton=1;
              }

              //GPIO_PORTN_DATA_R=0x00;
              //SysTick_Wait(8);//retardo de 2 us
              /*
              GPIO_PORTN_DATA_R=0x0F;
              SysTick_Wait(40);//retardo de 10 us
              //SysTick_Wait(20);//retardo de 5 us

              GPIO_PORTN_DATA_R=0x00;
              //SysTick_Wait(8);//retardo de 2 us
              //SysTick_Wait(4000000);//retardo de 1 s

              //SysTick_Wait(400000);//retardo de 100 ms
              SysTick_Wait(2000000);//retardo de 500 ms


              cuenta=0;*/



             /*
              GPIO_PORTN_DATA_R=0x01;
                           //SysTick_Wait(40);//retardo de 10 us
              SysTick_Wait(4000000);//retardo de 1 s
                           GPIO_PORTN_DATA_R=0x00;
                           //SysTick_Wait(4000000);//retardo de 1 s

                           //SysTick_Wait(400000);//retardo de 100 ms
                           SysTick_Wait(4000000);//retardo de 1 s
                           cuenta=0;
                     */







        }
        while(1);

}

