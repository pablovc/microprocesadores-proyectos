/*
 * main.c
 */
#include "inc/tm4c1294ncpdt.h"

typedef long (uint32_t);

uint32_t limpia;
int main(void) {

    SYSCTL_RCGCGPIO_R |= 0x1000; // Habilita reloj puerto N
    // Espera a que est� listo el puerto
    while((SYSCTL_PRGPIO_R & 0x1000)==0);
    GPIO_PORTN_DIR_R |= 0x03;   // pines 0 y 1 de p uerto N
    GPIO_PORTN_DEN_R = 0x03;
    GPIO_PORTN_DATA_R = 0x00;

    NVIC_ST_RELOAD_R = 2000000; //Cuenta para 500 ms
    NVIC_ST_CTRL_R = 0x01; //CLK= PIOSC/4, Habilita Cont sin ints.

    while (1)
    {
        while((NVIC_ST_CTRL_R &0x10000)==0);
         GPIO_PORTN_DATA_R ^= 0x01;
        limpia = NVIC_ST_CTRL_R;
    }

	
	return 0;
}
