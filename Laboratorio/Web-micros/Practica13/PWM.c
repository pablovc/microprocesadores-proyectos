// period is 16-bit number of PWM clock cycles in one period (3<=period)
// period for PF0 and PF1 must be the same
// duty is number of PWM clock cycles output is high  (2<=duty<=period-1)
// PWM clock rate = processor clock rate/SYSCTL_RCC_PWMDIV
//                = BusClock/2
//                = 16 MHz/2 = 8 MHz (in this example)

#include"tm4c1294ncpdt.h"
void PWM0A_Duty(uint16_t duty){
  PWM0_0_CMPA_R = duty - 1;             // 8) count value when output rises
}

void PWM0A_Init(uint16_t period, uint16_t duty){
	  SYSCTL_RCGCPWM_R |= SYSCTL_RCGCPWM_R0;   // 1) habilita reloj para PWM0
	  while((SYSCTL_PRPWM_R & SYSCTL_PRPWM_R0) == 0);
	  SYSCTL_RCGCGPIO_R |= SYSCTL_RCGCGPIO_R5; // 2) habilita reloj para Puerto F
	  while((SYSCTL_PRGPIO_R&SYSCTL_PRGPIO_R5) == 0);

	  GPIO_PORTF_AFSEL_R |= 0x01;           // 3) habilita funci�n alterna de PF0
	  GPIO_PORTF_DEN_R |= 0x01;             //    habilita modo digital PF0
	                                        // 4) configure PF0 as PWM0
	  GPIO_PORTF_PCTL_R = (GPIO_PORTF_PCTL_R&0xFFFFFFF0)+0x00000006; // la funci�n alterna es PWM
	  GPIO_PORTF_AMSEL_R &= ~0x01;          // deshabilita modo analogico de PF0 (default)

	  PWM0_CC_R |= PWM_CC_USEPWM;           // 5) usar divisor de PWM
	  PWM0_CC_R &= ~PWM_CC_PWMDIV_M;        //    limpia valor divisor de PWM
	  PWM0_CC_R += PWM_CC_PWMDIV_2;         //    valor de divisor= /2
	  PWM0_0_CTL_R = 0;                     // 6) reescribe modo de decremento
	  PWM0_0_GENA_R = (PWM_0_GENA_ACTCMPAD_ONE|PWM_0_GENA_ACTLOAD_ZERO);
	  // PF0 goes low(ZERO) on Count=LOAD
	  // PF0 goes high(ONE) on Count=CMPA down
	  PWM0_0_LOAD_R = period - 1;           // 7) No. de decrementos hasta 0
	  PWM0_0_CMPA_R = duty - 1;             // 8) No. de cuentas para el estado alto
	  PWM0_0_CTL_R |= PWM_0_CTL_ENABLE;     // 9) Iniciar PWM0
	  PWM0_ENABLE_R |= PWM_ENABLE_PWM0EN;   // 10) habilitar salida del PWM0A/PF0
}
void PWM0B_Init(uint16_t period, uint16_t duty){
  SYSCTL_RCGCPWM_R |= SYSCTL_RCGCPWM_R0;    // 1) habilita reloj para PWM0
  while((SYSCTL_PRPWM_R&SYSCTL_PRPWM_R0) == 0);
  SYSCTL_RCGCGPIO_R |= SYSCTL_RCGCGPIO_R5;  // 2) habilita reloj para Puerto F
  while((SYSCTL_PRGPIO_R&SYSCTL_PRGPIO_R5) == 0);

  GPIO_PORTF_AFSEL_R |= 0x10;           // 3) habilita funci�n alterna de PF1
  GPIO_PORTF_DEN_R |= 0x10;             //    habilita modo digital PF1
                                        // 4) 4) configure PF1 as PWM0
  GPIO_PORTF_PCTL_R = (GPIO_PORTF_PCTL_R&0xFFFFFF0F)+0x00000060; // la funci�n alterna es PWM
  GPIO_PORTF_AMSEL_R &= ~0x10;          //    deshabilita modo analogico de PF0 (default)

  PWM0_CC_R |= PWM_CC_USEPWM;           // 5) usar divisor de PWM
  PWM0_CC_R &= ~PWM_CC_PWMDIV_M;        //    limpia valor divisor de PWM
  PWM0_CC_R += PWM_CC_PWMDIV_2;         //    valor de divisor= /2
  PWM0_0_CTL_R = 0;                     // 6) reescribe modo de decremento
  PWM0_0_GENB_R = (PWM_0_GENB_ACTCMPBD_ONE|PWM_0_GENB_ACTLOAD_ZERO);
  // PF1 goes low on LOAD
  // PF1 goes high on CMPB down
  PWM0_0_LOAD_R = period - 1;           // 7) No. de decrementos hasta 0
  PWM0_0_CMPB_R = duty - 1;             // 8) No. de cuentas para el estado alto
  PWM0_0_CTL_R |= PWM_0_CTL_ENABLE;     // 9) Iniciar PWM0
  PWM0_ENABLE_R |= PWM_ENABLE_PWM1EN;   // 10) habilitar salida del PWM0B/PF1
}

// Cambia el ciclo de trabajo para PWM0B/PF1
// duty is number of PWM clock cycles output is high  (2<=duty<=period-1)
void PWM0B_Duty(uint16_t duty){
  PWM0_0_CMPB_R = duty - 1;             // 8) ciclos en nivel alto
}
void dummy_delay(void){
	unsigned int i;
	for(i=0; i<100; i++);
}

int main(void) {

	volatile unsigned int i;
	
	PWM0A_Init(60000, 45000);        // initialize PWM0A/PF0,  75% duty
	PWM0B_Init(60000, 15000);        // initialize PWM0B/PF1,  25% duty

	//  PWM0A_Duty(6000);    // 10%
	//  PWM0A_Duty(15000);   // 25%
	//  PWM0A_Duty(45000);   // 75%

	//  PWM0A_Init(6000, 3000);           // initialize PWM0A/PF0, 10000 Hz, 50% duty
	//  PWM0A_Init(1500, 1350);           // initialize PWM0A/PF0, 40000 Hz, 90% duty
	//  PWM0A_Init(1500, 225);            // initialize PWM0A/PF0, 40000 Hz, 15% duty
	//  PWM0A_Init(60, 30);               // initialize PWM0A/PF0, 1 MHz, 50% duty


	while(1){

		for(i=1; i<45000; i++){
			dummy_delay();
			PWM0A_Duty(i);
		}
	}
}
