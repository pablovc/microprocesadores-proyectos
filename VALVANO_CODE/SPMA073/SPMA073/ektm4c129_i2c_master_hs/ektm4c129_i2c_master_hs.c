//*****************************************************************************
//
// ektm4c129_i2c_master_hs.c - I2C Master with High Speed Data Transfer
//
// Copyright (c) 2013-2015 Texas Instruments Incorporated.  All rights reserved.
// Software License Agreement
// 
// Texas Instruments (TI) is supplying this software for use solely and
// exclusively on TI's microcontroller products. The software is owned by
// TI and/or its suppliers, and is protected under applicable copyright
// laws. You may not combine this software with "viral" open-source
// software in order to form a larger program.
// 
// THIS SOFTWARE IS PROVIDED "AS IS" AND WITH ALL FAULTS.
// NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT
// NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. TI SHALL NOT, UNDER ANY
// CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
// DAMAGES, FOR ANY REASON WHATSOEVER.
// 
// This is part of revision 2.1.1.71 of the EK-TM4C1294XL Firmware Package.
//
//*****************************************************************************
#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_types.h"
#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "driverlib/gpio.h"
#include "driverlib/i2c.h"
#include "driverlib/interrupt.h"
#include "driverlib/pin_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/uart.h"
#include "utils/random.h"
#include "utils/uartstdio.h"

//*****************************************************************************
//
//! \addtogroup example_list
//! <h1>ektm4c129_i2c_master_hs (ektm4c129_i2c_master_hs)</h1>
//!
//! A Simple I2C Master Code for Performing Data Transfer with a high speed
//! capable Slave using Interrupts
//
//*****************************************************************************

//*****************************************************************************
//
// Define for I2C Module
//
//*****************************************************************************
#define HS_PREAMBLE       0x07
#define SLAVE_ADDRESS_EXT 0x50
#define NUM_OF_I2CBYTES   255

//*****************************************************************************
//
// Enumerated Data Types for Master State Machine
//
//*****************************************************************************
enum I2C_MASTER_STATE
{
	I2C_HS_IDLE = 0,
	I2C_HS_PREAMBLE,
	I2C_HS_TX,
	I2C_HS_RX,
	I2C_HS_STOP,
	I2C_ERR_STATE
};

//*****************************************************************************
//
// Global variable for Delay Count
//
//*****************************************************************************
volatile uint8_t  g_ui8MasterTxData[NUM_OF_I2CBYTES];
volatile uint8_t  g_ui8MasterRxData[NUM_OF_I2CBYTES];
volatile uint32_t g_ui8ReadCount	= 0;
volatile uint32_t g_ui8WriteCount	= 0;
volatile uint8_t  g_ui8MasterCurrState;
volatile uint8_t  g_ui8MasterPrevState;
volatile bool     g_bI2CDirection;
volatile uint8_t  g_ui8MasterBytes  	 = NUM_OF_I2CBYTES;
volatile uint8_t  g_ui8MasterBytesLength = NUM_OF_I2CBYTES;

//*****************************************************************************
//
// Interrupt Handler for I2C Master Interface
//
//*****************************************************************************
void
I2C2IntHandler(void)
{
	uint32_t ui32I2CMasterInterruptStatus;

	//
	// Toggle PL4 High to Indicate Entry to ISR
	//
	GPIOPinWrite(GPIO_PORTL_BASE, GPIO_PIN_4, GPIO_PIN_4);

	//
	// Get the masked interrupt status and clear the flags
	//
	ui32I2CMasterInterruptStatus = I2CMasterIntStatusEx(I2C2_BASE, true);
	I2CMasterIntClearEx(I2C2_BASE, ui32I2CMasterInterruptStatus);

	//
	// Execute the State Machine
	//
	switch (g_ui8MasterCurrState) {
	case I2C_HS_IDLE:
		//
		// Move from IDLE to PREAMBLE State
		//
		g_ui8MasterPrevState = g_ui8MasterCurrState;
		g_ui8MasterCurrState = I2C_HS_PREAMBLE;

		//
		// Send High Speed Preamble on the Bus
		//
		I2CMasterSlaveAddrSet(I2C2_BASE, HS_PREAMBLE, true);
		I2CMasterControl(I2C2_BASE, I2C_MASTER_CMD_HS_MASTER_CODE_SEND);
		break;
	case I2C_HS_PREAMBLE:

		//
		// If Preamble is NAK then check direction flag and
		// move to TX or RX States
		//
		if(ui32I2CMasterInterruptStatus & I2C_MASTER_INT_NACK)
		{
			//
			// Move the current state to the previous state
			//
			g_ui8MasterPrevState = g_ui8MasterCurrState;

			if(!g_bI2CDirection)
			{
				g_ui8MasterCurrState = I2C_HS_TX;

				//
				// Send the Slave Address with RnW as Transmit
				// and First Data Byte. Based on Number of bytes the
				// command would be either START or FINISH
				//
				I2CMasterSlaveAddrSet(I2C2_BASE, SLAVE_ADDRESS_EXT, false);
				I2CMasterDataPut(I2C2_BASE, g_ui8MasterTxData[g_ui8MasterBytes++]);
				if(g_ui8MasterBytes == g_ui8MasterBytesLength)
				{
					I2CMasterControl(I2C2_BASE, I2C_MASTER_CMD_SINGLE_SEND);
				}
				else
				{
					I2CMasterControl(I2C2_BASE, I2C_MASTER_CMD_BURST_SEND_START);
				}
			}
			else
			{
				g_ui8MasterCurrState = I2C_HS_RX;

				//
				// Send the Slave Address with RnW as Receive. If only byte is
				// to be received then send FINISH else send START
				//
				I2CMasterSlaveAddrSet(I2C2_BASE, SLAVE_ADDRESS_EXT, true);
				if(g_ui8MasterBytesLength == 1)
				{
					I2CMasterControl(I2C2_BASE, I2C_MASTER_CMD_SINGLE_RECEIVE);
				}
				else
				{
					I2CMasterControl(I2C2_BASE, I2C_MASTER_CMD_BURST_RECEIVE_START);
				}
			}
		}
		//
		// if ACK then go to error state and log prev state
		//
		else
		{
			g_ui8MasterPrevState = g_ui8MasterCurrState;
			g_ui8MasterCurrState = I2C_ERR_STATE;
		}

		break;
	case I2C_HS_TX:
		//
		// Move the current state to the previous state
		// Else continue with the transmission till last byte
		//
		g_ui8MasterPrevState = g_ui8MasterCurrState;

		//
		// If Address or Data has been NAK'ed then go to stop state
		// If a Stop condition is seen due to number of bytes getting
		// done then move to STOP state
		//
		if(ui32I2CMasterInterruptStatus & I2C_MASTER_INT_NACK)
		{
			g_ui8MasterCurrState = I2C_HS_STOP;
		}
		else if(ui32I2CMasterInterruptStatus & I2C_MASTER_INT_STOP)
		{
			g_ui8MasterCurrState = I2C_HS_STOP;
		}
		else
		{
			I2CMasterDataPut(I2C2_BASE, g_ui8MasterTxData[g_ui8MasterBytes++]);
			if(g_ui8MasterBytes == g_ui8MasterBytesLength)
			{
				I2CMasterControl(I2C2_BASE, I2C_MASTER_CMD_BURST_SEND_FINISH);
			}
			else
			{
				I2CMasterControl(I2C2_BASE, I2C_MASTER_CMD_BURST_SEND_CONT);
			}

		}

		break;
	case I2C_HS_RX:
		//
		// Move the current state to the previous state
		// Else continue with the transmission till last byte
		//
		g_ui8MasterPrevState = g_ui8MasterCurrState;

		//
		// If Address has been NAK'ed then go to stop state
		// If a Stop condition is seen due to number of bytes getting
		// done then move to STOP state and read the last data byte
		//
		if(ui32I2CMasterInterruptStatus & I2C_MASTER_INT_NACK)
		{
			g_ui8MasterCurrState = I2C_HS_STOP;
		}
		else if(ui32I2CMasterInterruptStatus & I2C_MASTER_INT_STOP)
		{
			g_ui8MasterCurrState = I2C_HS_STOP;
			g_ui8MasterRxData[g_ui8MasterBytes++] = I2CMasterDataGet(I2C2_BASE);
		}
		else
		{
			//
			// Read the byte from I2C Buffer and decrement the number
			// of bytes counter to see if end has been reached or not
			//
			g_ui8MasterRxData[g_ui8MasterBytes++] = I2CMasterDataGet(I2C2_BASE);

			//
			// If end then NAK the byte and put Stop. Else continue
			// with ACK of the current byte and receive the next byte
			//
			if(g_ui8MasterBytes == g_ui8MasterBytesLength)
			{
				//
				// Do Nothing... The Transaction is over.
				//
				g_ui8MasterCurrState = I2C_HS_STOP;
			}
			else if(g_ui8MasterBytes == (g_ui8MasterBytesLength - 1))
			{
				I2CMasterControl(I2C2_BASE, I2C_MASTER_CMD_BURST_RECEIVE_FINISH);
			}
			else
			{
				I2CMasterControl(I2C2_BASE, I2C_MASTER_CMD_BURST_RECEIVE_CONT);
			}
		}

		break;
	case I2C_HS_STOP:
		//
		// Move the current state to the previous state
		// Else continue with the transmission till last byte
		//
		g_ui8MasterPrevState = g_ui8MasterCurrState;

		break;
	case I2C_ERR_STATE:
		g_ui8MasterCurrState = I2C_ERR_STATE;
		break;
	default:
		g_ui8MasterCurrState = I2C_ERR_STATE;
		break;
	}

	//
	// Toggle PL4 Low to Indicate Exit from ISR
	//
	GPIOPinWrite(GPIO_PORTL_BASE, GPIO_PIN_4, 0x0);
}

//*****************************************************************************
//
// This function sets up UART0 to be used for a console to display information
// as the example is running.
//
//*****************************************************************************
void
InitConsole(void)
{
    //
    // Enable GPIO port A which is used for UART0 pins.
    // TODO: change this to whichever GPIO port you are using.
    //
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);

    //
    // Configure the pin muxing for UART0 functions on port A0 and A1.
    // This step is not necessary if your part does not support pin muxing.
    // TODO: change this to select the port/pin you are using.
    //
    GPIOPinConfigure(GPIO_PA0_U0RX);
    GPIOPinConfigure(GPIO_PA1_U0TX);

    //
    // Enable UART0 so that we can configure the clock.
    //
    SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);

    //
    // Use the internal 16MHz oscillator as the UART clock source.
    //
    UARTClockSourceSet(UART0_BASE, UART_CLOCK_PIOSC);

    //
    // Select the alternate (UART) function for these pins.
    // TODO: change this to select the port/pin you are using.
    //
    GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);

    //
    // Initialize the UART for console I/O.
    //
    UARTStdioConfig(0, 115200, 16000000);
}

//*****************************************************************************
//
// Main Program to Configure the I2C Master
//
//*****************************************************************************
int
main(void)
{
	uint32_t ui32SysClock;
	uint8_t  ui8Count, ui8ByteToCheck;
	bool     bError;

    //
    // Set up the serial console to use for displaying messages.  This is
    // just for this example program and is not needed for EPI operation.
    //
    InitConsole();

    //
    // Display the setup on the console.
    //
    UARTprintf("\033[2J\033[H");
    UARTprintf("\r\nExample Code for I2C Master with");
    UARTprintf("\nInterrupt and HS Data Transfer\n\n");

	//
	// Enable GPIO for Configuring the I2C Interface Pins
	//
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOL);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOK);

	//
	// Wait for the Peripheral to be ready for programming
	//
	while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOL)
			|| !SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOK));

	//
	// Configure Pins for I2C2 Master Interface
	//
	GPIOPinConfigure(GPIO_PL1_I2C2SCL);
	GPIOPinConfigure(GPIO_PL0_I2C2SDA);
	GPIOPinTypeI2C(GPIO_PORTL_BASE, GPIO_PIN_0);
	GPIOPinTypeI2CSCL(GPIO_PORTL_BASE, GPIO_PIN_1);

	//
	// Configure Pins for I2C3 Slave Interface
	//
	GPIOPinConfigure(GPIO_PK4_I2C3SCL);
	GPIOPinConfigure(GPIO_PK5_I2C3SDA);
	GPIOPinTypeI2C(GPIO_PORTK_BASE, GPIO_PIN_5);
	GPIOPinTypeI2CSCL(GPIO_PORTL_BASE, GPIO_PIN_4);

	//
	// Configure GPIO Pin PL4 for Interrupt Time Processing
	//
	GPIOPinTypeGPIOOutput(GPIO_PORTL_BASE, GPIO_PIN_4);
	GPIOPinWrite(GPIO_PORTL_BASE, GPIO_PIN_4, 0x0);

    //
    // Setup System Clock for 120MHz
    //
	ui32SysClock = SysCtlClockFreqSet((SYSCTL_OSC_MAIN | SYSCTL_USE_PLL | SYSCTL_XTAL_25MHZ |
                        SYSCTL_CFG_VCO_480), 120000000);

	//
	// Stop the Clock, Reset and Enable I2C Module
	// in Master Function
	//
	SysCtlPeripheralDisable(SYSCTL_PERIPH_I2C2);
	SysCtlPeripheralReset(SYSCTL_PERIPH_I2C2);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_I2C2);

	//
	// Wait for the Peripheral to be ready for programming
	//
	while(!SysCtlPeripheralReady(SYSCTL_PERIPH_I2C2));

	//
	// Initialize and Configure the Master Module
	//
	I2CMasterInitExpClk(I2C2_BASE, ui32SysClock, false);

	//
	// Enable Interrupts for Arbitration Lost, Stop, NAK, Clock Low
	// Timeout and Data.
	//
	I2CMasterIntEnableEx(I2C2_BASE, (I2C_MASTER_INT_ARB_LOST |
			I2C_MASTER_INT_STOP | I2C_MASTER_INT_NACK |
			I2C_MASTER_INT_TIMEOUT | I2C_MASTER_INT_DATA));

	//
	// Enable the Interrupt in the NVIC from I2C Master
	//
	IntEnable(INT_I2C2);

	//
	// Enable the Glitch Filter. Writting a value 0 will
	// disable the glitch filter
	// I2C_MASTER_GLITCH_FILTER_DISABLED
	// I2C_MASTER_GLITCH_FILTER_1
	// I2C_MASTER_GLITCH_FILTER_2 : Ideal Value when in HS Mode
	//                              for 120MHz clock
	// I2C_MASTER_GLITCH_FILTER_4
	// I2C_MASTER_GLITCH_FILTER_8 : Ideal Value when in Std,
	// 								Fast, Fast+ for 120MHz clock
	// I2C_MASTER_GLITCH_FILTER_16
	// I2C_MASTER_GLITCH_FILTER_32
	//
	I2CMasterGlitchFilterConfigSet(I2C2_BASE, I2C_MASTER_GLITCH_FILTER_2);

	//
	// Initialize and Configure the Master Module State Machine
	//
	g_ui8MasterCurrState = I2C_HS_IDLE;

	//
	// Check if the Bus is Busy or not
	//
	while(I2CMasterBusBusy(I2C2_BASE));

	//
	// Randomly Initialize the Transmit buffer and
	// set the receive buffer to 0xFF
	//
	for(ui8Count=0 ; ui8Count < NUM_OF_I2CBYTES ; ui8Count++)
	{
		g_ui8MasterTxData[ui8Count]   = RandomSeed() & 0xFE;

		//
		// Change the Random Value for the next
		// iteration..
		//
		RandomAddEntropy(RandomSeed());

		//
		// Init the receive buffers with the value
		// 0xFF
		//
		g_ui8MasterRxData[ui8Count]   = 0xFF;
	}

	//
	// Set Transmit Flag and Populate the Data Buffer
	// with 4 bytes
	//
	g_bI2CDirection 		= false;
	g_ui8MasterBytesLength 	= 32;
	ui8ByteToCheck			= g_ui8MasterBytesLength;
	g_ui8MasterBytes 		= 0;

	//
	// Print Message before sending data
	//
	UARTprintf("Transmit %d bytes to external Slave...\n\n",g_ui8MasterBytesLength);

	//
	// Trigger the Transfer using Software Interrupt
	//
	IntTrigger(INT_I2C2);
	while(g_ui8MasterCurrState != I2C_HS_STOP);
	g_ui8MasterCurrState = I2C_HS_IDLE;

	//
	// Set Receive Flag and Populate the Data Buffer
	// with 2 bytes
	//
	g_bI2CDirection 		= true;
	g_ui8MasterBytesLength 	= 1;
	g_ui8MasterBytes 		= 0;

	//
	// Print Message before receiving data
	//
	UARTprintf("Receiving %d bytes from external Slave...\n\n",g_ui8MasterBytesLength);

	//
	// Trigger the Transfer using Software Interrupt
	//
	IntTrigger(INT_I2C2);
	while(g_ui8MasterCurrState != I2C_HS_STOP);
	g_ui8MasterCurrState = I2C_HS_IDLE;

	//
	// Perform Data Integrity Check...
	//
	bError = false;
	if(g_ui8MasterRxData[0] != g_ui8MasterTxData[ui8ByteToCheck-1])
	{
		bError = true;
	}

	if(bError)
	{
		UARTprintf("Failure... Data Mismatch!!!\n");
	}
	else
	{
		UARTprintf("Success... Data Match!!!\n");
	}
	while(1);
}
