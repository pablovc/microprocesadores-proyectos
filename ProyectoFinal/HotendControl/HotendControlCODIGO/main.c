
/*Asignatura: Microcontroladores y microprocesadores.   *Semestre.
*Proyecto final
*Temas:
*->Convertidor AnalOgico Digital
*->AcciOn de control.
*Presenta: .
-------------------------------------------------------------------------------
*/

//DESCRIPCION
/*
*El Programa utiliza al ADC para obtener el valor de la se�al que entra a PE5
* La seNYal de control sale del pin PL0 puerto
Se hace un acondicionamiento con coeficientes del modelo Steinhart-Hart de la seNYal por software
A partir de la lectura se hace acciOn de control cerrando el lazo a trav�s de un transistor TIP 120
conectado a una fuente de 12V a 10A el cual calienta un elemento calefactable
a travEs de la lectura del termistor se puede sintonizar un setpoint con la salida digital
Los datos se pueden visualizar a travEs de una pantalla LCD que se comunica por protocolo I2C la cual
estA conectada al PuertoB
 */

// LIBRERIAS QUE UTILIZA EL PROGRAMA
#include <stdbool.h>
#include <stdint.h> // Biblioteca para aritmEtica
#include <math.h>
#include "inc/tm4c1294ncpdt.h" //Biblioteca Tiva Ware
#include "I2C_LCD.h"
#include <string.h>//memset cadenas en LCD

char fila[]="       grados[C]";//fila LCD a introducir valores

//REGISTROS DE RELOJ
#define SYSCTL_RCGCGPIO_R       (*((volatile uint32_t *)0x400FE608)) //Reloj del puerto
#define SYSCTL_RCGCI2C_R        (*((volatile uint32_t *)0x400FE620)) //Reloj de I2C
#define SYSCTL_PRGPIO_R        (*((volatile uint32_t *)0x400FEA08)) //Bandera de "Peripherial Ready"

//REGISTROS DEL PUERTO B
#define GPIO_PORTB_DATA_R   (*((volatile uint32_t *)0x400593FC)) //Para los datos del puerto
#define GPIO_PORTB_DIR_R    (*((volatile uint32_t *)0x40059400)) //Para seleccionar funciOn
#define GPIO_PORTB_AFSEL_R  (*((volatile uint32_t *)0x40059420)) //Para seleccionar funciOn alterna
#define GPIO_PORTB_ODR_R    (*((volatile uint32_t *)0x4005950C)) //Para activar el Open Drain
#define GPIO_PORTB_DEN_R    (*((volatile uint32_t *)0x4005951C)) //Para activar funci�n digital
#define GPIO_PORTB_PCTL_R   (*((volatile uint32_t *)0x4005952C)) //Para el control del puerto

//REGISTROS DEL MOUDLO I2C
#define I2C0_MSA_R              (*((volatile uint32_t *)0x40020000)) //I2C Master Slave Adress
#define I2C0_MCS_R              (*((volatile uint32_t *)0x40020004)) //I2C Master Control Status
#define I2C0_MDR_R              (*((volatile uint32_t *)0x40020008)) //I2C Master Data Register
#define I2C0_MTPR_R             (*((volatile uint32_t *)0x4002000C)) //I2C Master Time Period
#define I2C0_MCR_R              (*((volatile uint32_t *)0x40020020)) //I2C Master Configuration Register

//PUERTO E Lectura Analogica
#define GPIO_PORTE_AHB_DIR_R    (*((volatile uint32_t *)0x4005C400))
#define GPIO_PORTE_AHB_AFSEL_R  (*((volatile uint32_t *)0x4005C420))
#define GPIO_PORTE_AHB_DEN_R    (*((volatile uint32_t *)0x4005C51C))
#define GPIO_PORTE_AHB_AMSEL_R  (*((volatile uint32_t *)0x4005C528))

#define SYSCTL_RCGCGPIO_R       (*((volatile uint32_t *)0x400FE608)) //Registro para habilitar reloj de GPIO
#define SYSCTL_PRGPIO_R         (*((volatile uint32_t *)0x400FEA08)) // Registro para verificar si el reloj esta listo (p.499)
#define SYSCTL_RCGCADC_R        (*((volatile uint32_t *)0x400FE638)) // Registro para habilitar el reloj al ADC(p. 396)
#define SYSCTL_PRADC_R          (*((volatile uint32_t *)0x400FEA38)) // Registro para verificar si el ADC esta listo (p.515)
//REGISTROS ADC 0
#define ADC0_PC_R               (*((volatile uint32_t *)0x40038FC4)) // Registro para configurar tasa de muestreo (p.1159)
#define ADC0_SSPRI_R            (*((volatile uint32_t *)0x40038020)) // Registro para configurar la prioridad del secuenciador (p.1099)
#define ADC0_ACTSS_R            (*((volatile uint32_t *)0x40038000)) // Registro para controlar la activaci�n del secuenciador (p. 1076)
#define ADC0_EMUX_R             (*((volatile uint32_t *)0x40038014)) // ACDEMUX Registro para seleccionar el evento (trigger) que inicia el muestreo en cada secuenciador (p.1091)
#define ADC0_IM_R               (*((volatile uint32_t *)0x40038008)) // Registro que controla la mascara de interrupciones en secuenciadores (p. 1081)
#define ADC0_ACTSS_R            (*((volatile uint32_t *)0x40038000)) // Registro que controla la activaci�n de los secuenciadores (p.1077)
#define ADC0_ISC_R              (*((volatile uint32_t *)0x4003800C)) //Registro de estatus y para borrar las condiciones de interrupci�n del secuenciador (p. 1084)
#define ADC0_PSSI_R             (*((volatile uint32_t *)0x40038028)) //Registro que permite al software iniciar el muestreo en los secuenciadores (p. 1102)
#define ADC0_RIS_R              (*((volatile uint32_t *)0x40038004)) //Registro muestra el estado de la se�al de interrupci�n de cada secuenciador (p.1079  )

//ADC 0 input 1
#define ADC0_SSEMUX3_R          (*((volatile uint32_t *)0x400380B8)) // Registro que selecciona entre las entradas AIN[19:16] o AIN[15:0]  (p.1046)
#define ADC0_SSMUX3_R           (*((volatile uint32_t *)0x400380A0)) // Registro para configurar la entrada anal�gica para el Secuenciador 3 (p.1041)
#define ADC0_SSCTL3_R           (*((volatile uint32_t *)0x400380A4)) // Registro que configura la muestra ejecutada con el Secuenciador 3 (p.1142)
#define ADC0_SSFIFO3_R          (*((volatile uint32_t *)0x400380A8)) //Registro que contiene los resultados de conversi�n de las muestras recogidas con el secuenciador (p. 1118)

//PLL
#define SYSCTL_PLLFREQ0_R       (*((volatile uint32_t *)0x400FE160)) //Registro para configurar el PLL
#define SYSCTL_PLLSTAT_R        (*((volatile uint32_t *)0x400FE168)) //Registro muestra el estado de encendido del PLL
#define SYSCTL_PLLFREQ0_PLLPWR  0x00800000  // Valor para encender el PLL

//Valor de lectura de termistor
volatile uint32_t LECTURA;
volatile uint32_t cuenta,Termino= 0;//variables de tiempo por timer

int Vo;
float R1 = 100000;              // resistencia fija del divisor de tension
float logR2, R2, TEMPERATURA,TEMP0;
float c1 = 2.114990448e-03, c2 = 0.3832381228e-04, c3 = 5.228061052e-07;

// coeficientes de S-H en pagina:
// http://www.thinksrs.com/downloads/programs/Therm%20Calc/NTCCalibrator/NTCcalculator.htm

// ******* FUNCIONES DE PROGRAMA ********
void clearScreen(){
    //Borra la pantalla
            LCD_Set_Cursor(1, 1);//cursor a coordenada
            LCD_Write_String("                ");//16 espacios en blanco
            LCD_Set_Cursor(2, 1);//cursor a coordenada
            LCD_Write_String("                ");//16 espacios en blanco
}

//*** FunciOn de conversiOn de valores en caracteres para desplegar en cadena de caracteres en LCD ***
void conVal(int valor,int corr,int base){
    int unidades=valor%base;
    int decenas=(valor-unidades)/base;

    switch (decenas){
        case 0 :   memset (fila+corr,'0',1);
            break;
        case 1 :  memset (fila+corr,'1',1);
            break;
        case 2 :  memset (fila+corr,'2',1);
            break;
        case 3 :  memset (fila+corr,'3',1);
            break;
        case 4 :  memset (fila+corr,'4',1);
                    break;
        case 5 :  memset (fila+corr,'5',1);
                    break;
        case 6 :  memset (fila+corr,'6',1);
                    break;
        case 7 :  memset (fila+corr,'7',1);
                            break;
        case 8 :  memset (fila+corr,'8',1);
                                    break;
        case 9 :  memset (fila+corr,'9',1);
                                            break;
        default:  memset (fila+corr,'?',1);
            break;
    }

    switch (unidades){
           case 0 :  memset (fila+1+corr,'0',1);
               break;
           case 1 :  memset (fila+1+corr,'1',1);
               break;
           case 2 :   memset (fila+1+corr,'2',1);
               break;
           case 3 :   memset (fila+1+corr,'3',1);
               break;
           case 4 :   memset (fila+1+corr,'4',1);
               break;
           case 5 :   memset (fila+1+corr,'5',1);
                         break;
           case 6 :   memset (fila+1+corr,'6',1);
                         break;
           case 7 :   memset (fila+1+corr,'7',1);
                         break;
           case 8 :   memset (fila+1+corr,'8',1);
                         break;
           case 9 :   memset (fila+1+corr,'9',1);
                                    break;

       }
 }

// RUTINA DE SERVICIO DE INTERRUPCI�N
void Timer03AIntHandler(void)
{
   TIMER3_ICR_R= 0X00000001 ; //LIMPIA BANDERA DE TIMER3
   Termino=Termino+1;
       if (Termino < 1024)
        {
           if(Termino==500){
               cuenta=cuenta+1;
           }

        }else{
            Termino=0;
        }
}//FIN TIMER 3A

void main(void){


    I2C_Init(); //FunciOn que inicializa los relojes, el GPIO y el I2C0

       //Inicializo Slave
       while(I2C0_MCS_R&0x00000001){}; // espera que el I2C estE listo
       LCD_Init();//inicializa pantalla LCD 16x2 con comunicaciOn I2C

       SysTick_Init();//inicia servicio de retraso por frecuencia

       LCD_Set_Cursor(1, 1);
           LCD_Write_String("Control         ");
           LCD_Set_Cursor(2, 1);
           LCD_Write_String("Hotend         ");
           SysTick_Wait(4000000);//espera 1S

                  clearScreen();//funcion borra pantalla
                  LCD_Set_Cursor(1, 1);
                   LCD_Write_String("              ");
                   LCD_Set_Cursor(2, 1);
                   LCD_Write_String("grados C     ");

    SYSCTL_RCGCGPIO_R |= 0X0412; // 1) HABILITA RELOJ PARA EL PUERTOS B,E,L  (p. 382)
    SYSCTL_RCGCTIMER_R |= 0X08; //RELOJ Y HABILITA TIMER 3 (p.380)
    //retardo para que el reloj alcance los puertos  Y TIMER 3
    while ((SYSCTL_PRGPIO_R & 0X0412) == 0){};  // reloj listo?
    TIMER3_CTL_R=0X00000000; //DESHABILITA TIMER 3 PARA CONFIGURAR (p.986)
    TIMER3_CFG_R= 0X00000000; //CONFIGURA TIMER DE 32 BITS (p. 976)
    //TIMER3_TAMR_R= 0X00000002; //CONFIGURAR PARA MODO PERIODICO CUENTA HACIA ABAJO (p. 977)
    TIMER3_TAILR_R= 0X00007530; // VALOR DE RECARGA (p.1004)
    TIMER3_TAPR_R= 0X00; // PRESCALADOR DE TIMER A, SOLO PARA MODOS DE 16 BITS (p.1008)
    TIMER3_ICR_R= 0X00000001 ; //LIMPIA POSIBLE BANDERA PENDIENTE DE TIMER3 (p.1002)
    TIMER3_IMR_R |= 0X00000001; //ACTIVA INTRRUPCION DE TIMEOUT (p.993)
    NVIC_EN1_R= 1<<(35-32); //HABILITA LA INTERRUPCION 35 (TIMER3 A)
    TIMER3_CTL_R |= 0X00000001; //HABILITA TIMER 3 (p.986)

    // habilita al Puerto L como salida digital para control de motor
    // PL0,...,PL3 como salidas digitales
    GPIO_PORTL_DIR_R = 0x0F;//->F habilita los primeros 4 bits del puerto
    GPIO_PORTL_DEN_R = 0x0F;//(F->1111)
    GPIO_PORTL_DATA_R = 0x09;//9->1001

    //Lectura de termistor
    GPIO_PORTE_AHB_DIR_R = 0x00;    // 2) PE4 entrada (anal�gica) (Puerto E)
    GPIO_PORTE_AHB_AFSEL_R |= 0x10; // 3) Habilita Funci�n Alterna de PE4 (Puerto E)
    GPIO_PORTE_AHB_DEN_R = 0x00;    // 4) Deshabilita Funci�n Digital de PE4 (Puerto E)
    GPIO_PORTE_AHB_AMSEL_R |= 0x10; // 5) Habilita Funci�n Anal�gica de PE4 (Puerto E)

    SYSCTL_RCGCADC_R  = 0x01;   // 6) Habilita reloj para l�gica de ADC0
    while((SYSCTL_PRADC_R&0x01)==0);// Se espera a que el reloj se estabilice

    //ADC 0
    ADC0_PC_R = 0x01;       // 7) Configura para 125Ksamp/s
    ADC0_SSPRI_R = 0x0123;  // 8) SS3 con la m�s alta prioridad
    ADC0_ACTSS_R = 0x0000;  // 9) Deshabilita SS3 (SS2, SS1, SS0) antes de cambiar configuraci�n de registros
    ADC0_EMUX_R = 0x0000;   // 10) Se configura SS3 (SS2, SS1, SS0) para iniciar muestreo por software
    ADC0_SSEMUX3_R = 0x00;  // 11)Entradas AIN(15:0)

    //Lectura
    ADC0_SSMUX3_R = (ADC0_SSMUX3_R & 0xFFFFFFF0) + 8; // canal AIN8 (PE5)

    ADC0_SSCTL3_R = 0x0006; // 12) SI: AIN, Habilitaci�n de INR3, Fin de secuencia; No:muestra diferencial
    ADC0_SSCTL2_R = 0x0006; // 12) SI: AIN, Habilitaci�n de INR2, Fin de secuencia; No:muestra diferencial
    ADC0_IM_R = 0x0000;     // 13) Deshabilita interrupciones de SS0,SS1,SS2,SS3
    ADC0_ACTSS_R |= 0x000F; // 14) Habilita SS3, SS2, SS1, SS0 (p. 1077)

    SYSCTL_PLLFREQ0_R |= SYSCTL_PLLFREQ0_PLLPWR;    // encender PLL
    while((SYSCTL_PLLSTAT_R&0x01)==0);              // espera a que el PLL fije su frecuencia
    SYSCTL_PLLFREQ0_R &= ~SYSCTL_PLLFREQ0_PLLPWR;   // apagar PLL

    ADC0_ISC_R = 0x000F;                    // Se recomienda Limpiar la bandera RIS del ADC0

    SysTick_Init();//inicio de servicio de espera de tiempo por frecuencia
    clearScreen();

    for(;;){
      ADC0_PSSI_R = 0x000F;             // Inicia conversi�n de SS0, SS1 SS2 y SS3
      while ((ADC0_RIS_R & 0x08)==0);   // Espera a que SS3 termine conversi�n (polling)
      LECTURA = (ADC0_SSFIFO1_R & 0xFFF); // Resultado en FIFO1 se asigna a variable "LECTURA"
      ADC0_ISC_R = 0x000F;              // Limpia la bandera RIS del ADC0
      //rutina que hace onda cuadrada de 1Hz para hacer pruebas
      /*if((cuenta%2)==1){
         GPIO_PORTL_DATA_R=0x01;
      }else{
          GPIO_PORTL_DATA_R=0x00;
      }*/

      Vo = LECTURA;         // lectura de termistor en (PE5)
      //valor maximo de lectura analogica=4095
      R2 = R1 * ((4095 / (float)Vo) - 1.0); // conversion de tension a resistencia

       logR2 = log(R2);          // logaritmo de R2 necesario para ecuacion
       TEMPERATURA = (1.0 / (c1 + c2*logR2 + c3*logR2*logR2*logR2));     // ecuacion S-H
       TEMPERATURA = TEMPERATURA - 273.15;   // Kelvin a Centigrados (Celsius)
       SysTick_Wait(1000000);//espera 0.25 [s] entre lecturas

       TEMP0=(2*(int)TEMPERATURA);
       if(TEMP0>=38){
           GPIO_PORTL_DATA_R=0x00;
       }else if(TEMP0<38){
           GPIO_PORTL_DATA_R=0x01;
       }

       LCD_Set_Cursor(1, 1);
       LCD_Write_String("  Temperatura");
       LCD_Set_Cursor(2, 1);
       conVal(TEMP0,2,10);
       LCD_Set_Cursor(2, 1);
       LCD_Write_String(fila);
    }//FOR INFINITO

}
