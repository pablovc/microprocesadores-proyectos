#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include "inc/tm4c1294ncpdt.h"

// LCD COMMANDS
#define LCD_INS 2
#define LCD_DAT 3

void retardo(uint32_t wait){
    TIMER4_TAILR_R = wait - 1;                          // Cargando el valor de la cuenta
    TIMER4_ICR_R = 0x01;                                // Limpia bandera del Timer4
    TIMER4_CTL_R = 0x01;                                // Habilita el Timer4
    while((TIMER4_RIS_R & (TIMER_RIS_TATORIS)) == 0);   // Esperar a que termine la cuenta
}

void LCD_instruction(uint8_t data_K,int wait){
    GPIO_PORTG_AHB_DATA_R = LCD_INS;        // Entrarï¿½ una instrucciï¿½n a la LCD
    GPIO_PORTK_DATA_R = data_K;             // Entrada de instrucciï¿½n
    GPIO_PORTG_AHB_DATA_R = 0x00;
    retardo(wait);
}

void LCD_init(){
    SYSCTL_RCGCGPIO_R |= SYSCTL_RCGCGPIO_R6 | SYSCTL_RCGCGPIO_R9;               // Habilita PG y PK
    while((SYSCTL_PRGPIO_R & (SYSCTL_RCGCGPIO_R6 | SYSCTL_RCGCGPIO_R9)) == 0);  // Espera a que PG y PK esten listos para usarse

    SYSCTL_RCGCTIMER_R |= SYSCTL_RCGCTIMER_R4;                  // habilita reloj para Timer4
    while((SYSCTL_RCGCTIMER_R & SYSCTL_RCGCTIMER_R4) == 0);     // Espera a que el Timer4 estï¿½ listo para usarse

    TIMER4_CTL_R &= ~0x00000001;    // Deshabilitar el timer
    TIMER4_CFG_R = 0x00;            // Timer de 32 bits
    TIMER4_TAMR_R = 0x01;           // Modo ONE-SHOT

    GPIO_PORTG_AHB_DIR_R = 0x03;    // PG[0,1] como salidas
    GPIO_PORTG_AHB_DEN_R = 0x03;    // PG[0,1] funciones digitales

    GPIO_PORTK_DIR_R = 0xFF;        // PK[0,...,7] como salidas
    GPIO_PORTK_DEN_R = 0xFF;        // PK[0,...,7] funciones digitales

    LCD_instruction(0x38,640);      // ACTIVAR FUNCIï¿½N. Datos de 8 bits; Dos lï¿½neas;            retardo de 40us
    LCD_instruction(0x0C,640);      // ENCENDER DISPLAY. Encender pantalla; Activar cursor;     retardo de 40us
    LCD_instruction(0x01,24480);    // BORRAR PANTALLA.                                         retardo de 1.53ms
    LCD_instruction(0x06,640);      // SELECCIONAR MODO. Incremental;                           retardo de 40us
}

void LCD_write(const char* num, const char* sms, int n){
    static int i;
    static int j;

    // Para mostrar el nï¿½mero
    for(i=0;i<10;i++){
        GPIO_PORTG_AHB_DATA_R = LCD_DAT;    // Entrarï¿½ un valor para imprimir en la LCD
        GPIO_PORTK_DATA_R = num[i];         // Entrada del nï¿½mero a imprimir
        GPIO_PORTG_AHB_DATA_R = 0x00;
        retardo(688);                       // Retardo de 43us
    }
    LCD_instruction(0xC0,640);              // Posicionar el cursor al principio de la segunda lï¿½nea

    // Para mostrar el mensaje
    if(n<17){       // Mensajes que no requieren desplazamiento
        for(i=0;i<n;i++){
            GPIO_PORTG_AHB_DATA_R = LCD_DAT;        // Entrarï¿½ un valor para imprimir en la LCD
            GPIO_PORTK_DATA_R = sms[i];             // Entrada del mensaje de texto a imprimir
            GPIO_PORTG_AHB_DATA_R = 0x00;
            retardo(688);                           // Retardo de 43us
        }
    }else{          // Mensajes que requieren desplazamiento
        for(j=0;j<n-15;j++){
            for(i=j;i<j+16;i++){
                GPIO_PORTG_AHB_DATA_R = LCD_DAT;    // Entrarï¿½ un valor para imprimir en la LCD
                GPIO_PORTK_DATA_R = sms[i];         // Entrada del mensaje de texto a imprimir
                GPIO_PORTG_AHB_DATA_R = 0x00;
                retardo(688);                       // Retardo de 43us
            }
            LCD_instruction(0xC0,640);              // Posicionar el cursor al principio de la segunda lï¿½nea
            retardo(12000000);  //12000000          // Retardo de 0.75s
        }
    }
}

int main(void){
    LCD_init();                                         // Inicializar la LCD y el timer4
    const char num[]="5527163578";                      // Variable para guardar el nï¿½mero del celular que mandï¿½ el mensaje
    const char sms[]="Ya quedo la LCD Manuel !!! :D";   // Variable para guardar el mensaje de texto
    int n=29;                                           // Variable para guardar el nï¿½mero de caracteres que contiene el mensaje
    LCD_write(num,sms,n);                               // Escribir en la LCD
    while(1){}
}
