\select@language {spanish}
\contentsline {section}{\numberline {1}Sistemas el\IeC {\'e}ctricos}{1}
\contentsline {subsection}{\numberline {1.1}Leyes de Kirchhoff y circuitos de par\IeC {\'a}metros concentrados}{2}
\contentsline {subsubsection}{\numberline {1.1.1}Ecuaciones de Maxwell}{2}
\contentsline {subsection}{\numberline {1.2}Concepto de circuito el\IeC {\'e}ctrico}{2}
\contentsline {subsection}{\numberline {1.3}Leyes de Kirchhoff}{2}
\contentsline {subsubsection}{\numberline {1.3.1}Ley de corrientes de Kirchhoff}{3}
\contentsline {subsubsection}{\numberline {1.3.2}Ley de tensiones de Kirchhoff}{3}
\contentsline {subsection}{\numberline {1.4}Circuito de par\IeC {\'a}metros concentrados}{4}
\contentsline {subsubsection}{\numberline {1.4.1}Circuito el\IeC {\'e}ctrico de par\IeC {\'a}metros concentrados}{4}
\contentsline {section}{\numberline {2}Sinusoide}{4}
\contentsline {subsection}{\numberline {2.1}Caracter\IeC {\'\i }sticas}{4}
\contentsline {section}{\numberline {3}Sinusoide}{5}
\contentsline {subsection}{\numberline {3.1}Caracter\IeC {\'\i }sticas}{6}
