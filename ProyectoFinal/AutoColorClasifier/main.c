//CÓDIGO 22
// Programa utiliza al ADC0 entradas PE4, PE5 con el  SS2
// se dispara por software y dispara dos conversiones,
//espera por ellas para finalizar, y regersa los dos resultados.

#include <stdint.h>
#include <stdbool.h>
#include <math.h>

#include "inc/tm4c1294ncpdt.h" //Biblioteca Tiva Ware

#include "I2C_LCD.h"

//lectura analogica
volatile uint32_t dato1;
volatile uint32_t dato2;
// servo motor
volatile uint32_t d,fin,i,j,k;
volatile uint32_t cicloTrabajo;
volatile uint32_t retardo;
volatile uint32_t periodo=80000;
//frecuencImetro
volatile uint32_t Count= 0;
volatile uint32_t freq= 0;
volatile uint32_t inicio= 0;
volatile uint32_t fin= 0;

volatile uint32_t retlec=400000;//retraso de lecturas  100[ms]

volatile uint32_t Rojo_Frec;
volatile uint32_t Verde_Frec;
volatile uint32_t Azul_Frec;

//para el cubo de color gris

//Rojo_Frec  (valor medido de 765,850)
//Verde_Frec      (valor medido de 390,410)
//Azul_Frec         (valor medido de 496,530)



//para el cubo de color naranja

//Rojo_Frec   (valor medido de 1426,1405)
//Verde_Frec       (valor medido de 344,350)
//Azul_Frec         (valor medido de 496,460)


//para el cubo de color rosa

//Rojo_Frec   (valor medido de 1651,1553)
//Verde_Frec      (valor medido de 587,536)
//Azul_Frec         (valor medido de 883,822)


//para el cubo de color azul

//Rojo_Frec  (valor medido de 701)
//Verde_Frec       (valor medido de 433)
//Azul_Frec         (valor medido de 584)



//#########CARACTERIZACION DE COLORES

volatile uint32_t Rojo_Frec_gris_valor=800;
volatile uint32_t Verde_Frec_gris_valor=400;
volatile uint32_t Azul_Frec_gris_valor=500;
volatile uint32_t Gris_umbral=100;

volatile uint32_t Rojo_Frec_naranja_valor=1236;
volatile uint32_t Verde_Frec_naranja_valor=315;
volatile uint32_t Azul_Frec_naranja_valor=415;
volatile uint32_t Naranja_umbral=70;

volatile uint32_t Rojo_Frec_rosa_valor=1450;
volatile uint32_t Verde_Frec_rosa_valor=513;
volatile uint32_t Azul_Frec_rosa_valor=786;
volatile uint32_t Rosa_umbral=70;

volatile uint32_t Rojo_Frec_azul_valor=701;
volatile uint32_t Verde_Frec_azul_valor=433;
volatile uint32_t Azul_Frec_azul_valor=584;
volatile uint32_t Azul_umbral=100;



//motor a pasos
volatile uint32_t Count0= 0;
volatile uint32_t Termino= 0;
volatile uint32_t banderaMotor=0;
volatile uint32_t interval=10;
volatile uint32_t sentido=0;
volatile uint32_t limite=400000000;
volatile uint32_t inicio0= 0;
volatile uint32_t fin0= 0;

volatile uint32_t anguloS1= 0;
volatile uint32_t anguloS2= 0;
volatile uint32_t color= 0;


// DefiniciOn de constantes para operaciones
#define NVIC_ST_CTRL_COUNT      0x00010000  // bandera de cuenta flag
#define NVIC_ST_CTRL_CLK_SRC    0x00000004  // Clock Source
#define NVIC_ST_CTRL_INTEN      0x00000002  // Habilitador de interrupción
#define NVIC_ST_CTRL_ENABLE     0x00000001  // Modo del contador
#define NVIC_ST_RELOAD_M        0x00FFFFFF  // Valor de carga del contador

// Definición de constantes para operaciones
#define SYSCTL_RCGC2_GPION      0x00001000  // bit de estado del reloj de puerto N
#define SYSCTL_RCGC2_R          (*((volatile unsigned long *)0x400FE608)) // Registro de Habilitación de Reloj de Puertos

#define GPIO_PORTE_AHB_DIR_R    (*((volatile uint32_t *)0x4005C400))
#define GPIO_PORTE_AHB_AFSEL_R  (*((volatile uint32_t *)0x4005C420)) //Registro para habilitar funciones alternativas del de GPIO (p.770)
#define GPIO_PORTE_AHB_DEN_R    (*((volatile uint32_t *)0x4005C51C)) //Registro para habilitar funciones digital del de GPIO (p.781)
#define GPIO_PORTE_AHB_AMSEL_R  (*((volatile uint32_t *)0x4005C528))

//REGISTROS DE RELOJ
#define SYSCTL_RCGCGPIO_R       (*((volatile uint32_t *)0x400FE608)) //Reloj del puerto
#define SYSCTL_RCGCI2C_R        (*((volatile uint32_t *)0x400FE620)) //Reloj de I2C
#define SYSCTL_PRGPIO_R        (*((volatile uint32_t *)0x400FEA08)) //Bandera de "Peripherial Ready"

//REGISTROS DEL PUERTO B
#define GPIO_PORTB_DATA_R   (*((volatile uint32_t *)0x400593FC)) //Para los datos del puerto
#define GPIO_PORTB_DIR_R    (*((volatile uint32_t *)0x40059400)) //Para seleccionar funciOn
#define GPIO_PORTB_AFSEL_R  (*((volatile uint32_t *)0x40059420)) //Para seleccionar funciOn alterna
#define GPIO_PORTB_ODR_R    (*((volatile uint32_t *)0x4005950C)) //Para activar el Open Drain
#define GPIO_PORTB_DEN_R    (*((volatile uint32_t *)0x4005951C)) //Para activar función digital
#define GPIO_PORTB_PCTL_R   (*((volatile uint32_t *)0x4005952C)) //Para el control del puerto

//REGISTROS DEL MOUDLO I2C
#define I2C0_MSA_R              (*((volatile uint32_t *)0x40020000)) //I2C Master Slave Adress
#define I2C0_MCS_R              (*((volatile uint32_t *)0x40020004)) //I2C Master Control Status
#define I2C0_MDR_R              (*((volatile uint32_t *)0x40020008)) //I2C Master Data Register
#define I2C0_MTPR_R             (*((volatile uint32_t *)0x4002000C)) //I2C Master Time Period
//#define I2C0_MCR_R

#define SYSCTL_RCGCGPIO_R       (*((volatile uint32_t *)0x400FE608)) //Registro para habilitar reloj de GPIO
#define SYSCTL_PRGPIO_R         (*((volatile uint32_t *)0x400FEA08)) // Registro para verificar si el reloj esta listo (p.499)
#define SYSCTL_RCGCADC_R        (*((volatile uint32_t *)0x400FE638)) // Registro para habilitar el reloj al ADC(p. 396)
#define SYSCTL_PRADC_R          (*((volatile uint32_t *)0x400FEA38)) // Registro para verificar si el ADC esta listo (p.515)
#define ADC0_PC_R               (*((volatile uint32_t *)0x40038FC4)) // Registro para configurar tasa de muestreo (p.1159)
#define ADC0_SSPRI_R            (*((volatile uint32_t *)0x40038020)) // Registro para configurar la prioridad del secuenciador (p.1099)
#define ADC0_ACTSS_R            (*((volatile uint32_t *)0x40038000)) // Registro para controlar la activación del secuenciador (p. 1076)
#define ADC0_EMUX_R             (*((volatile uint32_t *)0x40038014)) // Registro para seleccionar el evento (trigger) que inicia el muestreo en cada secuenciador (p.1091)
#define ADC0_SSEMUX2_R          (*((volatile uint32_t *)0x40038098)) // Registro que selecciona entre las entradas AIN[19:16] o AIN[15:0]  (p.1137)
#define ADC0_SSMUX2_R           (*((volatile uint32_t *)0x40038080)) // Registro para configurar la entrada analógica para el Secuenciador 2 (p.1128)
#define ADC0_SSCTL2_R           (*((volatile uint32_t *)0x40038084)) // Registro que configura la muestra ejecutada con el Secuenciador 2 (p.1142)
#define ADC0_IM_R               (*((volatile uint32_t *)0x40038008)) // Registro que controla la mascara de interrupciones en secuenciadores (p. 1081)
#define ADC0_ACTSS_R            (*((volatile uint32_t *)0x40038000)) // Registro que controla la activación de los secuenciadores (p.1077)
#define ADC0_ISC_R              (*((volatile uint32_t *)0x4003800C)) //Registro de estatus y para borrar las condiciones de interrupción del secuenciador (p. 1084)
#define ADC0_PSSI_R             (*((volatile uint32_t *)0x40038028)) //Registro que permite al software iniciar el muestreo en los secuenciadores (p. 1102)
#define ADC0_RIS_R              (*((volatile uint32_t *)0x40038004)) //Registro muestra el estado de la señal de interrupción de cada secuenciador (p.1079)
#define ADC0_SSFIFO2_R          (*((volatile uint32_t *)0x40038088)) //Registro que contiene los resultados de conversión de las muestras recogidas con el secuenciador (p. 1118)

#define ADC0_SAC_R              (*((volatile uint32_t *)0x40038030)) //Registro de control de muestras a promediar (p. 1105)
#define ADC0_CTL_R              (*((volatile uint32_t *)0x40038038))
#define ADC0_SSOP2_R            (*((volatile uint32_t *)0x40038090))
#define ADC0_SSTSH2_R           (*((volatile uint32_t *)0x4003809C))

#define SYSCTL_PLLFREQ0_R       (*((volatile uint32_t *)0x400FE160)) //Registro para configurar el PLL
#define SYSCTL_PLLSTAT_R        (*((volatile uint32_t *)0x400FE168)) //Registro muestra el estado de encendido del PLL
#define SYSCTL_PLLFREQ0_PLLPWR  0x00800000  // Valor para encender el PLL

enum secMotores {
    AB=0x03,
    ApB=0x06,
    ApBp=0x0C,
    ABp=0x09,
    HIGH=0x01,
    DHIGH=0x02,
    LOW=0x00
};

//*********************************
//** RUTINA DE SERVICIO DE INTERRUPCIÓN ***
//**CONTADOR PARA LA FUNCION DE CUENTA POR FLANCO

void Timer03AIntHandler(void)
{
   TIMER3_ICR_R= 0X00000004 ; //LIMPIA BANDERA DE EVENTO DE CAPTURA
    Count = Count + 1;       // Cuenta el número de flancos de subida

    Termino = Termino + 1;
    // 32 * 64 = 2048
    //valor anterior 1024
     if(sentido==1){
           if (Termino < limite)
           {
          Count0 = Count0 + 0x01;


                 if((Count0>0)&&(Count0<interval)){
                      GPIO_PORTL_DATA_R=AB; // A,B
                      banderaMotor=1;
                 }else if((Count0>interval)&&(Count0<(interval*2))){
                      GPIO_PORTL_DATA_R=ApB; // A',B
                      banderaMotor=2;
                 }else if((Count0>(interval*2))&&(Count0<(interval*3))){
                     GPIO_PORTL_DATA_R=ApBp; // A', B'
                     banderaMotor=3;
                 }else if((Count0>(interval*3))&&(Count0<(interval*4))){
                     GPIO_PORTL_DATA_R=ABp; // A, B'
                     banderaMotor=4;
                 }else if(Count0>(interval*4)){
                      Count0 = 0x0;
                 }
                       //break;



               // }
            }else{
                GPIO_PORTL_DATA_R=0x00; //CEROS
            }
     }else if(sentido==2){
         if (Termino < limite)
                    {
                   Count0 = Count0 + 0x01;


                          if((Count0>0)&&(Count0<interval)){
                              GPIO_PORTL_DATA_R=ABp; // A, B'
                               banderaMotor=4;

                          }else if((Count0>interval)&&(Count0<(interval*2))){
                               GPIO_PORTL_DATA_R=ApB; // A',B
                               banderaMotor=2;
                               GPIO_PORTL_DATA_R=ApBp; // A', B'
                              banderaMotor=3;
                          }else if((Count0>(interval*2))&&(Count0<(interval*3))){
                              GPIO_PORTL_DATA_R=ApB; // A',B
                              banderaMotor=2;
                          }else if((Count0>(interval*3))&&(Count0<(interval*4))){
                              GPIO_PORTL_DATA_R=AB; // A,B
                              banderaMotor=1;
                          }else if(Count0>(interval*4)){
                               Count0 = 0x0;
                          }
                                //break;



                        // }
                     }else{
                         GPIO_PORTL_DATA_R=0x00; //CEROS
                     }
     }else if(sentido==0){
         GPIO_PORTL_DATA_R=0x00; //CEROS
     }




 }


//#############FUNCIONES DEL PROGRAMA##################

//funcion de flanco para FrecuencImetro
void pulseIn(){
    inicio=Count;
    SysTick_Wait(400000);//  400000 100[ms]
    fin=Count;
    freq=(fin-inicio)/1;
}

void mueveServo(float segundos,int angulo,int servo){

    cicloTrabajo=45*angulo+4000;//conversion de angulo a variable de programa
    //Tomado de la ecuacion y=45*x+4000
    //resultante de la relacion de variable de programa con Angulo
    retardo=periodo-cicloTrabajo;//resta para compensaciOn del ciclo
    fin=50*segundos;
    //tomado de la ecuacion y=(50)X
    //resultante de la relacion de segundos con ciclo for de programa

    for(d=0;d<fin;d++){
                 if(servo==1){
                    GPIO_PORTM_DATA_R |= 0x01;
                 }
                 else if(servo==2){
                    GPIO_PORTM_DATA_R |= 0x02;
                 }else if(servo==3){
                     GPIO_PORTM_DATA_R |= 0x04;
                 }
                 SysTick_Wait(cicloTrabajo);//Alto (cOdigo de angulo)
                 GPIO_PORTM_DATA_R = 0x00;
                 SysTick_Wait(retardo);//Bajo por el resto del ciclo para que cumpla 20[ms]
    }
}
//tiempo mInimo recomendado para mover servo 0.4[s]

void alimentaObjeto(){
    sentido=0;
    for(j=0;j<45;j++){
               for(i=0;i<70;i++){
                       GPIO_PORTN_DATA_R = 0x01;
                       SysTick_Wait(1700);//Espera
                       GPIO_PORTN_DATA_R = 0x00;
                       SysTick_Wait(500);//Espera
               }
               //SysTick_Wait(700000);//Espera
               SysTick_Wait(500000);//Espera
           }
           GPIO_PORTN_DATA_R = 0x00;
           SysTick_Wait(6000000);//Espera 01.5[s]

}

void dejaObjeto(float ajustaServo,int sen1,int sen2){
    for(j=0;j<80;j++){

               mueveServo(ajustaServo,j,2);  //se ingresa como parAmetro los segundos, y despuEs el Angulo
                    anguloS2=j;
                  sentido=sen1;
        }
    sentido=0;
                       for(j=80;j>0;j--){

                               mueveServo(ajustaServo,j,2);  //se ingresa como parAmetro los segundos, y despuEs el Angulo
                                  anguloS2=j;
                                  sentido=sen2;
       }

}

void clearScreen(){
    LCD_Set_Cursor(1, 1);
                  LCD_Write_String("                ");
                  LCD_Set_Cursor(2, 1);
                  LCD_Write_String("                ");
}

// En este programa se utiliza el ADC0 con el Secuenciador de muestras 2
// ya que el puede tomar hasta 4 muestras, y aquí se necesitan 2 muestras.
//


void main(void){
    Count = 0;

    I2C_Init(); //FunciOn que inicializa los relojes, el GPIO y el I2C0

    //Inicializo Slave
    while(I2C0_MCS_R&0x00000001){}; // espera que el I2C estE listo
    LCD_Init();

    SysTick_Init();

    LCD_Set_Cursor(1, 1);
    LCD_Write_String("AutoClasificador");
    //LCD_Write_Char('a');
    LCD_Set_Cursor(2, 1);
    LCD_Write_String("William Daza");

        SysTick_Wait(4000000);//espera 1S

        clearScreen();



        //habilita PORTN, PORTF, PORTL PORTJ
  SYSCTL_RCGCGPIO_R |= 0X1F3A; // RELOJ PARA EL PUERTO N,L,M,K,J,F,E,D,B
  SYSCTL_RCGCTIMER_R |= 0X08; //RELOJ Y HABILITA TIMER 3 (p.380)

                   //retardo para que el reloj alcance el PORTN Y TIMER 3
         while ((SYSCTL_PRGPIO_R & 0X1F3A) == 0){};  // reloj listo?

         //CONFIGURAR LA TERMINAL DE ENTRADA DEL MODULO LECTOR DE COLOR
           //CORRESPONDIENTE A PD4
                 // habilita el bit 4 como digital
                 // configura como entrada
                 //
                 GPIO_PORTD_AHB_DIR_R &= ~(0x10); //bit 4 entrada
                 GPIO_PORTD_AHB_DEN_R |= 0x10; //BIT 4 DIGITAL
                 GPIO_PORTD_AHB_DATA_R = 0x00; //RESTO DE SALIDAS A 0
                 GPIO_PORTD_AHB_AFSEL_R = 0x10; //FUNCION ALTERNA EN BIT 4
                 GPIO_PORTD_AHB_PCTL_R = 0x00030000; //DIRIGIDO A T3CCP0



                        //TIMER3_CTL_R=0X00000000; //DESHABILITA TIMER 3 PARA CONFIGURAR (p.986)
                       // TIMER3_CFG_R= 0X00000000; //CONFIGURA TIMER DE 32 BITS (p. 976)
                        //TIMER3_TAMR_R= 0X00000002; //CONFIGURAR PARA MODO PERIODICO CUENTA HACIA ABAJO (p. 977)
                       //TIMER3_TAMR_R= 0X00000012; //CONFIGURAR PARA MODO PERIODICO CUENTA HACIA ARRIBA (p. 977)

                       TIMER3_TAPR_R= 0X00; // PRESCALADOR DE TIMER A, SOLO PARA MODOS DE 16 BITS (p.1008)


                      // TIMER3_ICR_R= 0X00000001 ; //LIMPIA POSIBLE BANDERA PENDIENTE DE TIMER3 (p.1002)
                      //  TIMER3_IMR_R |= 0X00000001; //ACTIVA INTRRUPCION DE TIMEOUT (p.993)

                        TIMER3_ICR_R= 0X00000005 ; //LIMPIA POSIBLE BANDERA PENDIENTE DE TIMER3 (p.1002)
                     TIMER3_IMR_R |= 0X00000005; //ACTIVA INTRRUPCION DE TIMEOUT (p.993)


                 TIMER3_TAILR_R= 0X000030D4; // VALOR DE RECARGA (p.1004)//VALOR 12500 (YA NO FUNCIONA)
                 TIMER3_CTL_R=0X00000000; //DESHABILITA TIMER EN LA CONFIGURACION
                 TIMER3_CFG_R= 0X00000004; //CONFIGURAR PARA 16 BITS
                 TIMER3_TAMR_R= 0X00000007; //CONFIGURAR PARA MODO PERIODICO CUENTA HACIA ABAJO MODO CAPTURA
                // TIMER3_TAMR_R= 0X00000017; //CONFIGURAR PARA MODO PERIODICO CUENTA HACIA ARRIBA MODO CAPTURA
                 TIMER3_CTL_R &=0XFFFFFFF3; // EVENTO FLANCO DE SUBIDA (CONSIDERANDO CONDICIONES INICIALES DE REGISTRO)
                 //TIMER3_ICR_R= 0X00000004 ; //LIMPIA POSIBLE BANDERA PENDIENTE DE CAPTURA
                 //TIMER3_IMR_R |= 0X00000004; //ACTIVA INTERRUPCION DE CAPTURA

                 TIMER3_ICR_R= 0X00000005 ; //LIMPIA POSIBLE BANDERA PENDIENTE DE TIMER3 (p.1002)
                 TIMER3_IMR_R |= 0X00000005; //ACTIVA INTRRUPCION DE TIMEOUT (p.993)
                 NVIC_EN1_R= 1<<(35-32); //HABILITA LA INTERRUPCION DE  TIMER 3
                 TIMER3_CTL_R |= 0X00000001; //HABILITA TIMER EN LA CONFIGURACION




  GPIO_PORTE_AHB_DIR_R = 0x00;    // 2) PE5-4 entradas (analógica)
  GPIO_PORTE_AHB_AFSEL_R |= 0x30; // 3) Habilita Función Alterna en PE5-4 (p. 770)
  GPIO_PORTE_AHB_DEN_R &= ~0x30;  // 4) Deshabilita Función Digital en PE5-4 (p 781)
  GPIO_PORTE_AHB_AMSEL_R |= 0x30; // 5) Habilita Función Analógica de PE5-4 (p. 786)

  SYSCTL_RCGCADC_R |= 0x01;       // 6) Habilita reloj para ADC0(p. 396)

  while((SYSCTL_PRADC_R & 0x01) == 0); // Se espera a que el reloj se estabilice

  ADC0_PC_R = 0x01;         // 7)Configura para 125Ksamp/s (p.1159)
  ADC0_SSPRI_R = 0x0123;    // 8)SS3 con la más alta prioridad

  ADC0_ACTSS_R &= ~0x0004;  // 9)Deshabilita SS2 antes de cambiar configuración de registros (p. 1076)

  ADC0_EMUX_R = 0x0000;     // 10) Se configura SS2 para disparar muestreo por software (default) (p.1091)

  ADC0_SAC_R = 0x0;         // 11) Se configura para no tener sobremuestreo por hardware(default)(p. 1105)

  ADC0_CTL_R = 0x0;         //12) Se configura con referencias internas (default VDDA and GNDA) (p. 1107)

  ADC0_SSOP2_R = 0x0000;    // 13) Se configure para salvar los resultados del ADC en FIFO (default)(p. 1134)

  ADC0_SSTSH2_R = 0x000;    // 14) Se configure el ADC para un periodo de 4  S&H (default) (p. 1134)

  ADC0_SSMUX2_R = 0x0089;    // 15) Se configura entradas 1°muestra=AIN9 2°muestra=AIN8 (p.1128)
  ADC0_SSEMUX2_R &= ~0x0011; // 16) Canales del SS2 para 1°muestra y 2°muestra en AIN(15:0) (p.1137)
  ADC0_SSCTL2_R = 0x0060;    // 17) SI: AIN, Habilitación de INR2; No:muestra diferencial (p.1142)
  ADC0_IM_R &= ~0x0010;      // 18) Deshabilita interrupción SS2 (p. 1081)
  ADC0_ACTSS_R |= 0x0004;    // 19) Habilita SS2 (p. 1076)

  SYSCTL_PLLFREQ0_R |= SYSCTL_PLLFREQ0_PLLPWR;  // encender PLL
  while((SYSCTL_PLLSTAT_R&0x01)==0);            // espera a que el PLL fije su frecuencia
  SYSCTL_PLLFREQ0_R &= ~SYSCTL_PLLFREQ0_PLLPWR; // apagar PLL

  ADC0_ISC_R = 0x0004;  // Se recomienda Limpia la bandera RIS del ADC0

  //Habilitación de puertos

   GPIO_PORTM_DIR_R |= 0x0F;    // puerto M de salida
   GPIO_PORTM_DEN_R |= 0x0F;    // habilita el puerto M

   // habilita al Puerto L como salida digital para control de motor
  // PL0,...,PL3 como salidas hacia el ULN2003 (A,A´,B,B´)
   GPIO_PORTL_DIR_R = 0x0F;
   GPIO_PORTL_DEN_R = 0x0F;

   GPIO_PORTK_DIR_R = 0x0F;
   GPIO_PORTK_DEN_R = 0x0F;


           // habilita PN0 y PN1 como salida digital para monitoreo del programa
           //
           GPIO_PORTN_DIR_R = 0x03;
           GPIO_PORTN_DEN_R = 0x03;


           //para el cubo de color gris

           //Rojo_Frec  (valor medido de 765,850)
           //Azul_Frec         (valor medido de 496,530)
           //Verde_Frec      (valor medido de 390,410)


           //para el cubo de color naranja

           //Rojo_Frec   (valor medido de 1426,1405)
           //Azul_Frec         (valor medido de 496,460)
           //Verde_Frec       (valor medido de 344,350)

           //para el cubo de color rosa

           //Rojo_Frec   (valor medido de 1651,1553)
           //Azul_Frec         (valor medido de 883,822)
           //Verde_Frec      (valor medido de 587,536)

           //para el cubo de color azul

           //Rojo_Frec  (valor medido de 701)
           //Azul_Frec         (valor medido de 584)
           //Verde_Frec       (valor medido de 433)

           //para el cubo de color blanco

           //Rojo_Frec   (valor medido de 1580)
           //Azul_Frec          (valor medido de 1099)
           //Verde_Frec       (valor medido de 885)
           do{



  ADC0_PSSI_R = 0x0004;            //Inicia conversión del SS2
  while((ADC0_RIS_R&0x04)==0);     // Espera a que SS2 termine conversión (polling)
  dato1 = (ADC0_SSFIFO2_R&0xFFF);// se lee el primer resultado
  dato2 = (ADC0_SSFIFO2_R&0xFFF);// se lee el segundo resultado
  ADC0_ISC_R = 0x0004;             //Limpia la bandera RIS del ADC0
  //tiempo mInimo recomendado para mover servo 0.4[s]




  float ajustaServo=0.05;//variable que ajusta ciclo del funcion de servo
        //color=3;

                switch(color){
                    case 1:
                         sentido=1;
                          for(j=0;j<3;j++){
                                   SysTick_Wait(4000000);
                           }
                        //sentido=0;
                        dejaObjeto(ajustaServo,1,2);
                        sentido=2;
                               for(j=0;j<3;j++){
                                    SysTick_Wait(4000000);
                                }
                        sentido=0;
                        break;

                    case 2:
                                             sentido=1;
                                              for(j=0;j<14;j++){
                                                       SysTick_Wait(4000000);
                                               }
                                            ///sentido=1;
                                            dejaObjeto(ajustaServo,1,2);
                                            sentido=2;
                                                   for(j=0;j<14;j++){
                                                        SysTick_Wait(4000000);
                                                    }
                                            sentido=0;
                                            break;

                    case 3:
                                             sentido=2;
                                              for(j=0;j<3;j++){
                                                       SysTick_Wait(4000000);
                                               }
                                            //sentido=0;
                                            dejaObjeto(ajustaServo,2,1);
                                            sentido=1;
                                                   for(j=0;j<3;j++){
                                                        SysTick_Wait(4000000);
                                                    }
                                            sentido=0;
                                            break;

                    case 4:
                                             sentido=2;
                                              for(j=0;j<14;j++){
                                                       SysTick_Wait(4000000);
                                               }
                                            //sentido=0;
                                            dejaObjeto(ajustaServo,2,1);
                                            sentido=1;
                                                   for(j=0;j<14;j++){
                                                        SysTick_Wait(4000000);
                                                    }
                                            sentido=0;
                                            break;
                    default:
                        sentido=0;
                        mueveServo(0.4,70,2);//mov rapido
                        mueveServo(0.4,0,2); //mov rapido
                       break;
                }

                     //######SERVO DE SENSOR DE COLOR PARA TOPE

                 for(j=0;j< 80;j++){
                      mueveServo(ajustaServo,j,1);  //se ingresa como parAmetro los segundos, y despuEs el Angulo
                      anguloS1=j;
                      sentido=0;
                   }




                        //mueveServo(6,anguloS1,1);//equivalente de inaccion de servo para que espere el mismo tiempo
                        //mueveServo(6,anguloS2,2);//equivalente de inaccion de servo para que espere el mismo tiempo

                        //mueveServo(12,anguloS1,1);//equivalente de inaccion de servo para que espere el mismo tiempo
                        //mueveServo(12,anguloS2,2);//equivalente de inaccion de servo para que espere el mismo tiempo
                        //SysTick_Wait(20000000);



                        //######SERVO DE RAMPA
                    // dejaObjeto(ajustaServo);







                 alimentaObjeto();//funciOn mueve motor DC para obtener objeto




                   //SysTick_Wait(6000000);//Espera 01.5[s]

                   SysTick_Wait(2000000);//Espera 0.5[s]

                   //posicion 0 grados



                   //######SERVO DE COLOR
                   for(j=80;j> 0;j--){
                          mueveServo(ajustaServo,j,1);  //se ingresa como parAmetro los segundos, y despuEs el Angulo
                          anguloS1=j;
                          sentido=0;
                   }

                   //######SERVO DE RAMPA
                   for(j=0;j<25;j++){

                          mueveServo(ajustaServo,j,2);  //se ingresa como parAmetro los segundos, y despuEs el Angulo
                          anguloS2=j;
                          sentido=0;
                    }
                    for(j=25;j>0;j--){

                           mueveServo(ajustaServo,j,2);  //se ingresa como parAmetro los segundos, y despuEs el Angulo
                           anguloS2=j;
                           sentido=0;
                   }

                    for(j=0;j< 80;j++){
                                          mueveServo(ajustaServo,j,1);  //se ingresa como parAmetro los segundos, y despuEs el Angulo
                                          anguloS1=j;
                                          sentido=0;
                                       }

                    SysTick_Wait(8000000);//Espera 2[s]

                    //for(j=0;j)
                    SysTick_Wait(retlec);//  400000 100[ms]
                                                  GPIO_PORTK_DATA_R = 0x00;// S2 y S3 ceros
                                                  //GPIO_PORTL_DATA_R = 0x0F;// S2 y S3 ceros
                                                  SysTick_Wait(retlec);//  400000 100[ms]
                                                  pulseIn();
                                                   Rojo_Frec=freq;
                                                   SysTick_Wait(retlec);//  400000 100[ms]
                                                   GPIO_PORTK_DATA_R = 0x03;// S2 y S3 unos
                                                   SysTick_Wait(retlec);//  400000 100[ms]
                                                   pulseIn();
                                                   Verde_Frec=freq;
                                                   SysTick_Wait(retlec);//  400000 100[ms]
                                                   GPIO_PORTK_DATA_R = 0x02;// S2 cero y S3 uno
                                                   SysTick_Wait(retlec);//  400000 100[ms]
                                                   pulseIn();
                                                   Azul_Frec=freq;
                                                   SysTick_Wait(retlec);// 400000 100[ms]


                                int Gris_R_sup=Rojo_Frec_gris_valor+Gris_umbral;
                                int Gris_R_inf=Rojo_Frec_gris_valor-Gris_umbral;
                                int Gris_A_sup=Azul_Frec_gris_valor+Gris_umbral;
                                int Gris_A_inf=Azul_Frec_gris_valor-Gris_umbral;
                                int Gris_V_sup=Verde_Frec_gris_valor+Gris_umbral;
                                int Gris_V_inf=Verde_Frec_gris_valor-Gris_umbral;

                    if(((Rojo_Frec>=Gris_R_inf)&&(Rojo_Frec<=Gris_R_sup))&&((Azul_Frec>=Gris_A_inf)&&(Azul_Frec<=Gris_A_sup))&&((Verde_Frec>=(Verde_Frec_gris_valor-Gris_umbral))&&(Verde_Frec<=(Verde_Frec_gris_valor+Gris_umbral)))){
                        color=1;//color gris
                        clearScreen();
                        LCD_Set_Cursor(1, 1);
                        LCD_Write_String("Color:");
                        LCD_Set_Cursor(2, 1);
                        LCD_Write_String("Gris");
                    }else if(((Rojo_Frec>=(Rojo_Frec_naranja_valor-Naranja_umbral))&&(Rojo_Frec<=(Rojo_Frec_naranja_valor+Naranja_umbral)))&&((Azul_Frec>=(Azul_Frec_naranja_valor-Naranja_umbral))&&(Azul_Frec<=(Azul_Frec_naranja_valor+Naranja_umbral)))&&((Verde_Frec>=(Verde_Frec_naranja_valor-Naranja_umbral))&&(Verde_Frec<=(Verde_Frec_naranja_valor+Naranja_umbral)))){
                        color=2;//color naranja
                        clearScreen();
                        LCD_Set_Cursor(1, 1);
                        LCD_Write_String("Color:");
                        LCD_Set_Cursor(2, 1);
                        LCD_Write_String("Naranja");
                    }else if(((Rojo_Frec>=(Rojo_Frec_rosa_valor-Rosa_umbral))&&(Rojo_Frec<=(Rojo_Frec_rosa_valor+Rosa_umbral)))&&((Azul_Frec>=(Azul_Frec_rosa_valor-Rosa_umbral))&&(Azul_Frec<=(Azul_Frec_rosa_valor+Rosa_umbral)))&&((Verde_Frec>=(Verde_Frec_rosa_valor-Rosa_umbral))&&(Verde_Frec<=(Verde_Frec_rosa_valor+Rosa_umbral)))){
                        color=3;//color rosa
                        clearScreen();
                        LCD_Set_Cursor(1, 1);
                        LCD_Write_String("Color:");
                        LCD_Set_Cursor(2, 1);
                        LCD_Write_String("Rosa");
                    }else if(((Rojo_Frec>=(Rojo_Frec_azul_valor-Azul_umbral))&&(Rojo_Frec<=(Rojo_Frec_azul_valor+Azul_umbral)))&&((Azul_Frec>=(Azul_Frec_azul_valor-Azul_umbral))&&(Azul_Frec<=(Azul_Frec_azul_valor+Azul_umbral)))&&((Verde_Frec>=(Verde_Frec_azul_valor-Azul_umbral))&&(Verde_Frec<=(Verde_Frec_azul_valor+Azul_umbral)))){
                        color=4;//color azul
                        clearScreen();
                        LCD_Set_Cursor(1, 1);
                        LCD_Write_String("Color:");
                        LCD_Set_Cursor(2, 1);
                        LCD_Write_String("Azul");
                    }else{
                        color=6;//todo lo demas
                        clearScreen();
                        LCD_Set_Cursor(1, 1);
                        LCD_Write_String("Color:");
                        LCD_Set_Cursor(2, 1);
                        LCD_Write_String("Desconocido");
                    }
                    for(j=80;j> 0;j--){
                                              mueveServo(ajustaServo,j,1);  //se ingresa como parAmetro los segundos, y despuEs el Angulo
                                              anguloS1=j;
                                              sentido=0;
                                       }

                  // mueveServo(0.4,20,2);//mov rapido
                   //mueveServo(0.4,0,2); //mov rapido









        //SysTick_Wait(6000000);//Espera 01.5[s]




  }while(1);
}
