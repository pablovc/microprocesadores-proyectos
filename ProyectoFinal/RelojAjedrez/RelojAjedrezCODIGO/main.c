

#include <stdbool.h>
#include <stdint.h>
#include "I2C_LCD.h"
#include "inc/tm4c1294ncpdt.h" //Biblioteca Tiva Ware
#include <string.h>//memset cadenas en LCD
#include <math.h>

//REGISTROS DE RELOJ
#define SYSCTL_RCGCGPIO_R       (*((volatile uint32_t *)0x400FE608)) //Reloj del puerto
#define SYSCTL_RCGCI2C_R        (*((volatile uint32_t *)0x400FE620)) //Reloj de I2C
#define SYSCTL_PRGPIO_R        (*((volatile uint32_t *)0x400FEA08)) //Bandera de "Peripherial Ready"

//REGISTROS DEL PUERTO B
#define GPIO_PORTB_DATA_R   (*((volatile uint32_t *)0x400593FC)) //Para los datos del puerto
#define GPIO_PORTB_DIR_R    (*((volatile uint32_t *)0x40059400)) //Para seleccionar funciOn
#define GPIO_PORTB_AFSEL_R  (*((volatile uint32_t *)0x40059420)) //Para seleccionar funciOn alterna
#define GPIO_PORTB_ODR_R    (*((volatile uint32_t *)0x4005950C)) //Para activar el Open Drain
#define GPIO_PORTB_DEN_R    (*((volatile uint32_t *)0x4005951C)) //Para activar funci�n digital
#define GPIO_PORTB_PCTL_R   (*((volatile uint32_t *)0x4005952C)) //Para el control del puerto

//REGISTROS DEL MOUDLO I2C
#define I2C0_MSA_R              (*((volatile uint32_t *)0x40020000)) //I2C Master Slave Adress
#define I2C0_MCS_R              (*((volatile uint32_t *)0x40020004)) //I2C Master Control Status
#define I2C0_MDR_R              (*((volatile uint32_t *)0x40020008)) //I2C Master Data Register
#define I2C0_MTPR_R             (*((volatile uint32_t *)0x4002000C)) //I2C Master Time Period
#define I2C0_MCR_R              (*((volatile uint32_t *)0x40020020)) //I2C Master Configuration Register

//PLL
#define SYSCTL_PLLFREQ0_R       (*((volatile uint32_t *)0x400FE160)) //Registro para configurar el PLL
#define SYSCTL_PLLSTAT_R        (*((volatile uint32_t *)0x400FE168)) //Registro muestra el estado de encendido del PLL
#define SYSCTL_PLLFREQ0_PLLPWR  0x00800000  // Valor para encender el PLL


volatile uint32_t Termino= 0;


//primera fila LCD
char num[]="  00:00  00:00  ";
uint32_t i,j,k;//variables de programa
uint32_t cuenta,cuenta0,cuentaJ1,cuenta0J1,cuentaJ2,cuenta0J2;//variables de programa
uint32_t cuentaAntJ1,cuentaAnt0J1,cuentaAntJ2,cuentaAnt0J2;//variables de programa
uint32_t dato;//variables para botones de accion

//*** FunciOn de conversiOn de valores en caracteres para desplegar en cadena de caracteres en LCD ***
void conVal(int valor,int corr,int base){
    int unidades=valor%base;
    int decenas=(valor-unidades)/base;

    switch (decenas){
        case 0 :   memset (num+corr,'0',1);
            break;
        case 1 :  memset (num+corr,'1',1);
            break;
        case 2 :  memset (num+corr,'2',1);
            break;
        case 3 :  memset (num+corr,'3',1);
            break;
        case 4 :  memset (num+corr,'4',1);
                    break;
        case 5 :  memset (num+corr,'5',1);
                    break;
        case 6 :  memset (num+corr,'6',1);
                    break;
        case 7 :  memset (num+corr,'7',1);
                            break;
        case 8 :  memset (num+corr,'8',1);
                                    break;
        case 9 :  memset (num+corr,'9',1);
                                            break;
        default:  memset (num+corr,'?',1);
            break;
    }

    switch (unidades){
           case 0 :  memset (num+1+corr,'0',1);
               break;
           case 1 :  memset (num+1+corr,'1',1);
               break;
           case 2 :   memset (num+1+corr,'2',1);
               break;
           case 3 :   memset (num+1+corr,'3',1);
               break;
           case 4 :   memset (num+1+corr,'4',1);
               break;
           case 5 :   memset (num+1+corr,'5',1);
                         break;
           case 6 :   memset (num+1+corr,'6',1);
                         break;
           case 7 :   memset (num+1+corr,'7',1);
                         break;
           case 8 :   memset (num+1+corr,'8',1);
                         break;
           case 9 :   memset (num+1+corr,'9',1);
                                    break;
           case 10 :
               memset (num+corr,'1',1);
               memset (num+1+corr,'0',1);
                break;
           case 11 :
               memset (num+corr,'1',1);
              memset (num+1+corr,'1',1);
                                    break;
           case 12 :
               memset (num+corr,'1',1);
               memset (num+1+corr,'2',1);
                                    break;
           case 13 :
               memset (num+corr,'1',1);
               memset (num+1+corr,'3',1);
                                    break;
           case 14 :
               memset (num+corr,'1',1);
               memset (num+1+corr,'4',1);
                                    break;
           case 15 :
               memset (num+corr,'1',1);
               memset (num+1+corr,'5',1);
                                    break;
                      default:  memset (num+1+corr,'?',1);
                       break;
       }
 }

// ******* FUNCIONES DE PROGRAMA ********
void clearScreen(){
    //Borra la pantalla
            LCD_Set_Cursor(1, 1);//cursor a coordenada
            LCD_Write_String("                ");//16 espacios en blanco
            LCD_Set_Cursor(2, 1);//cursor a coordenada
            LCD_Write_String("                ");//16 espacios en blanco
}

// RUTINA DE SERVICIO DE INTERRUPCION

void Timer03AIntHandler(void)
{
    //LIMPIA BANDERA
      TIMER3_ICR_R= 0X00000001 ; //LIMPIA BANDERA DE TIMER3

      Termino=Termino+1;
      if (Termino < 1024)
       {
          if(Termino==500){
              cuenta=cuenta+1;
          }

       }else{
           Termino=0;
       }

}//FIN TIMER 3A

//*** PROGRAMA PRINCIPAL ****

void main(){

    I2C_Init(); //FunciOn que inicializa los relojes, el GPIO y el I2C0

        while(I2C0_MCS_R&0x00000001){}; // espera que el I2C estE listo

        LCD_Init();//inicializa pantalla LCD 16x2 con comunicaciOn I2C

        SysTick_Init();//inicia servicio de retraso por frecuencia

            SYSCTL_RCGCGPIO_R |= 0X0102; // 1) HABILITA RELOJ PARA EL PUERTOS B,J (p. 382)
            SYSCTL_RCGCTIMER_R |= 0X08; //RELOJ Y HABILITA TIMER 3 (p.380)
            //retardo para que el reloj alcance el PORTN Y TIMER 3
            while ((SYSCTL_PRGPIO_R & 0X0102) == 0){};  // reloj listo?
            TIMER3_CTL_R=0X00000000; //DESHABILITA TIMER 3 PARA CONFIGURAR (p.986)
            TIMER3_CFG_R= 0X00000000; //CONFIGURA TIMER DE 32 BITS (p. 976)
            //TIMER3_TAMR_R= 0X00000002; //CONFIGURAR PARA MODO PERIODICO CUENTA HACIA ABAJO (p. 977)
           TIMER3_TAMR_R= 0X00000012; //CONFIGURAR PARA MODO PERIODICO CUENTA HACIA ARRIBA (p. 977)
          TIMER3_TAILR_R= 0X00004E20; // VALOR DE RECARGA (p.1004) //VALOR 20000
           TIMER3_TAPR_R= 0X00; // PRESCALADOR DE TIMER A, SOLO PARA MODOS DE 16 BITS (p.1008)
            TIMER3_ICR_R= 0X00000001 ; //LIMPIA POSIBLE BANDERA PENDIENTE DE TIMER3 (p.1002)
            TIMER3_IMR_R |= 0X00000001; //ACTIVA INTRRUPCION DE TIMEOUT (p.993)
            NVIC_EN1_R= 1<<(35-32); //HABILITA LA INTERRUPCION 35 (TIMER3 A)
            TIMER3_CTL_R |= 0X00000001; //HABILITA TIMER 3 (p.986)

            GPIO_PORTJ_AHB_DIR_R = 0; // Se configura el pin 0 y 1 del puerto J como entradas
            GPIO_PORTJ_AHB_DEN_R = 0x03; // Se configura el pin 0 y 1 del puerto J como digitales
            GPIO_PORTJ_AHB_PUR_R = 0x03; // Resistencia en modo Pull-Up

            SysTick_Init();//inicio de servicio de espera de tiempo por frecuencia
            clearScreen();
            k=0;
            j=0;

            for(;;){

                 if(cuenta==59){
                    cuenta=0;
                    cuenta0=cuenta0+1;

                 }
                 if(j==0){
                     if(k==0){
                         if((cuentaJ1>=59)||(cuentaAntJ1>=59)){
                               cuentaJ1=0;
                               cuentaAntJ1=0;
                               cuenta0J1=cuenta0J1+1;

                          }
                         cuentaJ1=cuentaAntJ1+cuenta;
                         cuenta0J1=cuentaAnt0J1+cuenta0;
                     }else if(k==1){
                         if((cuentaJ2>=59)||(cuentaAntJ2>=59)){
                              cuentaJ2=0;
                              cuentaAntJ2=0;
                              cuenta0J2=cuenta0J2+1;

                         }
                         cuentaJ2=cuentaAntJ2+cuenta;
                        cuenta0J2=cuentaAnt0J2+cuenta0;
                     }
                 }else{

                 }

                 conVal(cuenta0J1,2,10);//minutos J1
                 conVal(cuentaJ1,5,10); //segundos J1
                 conVal(cuenta0J2,9,10);//minutos J2
                 conVal(cuentaJ2,12,10);//segundos J2
                 LCD_Set_Cursor(1, 1); //escritura en pantalla
                 LCD_Write_String(num);


                dato=GPIO_PORTJ_AHB_DATA_R;//lectura de botones en puerto J
                if(dato==1){//boton 1 puerto J
                     cuentaAntJ1=cuentaJ1; //guardando variable anterior de segundos
                     cuentaAntJ2=cuentaJ2;
                     cuentaAnt0J1=cuenta0J1; //guardando variable anterior de minutos
                     cuentaAnt0J2=cuenta0J2;
                     cuenta=0;//reiniciando cuenta
                     cuenta0=0;
                     k=1;
                }else if(dato==2){
                     cuentaAntJ1=cuentaJ1; //guardando variable anterior de segundos
                     cuentaAntJ2=cuentaJ2;
                     cuentaAnt0J1=cuenta0J1; //guardando variable anterior de minutos
                     cuentaAnt0J2=cuenta0J2;
                     cuenta=0;//reiniciando cuenta
                     cuenta0=0;
                     k=0;
                }else if(dato==0){
                    //caso donde los 2 botones son presionados
                    //reiniciando tablero
                    cuenta=0;
                    cuenta0=0;
                    cuentaJ1=0;
                    cuentaJ2=0;
                    cuenta0J1=0;
                    cuenta0J2=0;
                    conVal(cuenta0J1,2,10);//minutos J1
                    conVal(cuentaJ1,5,10); //segundos J1
                    conVal(cuenta0J2,9,10);//minutos J2
                    conVal(cuentaJ2,12,10);//segundos J2
                    LCD_Set_Cursor(1, 1); //escritura en pantalla
                    LCD_Write_String(num);
                    LCD_Set_Cursor(2, 1);
                    LCD_Write_String("                ");
                    j=0;
                }else{

                }
                if((cuenta0J1||cuenta0J2)==1){
                    conVal(cuentaAnt0J1,2,10);//minutos J1
                    conVal(cuentaAntJ1,5,10); //segundos J1
                    conVal(cuentaAnt0J2,9,10);//minutos J2
                    conVal(cuentaAntJ2,12,10);//segundos J2
                    LCD_Set_Cursor(2, 1);
                    j=1;
                    LCD_Write_String(" FinDeLaPartida ");

                }

               //LCD_Set_Cursor(2, 1);
               //LCD_Write_String("PabloVivarColina");

            //SysTick_Wait(4000000);//espera 1S

       }//FIN FOR INFINITO
}
