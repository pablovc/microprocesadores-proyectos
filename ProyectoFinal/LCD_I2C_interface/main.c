
#include <stdint.h>
#include "I2C_LCD.h"

//REGISTROS DE RELOJ
#define SYSCTL_RCGCGPIO_R       (*((volatile uint32_t *)0x400FE608)) //Reloj del puerto
#define SYSCTL_RCGCI2C_R        (*((volatile uint32_t *)0x400FE620)) //Reloj de I2C
#define SYSCTL_PRGPIO_R        (*((volatile uint32_t *)0x400FEA08)) //Bandera de "Peripherial Ready"

//REGISTROS DEL PUERTO B
#define GPIO_PORTB_DATA_R   (*((volatile uint32_t *)0x400593FC)) //Para los datos del puerto
#define GPIO_PORTB_DIR_R    (*((volatile uint32_t *)0x40059400)) //Para seleccionar funciOn
#define GPIO_PORTB_AFSEL_R  (*((volatile uint32_t *)0x40059420)) //Para seleccionar funciOn alterna
#define GPIO_PORTB_ODR_R    (*((volatile uint32_t *)0x4005950C)) //Para activar el Open Drain
#define GPIO_PORTB_DEN_R    (*((volatile uint32_t *)0x4005951C)) //Para activar función digital
#define GPIO_PORTB_PCTL_R   (*((volatile uint32_t *)0x4005952C)) //Para el control del puerto

//REGISTROS DEL MOUDLO I2C
#define I2C0_MSA_R              (*((volatile uint32_t *)0x40020000)) //I2C Master Slave Adress
#define I2C0_MCS_R              (*((volatile uint32_t *)0x40020004)) //I2C Master Control Status
#define I2C0_MDR_R              (*((volatile uint32_t *)0x40020008)) //I2C Master Data Register
#define I2C0_MTPR_R             (*((volatile uint32_t *)0x4002000C)) //I2C Master Time Period
#define I2C0_MCR_R              (*((volatile uint32_t *)0x40020020)) //I2C Master Configuration Register



//*** PROGRAMA PRINCIPAL ****

void main(){


    I2C_Init(); //FunciOn que inicializa los relojes, el GPIO y el I2C0

    //Inicializo Slave
    while(I2C0_MCS_R&0x00000001){}; // espera que el I2C estE listo
    LCD_Init();

    SysTick_Init();

    LCD_Set_Cursor(1, 1);
    LCD_Write_String("Hola mundos :D!!");
    //LCD_Write_Char('a');
    LCD_Set_Cursor(2, 1);
    LCD_Write_String("PabloVivarColina");

        SysTick_Wait(4000000);//espera 1S


        LCD_Set_Cursor(1, 1);
               LCD_Write_String("                ");
               LCD_Set_Cursor(2, 1);
               LCD_Write_String("                ");

        LCD_Set_Cursor(1, 1);
        LCD_Write_String("Hola como");
        //LCD_Write_Char('a');
        LCD_Set_Cursor(2, 1);
        LCD_Write_String("Estas");


}
