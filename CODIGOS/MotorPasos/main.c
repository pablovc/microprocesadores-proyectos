/*Asignatura: Microcontroladores y microprocesadores.   *Semestre: 2019-2.
*Tema 9: Perifericos
*Punto 9.2: El temporizador y sus aplicaciones.
*Práctica: Motor a pasos unipolar.
*Presentan: Dr. Saúl de la Rosa Nieves,  Ing. Jose Eduardo Villa Herrera.
-------------------------------------------------------------------------------
*/
// LIBRERIAS QUE UTILIZA EL PROGRAMA
#include <stdbool.h>
#include <stdint.h>
#include "inc/tm4c1294ncpdt.h"
volatile uint32_t Count= 0;
volatile uint32_t Termino= 0;

// RUTINA DE SERVICIO DE INTERRUPCIÓN
void Timer03AIntHandler(void)
{
    //LIMPIA BANDERA
    TIMER3_ICR_R= 0X00000001 ; //LIMPIA BANDERA DE TIMER3
    Termino = Termino + 1;


    // 32 * 64 = 2048
     if (Termino < 1024)
     {
    Count = Count + 0x01;

    switch (Count&0x0F) {
           case 0x01:
             GPIO_PORTL_DATA_R=0x03; // A,B
             GPIO_PORTN_DATA_R = 0x03; // A,B
            // GPIO_PORTF_AHB_DATA_R = 0x00; //
             break;
           case 0x02:
             GPIO_PORTL_DATA_R=0x06; // A',B
             GPIO_PORTN_DATA_R = 0x01; // B
            // GPIO_PORTF_AHB_DATA_R = 0x10; // A'
             break;
           case 0x03:
             GPIO_PORTL_DATA_R=0x0C; // A', B'
             GPIO_PORTN_DATA_R = 0x00; //
            // GPIO_PORTF_AHB_DATA_R = 0x11; //A', B'
             break;
           case 0x04:
             GPIO_PORTL_DATA_R=0x09; // A, B'
             GPIO_PORTN_DATA_R = 0x02; // A
             //GPIO_PORTF_AHB_DATA_R = 0x01; // B'
             Count = 0x0;
              break;
       }
      }else{
          GPIO_PORTL_DATA_R=0x00; //CEROS
      }
    }
/*********************************************
* SE GENERAN LAS SEÑALES DE CONTROL PARA UN MOTOR UNIPOLAR
* PARA LA SINCRONIZACIÓN SE UTILIZA EL TIMER3 EN SU CONFIGURACIÓN DE 32 BITS
*
* LAS SEÑALES DE CONTROL SE MANEJAN POR EL PUERTO L
*
* SE UTILIZAN LOS LEDs CONTECTADOS A LOS PUERTOS F y N
* PARA VISUALIZAR LAS SEÑALES GENERADAS
* PF0, PF4 como salidas (Leds).
* PN0, PN1 como salidas (Leds)
*/
 main(void) {
     //habilita PORTN, PORTF, PORTL
      SYSCTL_RCGCGPIO_R |= 0X1420; // RELOJ PARA EL PUERTO F, L y N
      SYSCTL_RCGCTIMER_R |= 0X08; //RELOJ Y HABILITA TIMER 3 (p.380)

         //retardo para que el reloj alcance el PORTN Y TIMER 3
         while ((SYSCTL_PRGPIO_R & 0X1420) == 0){};  // reloj listo?
         TIMER3_CTL_R=0X00000000; //DESHABILITA TIMER 3 PARA CONFIGURAR (p.986)
         TIMER3_CFG_R= 0X00000000; //CONFIGURA TIMER DE 32 BITS (p. 976)
         //TIMER3_TAMR_R= 0X00000002; //CONFIGURAR PARA MODO PERIODICO CUENTA HACIA ABAJO (p. 977)
        TIMER3_TAMR_R= 0X00000012; //CONFIGURAR PARA MODO PERIODICO CUENTA HACIA ARRIBA (p. 977)
        //TIMER3_TAILR_R= 0X000F7100; // VALOR DE RECARGA (p.1004)
        //TIMER3_TAILR_R= 0X0004E200; // VALOR DE RECARGA (p.1004)
         //TIMER3_TAILR_R= 0X00027100; // VALOR DE RECARGA (p.1004)
        //TIMER3_TAILR_R= 0X00013780; // VALOR DE RECARGA (p.1004)
       //TIMER3_TAILR_R= 0X00009C40; // VALOR DE RECARGA (p.1004)
       TIMER3_TAILR_R= 0X00004E20; // VALOR DE RECARGA (p.1004) //VALOR 20000
      //TIMER3_TAILR_R= 0X0000445C; // VALOR DE RECARGA (p.1004)//VALOR 17500 (NO ESTABLE)
       //TIMER3_TAILR_R= 0X00003A98; // VALOR DE RECARGA (p.1004)//VALOR 15000 (NO ESTABLE)
       //TIMER3_TAILR_R= 0X000038A4; // VALOR DE RECARGA (p.1004)//VALOR 14500 (YA NO FUNCIONA)
      //TIMER3_TAILR_R= 0X000036B0; // VALOR DE RECARGA (p.1004)//VALOR 14000 (YA NO FUNCIONA)
      //TIMER3_TAILR_R= 0X000030D4; // VALOR DE RECARGA (p.1004)//VALOR 12500 (YA NO FUNCIONA)
        //TIMER3_TAILR_R= 0X00002710; // VALOR DE RECARGA (p.1004)//VALOR 10000 (YA NO FUNCIONA)
        TIMER3_TAPR_R= 0X00; // PRESCALADOR DE TIMER A, SOLO PARA MODOS DE 16 BITS (p.1008)
         TIMER3_ICR_R= 0X00000001 ; //LIMPIA POSIBLE BANDERA PENDIENTE DE TIMER3 (p.1002)
         TIMER3_IMR_R |= 0X00000001; //ACTIVA INTRRUPCION DE TIMEOUT (p.993)
         NVIC_EN1_R= 1<<(35-32); //HABILITA LA INTERRUPCION 35 (TIMER3 A)
         TIMER3_CTL_R |= 0X00000001; //HABILITA TIMER 3 (p.986)

         // habilita al Puerto L como salida digital para control de motor
         // PL0,...,PL3 como salidas hacia el ULN2003 (A,A´,B,B´)
         GPIO_PORTL_DIR_R = 0x0F;
         GPIO_PORTL_DEN_R = 0x0F;
         GPIO_PORTL_DATA_R = 0x09;

         // habilita PN0 y PN1 como salida digital para monitoreo del programa
         //
         GPIO_PORTN_DIR_R = 0x03;
         GPIO_PORTN_DEN_R = 0x03;

         // habilita PF0 y PF4 como salida digital para monitoreo del programa
         //
         //GPIO_PORTF_AHB_DIR_R = 0x11;
         //GPIO_PORTF_AHB_DEN_R = 0x11;

         while(1);

}
