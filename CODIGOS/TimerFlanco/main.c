#include <stdbool.h>
#include <stdint.h>
#include "inc/tm4c1294ncpdt.h"

#include "I2C_LCD.h"

volatile uint32_t Count= 0;
volatile uint32_t freq= 0;
volatile uint32_t inicio= 0;
volatile uint32_t fin= 0;

//*********************************
//** RUTINA DE SERVICIO DE INTERRUPCIÓN ***
//**CONTADOR PARA LA FUNCION DE CUENTA POR FLANCO

void Timer03AIntHandler(void)
{
   TIMER3_ICR_R= 0X00000004 ; //LIMPIA BANDERA DE EVENTO DE CAPTURA
    Count = Count + 1;       // Cuenta el número de flancos de subida

 }


//************************************************************
// CONFIGURACIÓN PARA CAPTURA EN LA ENTRADA DEL TIMER 3A
 main(void) {

     Count = 0;
     SysTick_Init();
     //habilita PORTD
      SYSCTL_RCGCGPIO_R = SYSCTL_RCGCGPIO_R3; //HABILITA PORTD

         SYSCTL_RCGCTIMER_R |= 0X08; //HABILITA TIMER 3
         while ((SYSCTL_PRGPIO_R & 0X0008) == 0){};  // reloj listo?

         //CONFIGURAR LA TERMINAL DE ENTRADA DEL MODULO T3CCP0
         //CORRESPONDIENTE A PD4
               // habilita el bit 4 como digital
               // configura como entrada
               //
               GPIO_PORTD_AHB_DIR_R &= ~(0x10); //bit 4 entrada
               GPIO_PORTD_AHB_DEN_R |= 0x10; //BIT 4 DIGITAL
               GPIO_PORTD_AHB_DATA_R = 0x00; //RESTO DE SALIDAS A 0
               GPIO_PORTD_AHB_AFSEL_R = 0x10; //FUNCION ALTERNA EN BIT 4
               GPIO_PORTD_AHB_PCTL_R = 0x00030000; //DIRIGIDO A T3CCP0


         TIMER3_TAILR_R= 0X000030D4; // VALOR DE RECARGA (p.1004)//VALOR 12500 (YA NO FUNCIONA)
         TIMER3_CTL_R=0X00000000; //DESHABILITA TIMER EN LA CONFIGURACION
         TIMER3_CFG_R= 0X00000004; //CONFIGURAR PARA 16 BITS
         TIMER3_TAMR_R= 0X00000007; //CONFIGURAR PARA MODO PERIODICO CUENTA HACIA ABAJO MODO CAPTURA
        // TIMER3_TAMR_R= 0X00000017; //CONFIGURAR PARA MODO PERIODICO CUENTA HACIA ARRIBA MODO CAPTURA
         TIMER3_CTL_R &=0XFFFFFFF3; // EVENTO FLANCO DE SUBIDA (CONSIDERANDO CONDICIONES INICIALES DE REGISTRO)
         TIMER3_ICR_R= 0X00000004 ; //LIMPIA POSIBLE BANDERA PENDIENTE DE CAPTURA
         TIMER3_IMR_R |= 0X00000004; //ACTIVA INTERRUPCION DE CAPTURA
         NVIC_EN1_R= 1<<(35-32); //HABILITA LA INTERRUPCION DE  TIMER 3
         TIMER3_CTL_R |= 0X00000001; //HABILITA TIMER EN LA CONFIGURACION

         for(;;){

             inicio=Count;
             SysTick_Wait(400000);//  400000 100[ms]
             fin=Count;
             freq=(fin-inicio)/1;
         }



         //while(1);

}
