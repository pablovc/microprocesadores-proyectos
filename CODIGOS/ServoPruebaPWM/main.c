

//LibrerIas incluidas

#include <stdint.h> // Libreria para aritmEtica

#include "inc/tm4c1294ncpdt.h" //Biblioteca Tiva Ware

// ***** Variables globales ***** //
uint32_t d,fin,i;
uint32_t cicloTrabajo;
uint32_t retardo;
uint32_t periodo=80000;

// DefiniciOn de constantes para operaciones
#define NVIC_ST_CTRL_COUNT      0x00010000  // bandera de cuenta flag
#define NVIC_ST_CTRL_CLK_SRC    0x00000004  // Clock Source
#define NVIC_ST_CTRL_INTEN      0x00000002  // Habilitador de interrupción
#define NVIC_ST_CTRL_ENABLE     0x00000001  // Modo del contador
#define NVIC_ST_RELOAD_M        0x00FFFFFF  // Valor de carga del contador

// Definición de apuntadores para direccionar
//#define GPIO_PORTN_DATA_R       (*((volatile unsigned long *)0x4006400C)) // Registro de Datos Puerto N
//#define GPIO_PORTN_DIR_R        (*((volatile unsigned long *)0x40064400)) // Registro de Dirección Puerto N
//#define GPIO_PORTN_DEN_R        (*((volatile unsigned long *)0x4006451C)) // Registro de Habilitación Puerto N

//#define SYSCTL_PRGPIO_R         (*((volatile unsigned long *)0x400FEA08)) // Registro de estatus de Reloj de Puerto

// Definición de constantes para operaciones
#define SYSCTL_RCGC2_GPION      0x00001000  // bit de estado del reloj de puerto N
#define SYSCTL_RCGC2_R          (*((volatile unsigned long *)0x400FE608)) // Registro de Habilitación de Reloj de Puertos

//**********FUNCION Inizializa el SysTick *********************
void SysTick_Init(void){
  NVIC_ST_CTRL_R = 0;                   // Desahabilita el SysTick durante la configuración
  NVIC_ST_RELOAD_R = NVIC_ST_RELOAD_M;  // Se establece el valor de cuenta deseado en RELOAD_R
  NVIC_ST_CURRENT_R = 0;                // Se escribe al registro current para limpiarlo

  NVIC_ST_CTRL_R = 0x00000001;         // Se Habilita el SysTick y se selecciona la fuente de reloj
}

//*************FUNCION Tiempo de retardo utilizando wait.***************
// El parametro de retardo esta en unidades del reloj interno/4 = 4 MHz (250 ns)
void SysTick_Wait(uint32_t retardo){
    NVIC_ST_RELOAD_R= retardo-1;   //nUmero de cuentas por esperar
    NVIC_ST_CURRENT_R = 0;
    while((NVIC_ST_CTRL_R&0x00010000)==0){//espera hasta que la bandera COUNT sea valida
    }
   } //

//los ciclos de codificacion para el servo SG90
    //estAn dados por 20 [mS]
    //en el codigo 4000000 en el valor del SysTick es 1[s]
    //por lo que 80000 equivale a 20 [ms]

    //cada ciclo de servo consume 20 [mS]
    //esto es importante tomar en consideracion
    //para el tiempo de respuesta del mismo
    //50 ciclos de for equivale a 1 segundo

void mueveServo(float segundos,int angulo,int servo){

    cicloTrabajo=45*angulo+4000;//conversion de angulo a variable de programa
    //Tomado de la ecuacion y=45*x+4000
    //resultante de la relacion de variable de programa con Angulo
    retardo=periodo-cicloTrabajo;//resta para compensaciOn del ciclo
    fin=50*segundos;
    //tomado de la ecuacion y=(50)X
    //resultante de la relacion de segundos con ciclo for de programa

    for(d=0;d<fin;d++){
                 if(servo==1){
                    GPIO_PORTM_DATA_R |= 0x01;
                 }
                 else if(servo==2){
                    GPIO_PORTM_DATA_R |= 0x02;
                 }else if(servo==3){
                     GPIO_PORTM_DATA_R |= 0x04;
                 }
                 SysTick_Wait(cicloTrabajo);//Alto (cOdigo de angulo)
                 GPIO_PORTM_DATA_R = 0x00;
                 SysTick_Wait(retardo);//Bajo por el resto del ciclo para que cumpla 20[ms]
    }
}
//tiempo mInimo recomendado para mover servo 0.4[s]

 main(void){

  //SYSCTL_RCGC2_R |= SYSCTL_RCGC2_GPION; //
  SYSCTL_RCGCGPIO_R |= 0X1E12; // 1) HABILITA RELOJ PARA EL PUERTOS B,E,M,K,L,N (p. 382)

  SysTick_Init();//inicializa servicio de interrupciones por reloj

  while ((SYSCTL_PRGPIO_R & 0X1000) == 0){};  // reloj listo?



  GPIO_PORTM_DIR_R |= 0x0F;    // puerto M de salida
  GPIO_PORTM_DEN_R |= 0x0F;    // habilita el puerto M



  for(;;){

      //tiempo mInimo recomendado para mover servo 0.4[s]

      for(i=1;i<4;i++){
        //posicion 0 grados
            //se ingresa como parAmetro los segundos, y despuEs el Angulo
             mueveServo(0.4,0,i);
         //posicion 45 grados
             mueveServo(0.4,45,i);
         //posicion 90 grados
             mueveServo(0.4,90,i);
         //SysTick_Wait(2000000);//Espera 0.5[s]
         //posicion 135 grados POSICION MAXIMA
             mueveServo(1,135,i);
         SysTick_Wait(6000000);//Espera 01.5[s]
      }


  }//fin for infinito
}
