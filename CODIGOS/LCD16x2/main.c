/*Asignatura: Microcontroladores y microprocesadores.   *Semestre: 2020-2.
*Ejercicio: Reloj Digital I2C
*Presenta Pablo Vivar Colina.
-------------------------------------------------------------------------------
*/

/*****DESCRIPCION****
 * Reloj digital que funciona con modulo SPI (i2c) RTC (real time clock) con el cual el microcontrolador TIVA se comunica escribiendo y leyendo
 * el dato de fecha y hora, Este dato es desplegado en una pantalla LCD y es configurado algunos valores mediante el servicio de interrupciones
 * con botones fisicos
 *
 */

/****DIAGRAMA***
 *                 ______
 * _________       |     |
 * |        |->SDA | RTC |
 * |  TIVA  |<-SCL | i2c |
 * |________|      |_____|
 *  | | | |
 * _V V V V_____Dato de 8 bit
 * | LCD       |
 * |___________|
 *
 */

/*****CONEXIONES******
 * PotenciOmetro: pin1->GND pin2-> V0 LCD pin3-> +5V
 * LCD 1602A a TIVA
 * VSS-> GND
 * VDD-> +5V
 * V0-2 pin2 potenciOmetro
 *RS-> PG0
 *E -> GND
 *RW-> PG1
 *D0->PK0
 *D1->PK1
 *D2->PK2
 *D3->PK3
 *D4->PK4
 *D5->PK5
 *D6->PK6
 *D7->PK7
 *A-> +5V resistor recomendable de 330 Ohm
 *K-> GND
 * RTC DS1307 a TIVA
 * GND-> GND
 * 5V->5V
 * SDA->PB2
 * SCL->PB3
 */

/***NOTAS DE STARTUP***
 * modificar startup para handler del puerto J con intecnion de que el servicio de interrupciones funcione adecuadamente
 *
 */


/* bibliotecas Seleccionadas */
#include <stdio.h>
#include <string.h>//memset cadenas en LCD
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include "inc/tm4c1294ncpdt.h" //Tiva Ware

// Comandos LCD
#define LCD_INS 2
#define LCD_DAT 3

//REGISTROS DE RELOJ I2C
#define SYSCTL_RCGCGPIO_R       (*((volatile uint32_t *)0x400FE608)) //Reloj del puerto
#define SYSCTL_RCGCI2C_R        (*((volatile uint32_t *)0x400FE620)) //Reloj de I2C
#define SYSCTL_PRGPIO_R        (*((volatile uint32_t *)0x400FEA08)) //Bandera de "Peripherial Ready"

//REGISTROS INTERRUPCIONES
/*#define GPIO_PORTJ_DIR_R        (*((volatile uint32_t *)0x40060000)) //Registro de Dirección PJ , todos los pines del puerto como entradas(p. 760)
#define GPIO_PORTJ_DEN_R        (*((volatile uint32_t *)0x4006051C)) //Registro de habilitación PJ (p. 781)
#define GPIO_PORTJ_PUR_R        (*((volatile uint32_t *)0x40060510)) //Registro de pull-up PJ
#define GPIO_PORTJ_DATA_R       (*((volatile uint32_t *)0x40060004)) //Registro de Datos J
#define GPIO_PORTJ_IS_R         (*((volatile uint32_t *)0x40060404)) //Registro de configuraciOn de detecciOn de nivel o flanco
#define GPIO_PORTJ_IBE_R        (*((volatile uint32_t *)0x40060408)) //Registro de configuraciOn de interrupciOn por ambos flancos
#define GPIO_PORTJ_IEV_R        (*((volatile uint32_t *)0x4006040C)) //Registro de configuraciOn de interrupciOn por un flanco
#define GPIO_PORTJ_ICR_R        (*((volatile uint32_t *)0x4006041C)) //Registro de limpieza de interrupciOn de flanco en PJ
#define GPIO_PORTJ_IM_R         (*((volatile uint32_t *)0x40060410)) //Registro de mascara de interrupciOn PJ (p.764)
#define NVIC_EN1_R              (*((volatile uint32_t *)0xE000E104)) // Registro de habilitaciOn de interrupciOn PJ
#define NVIC_PRI12_R             (*((volatile uint32_t *)0xE000E430))//Registro de prioridad de interrupciOn
*/

// Incrementa la variable una vez cada que se presiona SW1 -Interrupcion PJ => #51 (p. 115)
/*volatile uint32_t SW0 = 0;
void EdgeCounter_Init(void){
  SYSCTL_RCGCGPIO_R |= 0x00000100; // (a) activa el reloj para el puerto J
  SW0=0;
  GPIO_PORTJ_DIR_R &= ~0x03;       // (c) PJ0 direcciOn entrada - boton SW1
    GPIO_PORTJ_DEN_R |= 0x03;        //     PJ0 se habilita
    GPIO_PORTJ_PUR_R |= 0x03;        //     habilita weak pull-up on PJ1
    GPIO_PORTJ_IS_R &= ~0x03;        // (d) PJ1 es sensible por flanco (p.761)
    GPIO_PORTJ_IBE_R &= ~0x03;       //     PJ1 no es sensible a dos flancos (p. 762)
    GPIO_PORTJ_IEV_R &= ~0x03;       //     PJ1 detecta eventos de flanco de bajada (p.763)
    GPIO_PORTJ_ICR_R = 0x03;         // (e) limpia la bandera 0 (p.769)
    GPIO_PORTJ_IM_R |= 0x03;         // (f) Se desenmascara la interrupcion PJ0 y se envia al controlador de interrupciones (p.764)

  NVIC_PRI12_R = (NVIC_PRI12_R&0x00FFFFFF)|0x00000000; // (g) prioridad 0  (p. 159)
  NVIC_EN1_R= 1<<(51-32);          //(h) habilita la interrupciOn 51 en NVIC (p. 154)
}*/

/*
void GPIOPortJ_Handler(void)
{
            GPIO_PORTJ_ICR_R = 0x03; //mascara del puerto J
            SW0=SW0+1;//incremento de la cuenta cuando es presionado el boton
}
*/


//REGISTROS DEL PUERTO B
#define GPIO_PORTB_DATA_R   (*((volatile uint32_t *)0x400593FC)) //Para los datos del puerto
#define GPIO_PORTB_DIR_R    (*((volatile uint32_t *)0x40059400)) //Para seleccionar funciOn
#define GPIO_PORTB_AFSEL_R  (*((volatile uint32_t *)0x40059420)) //Para seleccionar funciOn alterna
#define GPIO_PORTB_ODR_R    (*((volatile uint32_t *)0x4005950C)) //Para activar el Open Drain
#define GPIO_PORTB_DEN_R    (*((volatile uint32_t *)0x4005951C)) //Para activar función digital
#define GPIO_PORTB_PCTL_R   (*((volatile uint32_t *)0x4005952C)) //Para el control del puerto

//REGISTROS DEL MÓUDLO I2C
#define I2C0_MSA_R              (*((volatile uint32_t *)0x40020000)) //I2C Master Slave Adress
#define I2C0_MCS_R              (*((volatile uint32_t *)0x40020004)) //I2C Master Control Status
#define I2C0_MDR_R              (*((volatile uint32_t *)0x40020008)) //I2C Master Data Register
#define I2C0_MTPR_R             (*((volatile uint32_t *)0x4002000C)) //I2C Master Time Period
#define I2C0_MCR_R              (*((volatile uint32_t *)0x40020020)) //I2C Master Congirutation Register

/*
El registro I2C Master Control/Status (I2C_MCS_R) tiene:
-Modo READ-ONLY DATUS: los 7 bits menos significativos son:
    7:Clock Time Out Error  6:BUS BUSY      5:IDLE
    4:Arbitration Lost      3:DataAck       2:AdrAck
    1:Error                 0:CONTROLLER BUSY

-Modo WRITE-ONLY CONTROL_ Los 6 bits menos significativos son:
    6:BURST    5:QuickCommand  4:High Speed Enable
    3:ACK      2:STOP          1:START
    0:RUN
*/
#define I2C_MCS_ACK 0x00000008 //Transmmitter Acknowledge Enable
#define I2C_MCS_DATACK 0x00000008 // Data Acknowledge Enable
#define I2C_MCS_ADRACK 0x00000004 // Acknowledge Address
#define I2C_MCS_STOP 0x00000004 // Generate STOP
#define I2C_MCS_START 0x00000002 // Generate START
#define I2C_MCS_ERROR 0x00000002 // Error
#define I2C_MCS_RUN 0x00000001 // I2C Master Enable
#define MAXRETRIES 5 // number of receive attempts before giving up

//**Direcciones del DS1307
int AdreDS1307 =0x068;///Dirección del RTC DS1307
int AdreSec= 0x00;
int AdreMin=0x01;

/*El cAlculo del Time Period Register (TPR) se especifica en la página 1284
 Asumiendo un reloj de 16 MHz y un modo de operación estAndar (100 kbps):
*/
int TPR = 7;

// Variables para manejar los valores del RTC
uint8_t segundos,segundosA=0,segundosB=1, minutos, horas, dia, fecha, mes, year;
uint8_t error;
uint32_t i;

//primera fila LCD
char num[]="||||||||::::::::";

//*** FunciOn que inicializa los relojes, el GPIO y el I2C0 ***
void I2C_Init(void){
    //CONFIGURACIÓN DE LOS RELOJ
    SYSCTL_RCGCI2C_R |= 0x0001; // Activamos el reloj de I2C0 [I2C9 I2C8 I2C7 ... I2C0]<--Mapa de RCGCI2C
    SYSCTL_RCGCGPIO_R |= 0x0002; // Activamos el reloj GPIO_PORTB mientras se activa el reloj de I2C0
    while((SYSCTL_PRGPIO_R&0x0002) == 0){};//Espero a que se active el reloj del puerto B

    //CONFIGURACION DE LOS GPIOS
    /*Acorde con la tabla "Signals by function" de la p. 1808:
     el PIN 2 del puerto B (PB2) es el I2C0SCL del I2C0, y
     el PIN 3 del puerto B (PB3) es el I2C0SDA del I2C0
    */
    GPIO_PORTB_AFSEL_R |= 0x0C; // Activo la función alterna del PB2 y PB3
    GPIO_PORTB_ODR_R |= 0x08;   // Activo el OPEN DRAIN para el PB3, ya que el PB2 ya tiene uno por preconfig.
    GPIO_PORTB_DIR_R |= 0x0C;   //Activo al PB2 y al PB3 como OUTPUT
    GPIO_PORTB_DEN_R |= 0x0C;   //Activo la función digital de PB3 y PB2
    /*
    Como el registro AFSEL indica que se ejecuta una funciOn externa, en el registro PCTL
    debemos indicar QUE funciOn alterna se realizarA acorde con la tabla 26-5 de la p.1808 e indicarlo
     en el correspondiente PCMn (uno por cada bit del puerto) del registro PCTL
     */
    GPIO_PORTB_PCTL_R|=0x00002200;

    //CONFIGURACIÓN DEL MODULO I2C0
    I2C0_MCR_R = 0x00000010; // Habilitar función MASTER para el I2C0
    I2C0_MTPR_R = TPR; // Se establece una velocidad estándar de 100kbps
}

// ** Función esperar **
int esperar(){
    while(I2C0_MCS_R&0x00000001){}; //Espero a que la transmisión acabe
      if(I2C0_MCS_R&0x00000002==1){ //¿Hubo error?
          error=1;
          return error;
      };
      return 0;
}

//** Función para configurar al esclavo RTC DS1307 **
void CargarFecha(int modo){
    /*
     Programar: Lunes 9 de Marzo del 2020, a las 23:58:00 pm

    El mapa de memoria del DS1207 es el siguiente:
    DIRECCIÓN  FUNCIÓN    BIT7   BIT6  BIT5  BIT4  BIT3  BIT2  BIT1   BIT0
        00h    Segundos     0     0     0      0    0      0    0       0
        01h     Minutos     0     0     0      0    0      0    0       0
        02h     Horas       0     0     0      1    0      0    0       0
        03h     Día         0     0     0      0    0      0    1       0
        04h     Fecha       0     0     0      0    1      0    0       0
        05h     Mes         0     0     0      0    0      1    0       1
        06h     Año         0     0     0      1    1      0    0       0
        07h     Control     0     0     0      0    0      0    1       1
     08h-3Fh    RAM 56x8
     */
    //Por lo tanto


    if(modo==0){//modo 0 para borrar datos internos
        segundos=0x00, minutos=0x00, horas=0x00, dia=0x00, fecha=0x00, mes=0x00, year=0x00;
    }else if(modo==1){//modo 1 para ingresar valores
        segundos=0x44, minutos=0x58, horas=0x23, dia=0x05, fecha=0x09, mes=0x03, year=0x20;
    }

    /*
     * DESPLAGADO EN LCD
        conVal(fecha,2);
        conVal(mes,4);
        conVal(year,6);
        conVal(horas,9);
        conVal(minutos,11);
        conVal(segundos,13);
     *
     */
    while(I2C0_MCS_R&0x00000001){}; // wait for I2C ready
    //Para transmitir
    I2C0_MSA_R=(AdreDS1307<<1)&0xFE; //Cargo la dirección del DS1307 e indico "SEND", es decir, el Slave va a recibir
    I2C0_MDR_R=AdreSec&0x0FF; //Envio la Subdirección( dirección del registro interno "segundos") al DS1307
    I2C0_MCS_R=(I2C_MCS_RUN|I2C_MCS_START); // Condición de START y corro

    esperar();
    for(i=0;i<300;i++){} //Delay
    I2C0_MDR_R=segundos; //Envio el valor de "segundos"
    I2C0_MCS_R=(I2C_MCS_RUN);

    esperar();
    for(i=0;i<300;i++){} //Delay
    I2C0_MDR_R=minutos; //Envio el valor de "minutos"
    I2C0_MCS_R=(I2C_MCS_RUN); //Inicio la transmisiOn 1

    esperar();
    for(i=0;i<300;i++){} //Delay
    I2C0_MDR_R=horas; //Envio el valor de "horas"
    I2C0_MCS_R=(I2C_MCS_RUN); //Inicio la transmisiOn 2

    esperar();
    for(i=0;i<300;i++){} //Delay
    I2C0_MDR_R=dia; //Envio el valor de "dia"
    I2C0_MCS_R=(I2C_MCS_RUN); //Inicio la transmisiOn 4

    esperar();
    for(i=0;i<300;i++){} //Delay
    I2C0_MDR_R=fecha; //Envio el valor de "fecha"
    I2C0_MCS_R=(I2C_MCS_RUN); //Inicio la transmisiOn 5

    esperar();
    for(i=0;i<300;i++){} //Delay
    I2C0_MDR_R=mes; //Envio el valor de "mes"
    I2C0_MCS_R=(I2C_MCS_RUN); //Inicio la transmisiOn 6

    esperar();
    for(i=0;i<300;i++){} //Delay
    I2C0_MDR_R=year; //Envio el valor de "year"
    I2C0_MCS_R=(I2C_MCS_STOP|I2C_MCS_RUN); //Inicio la ultima transmisiOn y STOP

    esperar();
    for(i=0;i<300;i++){} //Delay
}


//*** FunciOn que lee valores por comunicaciOn i2c,  ***
void leerFecha(){
        while(I2C0_MCS_R&0x00000001){}; // wait for I2C ready
        //Para actualizar registro para iniciar la lectura
        I2C0_MSA_R=(AdreDS1307<<1)&0xFE; //Cargo la direcciOn del DS1207 e indico "SEND", es decir, el Slave va a recibir
        I2C0_MDR_R=AdreSec&0x0FF; //Envio la SubdirecciOn( direcciOn del registro interno "segundos") al DS1307
        I2C0_MCS_R=(I2C_MCS_STOP|I2C_MCS_START|I2C_MCS_RUN);// CondiciOn de START, habilito al Master y Alto
        esperar();
        for(i=0;i<300;i++){} //Delay

        //Para recibir información
        I2C0_MSA_R=(AdreDS1307<<1)&0xFE; //La dirección del DS1307 en el Master Slave Adress
        I2C0_MSA_R|=0x01; //Indico "RECIEVE", es decir, el Slave va a transmitir
        I2C0_MCS_R=(I2C_MCS_STOP|I2C_MCS_START|I2C_MCS_RUN); // CondiciOn de START, habilito al Master y Alto
        esperar();
        for(i=0;i<300;i++){} //Delay
        segundos=(I2C0_MDR_R&0xFF); //El Master lee lo que envIa el DS1307

        I2C0_MCS_R=(I2C_MCS_STOP|I2C_MCS_START|I2C_MCS_RUN); // CondiciOn de START, habilito al Master y Alto
        esperar();
        for(i=0;i<300;i++){} //Delay
        horas=(I2C0_MDR_R&0xFF); //El Master lee lo que envIa el DS1307

         I2C0_MCS_R=(I2C_MCS_STOP|I2C_MCS_START|I2C_MCS_RUN); // CondiciOn de START, habilito al Master y Alto
         esperar();
         for(i=0;i<300;i++){} //Delay
         fecha=(I2C0_MDR_R&0xFF); //El Master lee lo que envIa el DS1307

         I2C0_MCS_R=(I2C_MCS_STOP|I2C_MCS_START|I2C_MCS_RUN); // CondiciOn de START, habilito al Master y Alto
         esperar();
         for(i=0;i<300;i++){} //Delay
         year=(I2C0_MDR_R&0xFF); //El Master lee lo que envIa el DS1307

}

//*** FunciOn Retardo para pantalla LCD ***
void retardo(uint32_t wait){
    TIMER4_TAILR_R = wait - 1;                          // Cargando el valor de la cuenta
    TIMER4_ICR_R = 0x01;                                // Limpia bandera del Timer4
    TIMER4_CTL_R = 0x01;                                // Habilita el Timer4
    while((TIMER4_RIS_R & (TIMER_RIS_TATORIS)) == 0);   // Esperar a que termine la cuenta
}

//*** FunciOn ingresa valores pantalla LCD ***
void LCD_instruction(uint8_t data_K,int wait){
    GPIO_PORTG_AHB_DATA_R = LCD_INS;        // Entraria una instruccion a la LCD
    GPIO_PORTK_DATA_R = data_K;             // Entrada de instruccion
    GPIO_PORTG_AHB_DATA_R = 0x00;
    retardo(wait);
}


//*** FunciOn inicializa pantalla LCD ***
void LCD_init(){
    SYSCTL_RCGCGPIO_R |= SYSCTL_RCGCGPIO_R6 | SYSCTL_RCGCGPIO_R9;               // Habilita PG y PK
    while((SYSCTL_PRGPIO_R & (SYSCTL_RCGCGPIO_R6 | SYSCTL_RCGCGPIO_R9)) == 0);  // Espera a que PG y PK esten listos para usarse

    SYSCTL_RCGCTIMER_R |= SYSCTL_RCGCTIMER_R4;                  // habilita reloj para Timer4
    while((SYSCTL_RCGCTIMER_R & SYSCTL_RCGCTIMER_R4) == 0);     // Espera a que el Timer4 estA listo para usarse

    TIMER4_CTL_R &= ~0x00000001;    // Deshabilitar el timer
    TIMER4_CFG_R = 0x00;            // Timer de 32 bits
    TIMER4_TAMR_R = 0x01;           // Modo ONE-SHOT

    GPIO_PORTG_AHB_DIR_R = 0x03;    // PG[0,1] como salidas
    GPIO_PORTG_AHB_DEN_R = 0x03;    // PG[0,1] funciones digitales

    GPIO_PORTK_DIR_R = 0xFF;        // PK[0,...,7] como salidas
    GPIO_PORTK_DEN_R = 0xFF;        // PK[0,...,7] funciones digitales

    LCD_instruction(0x38,640);      // ACTIVAR FUNCION. Datos de 8 bits; Dos lIneas;            retardo de 40us
    LCD_instruction(0x0C,640);      // ENCENDER DISPLAY. Encender pantalla; Activar cursor;     retardo de 40us
    LCD_instruction(0x01,24480);    // BORRAR PANTALLA.                                         retardo de 1.53ms
    LCD_instruction(0x06,640);      // SELECCIONAR MODO. Incremental;                           retardo de 40us
}


//*** FunciOn escritura de caracteres por cadena en pantalla LCD ***
void LCD_write(const char* num, const char* sms, int n){
//void LCD_write(const char* num, int n){

    static int i;
    static int j;

    // Para mostrar el nUmero
    //for(i=0;i<10;i++){
    for(i=0;i<16;i++){
        GPIO_PORTG_AHB_DATA_R = LCD_DAT;    // Entrada un valor para imprimir en la LCD
        GPIO_PORTK_DATA_R = num[i];         // Entrada del numero a imprimir
        GPIO_PORTG_AHB_DATA_R = 0x00;
        retardo(688);                       // Retardo de 43us
    }
    LCD_instruction(0xC0,640);              // Posicionar el cursor al principio de la segunda linea

    // Para mostrar el mensaje
    if(n<17){       // Mensajes que no requieren desplazamiento
        for(i=0;i<n;i++){
            GPIO_PORTG_AHB_DATA_R = LCD_DAT;        // Entrada un valor para imprimir en la LCD
            GPIO_PORTK_DATA_R = sms[i];             // Entrada del mensaje de texto a imprimir
            GPIO_PORTG_AHB_DATA_R = 0x00;
            retardo(688);                           // Retardo de 43us
        }
    }else{          // Mensajes que requieren desplazamiento
        for(j=0;j<n-15;j++){
            for(i=j;i<j+16;i++){
                GPIO_PORTG_AHB_DATA_R = LCD_DAT;    // Entrada un valor para imprimir en la LCD
                GPIO_PORTK_DATA_R = sms[i];         // Entrada del mensaje de texto a imprimir
                GPIO_PORTG_AHB_DATA_R = 0x00;
                retardo(688);                       // Retardo de 43us
            }
            LCD_instruction(0xC0,640);              // Posicionar el cursor al principio de la segunda lInea
            retardo(12000000);  //12000000          // Retardo de 0.75s
        }
    }

}

//*** FunciOn de conversiOn de valores en caracteres para desplegar en cadena de caracteres en LCD ***
void conVal(int valor,int corr,int base){
    int unidades=valor%base;
    int decenas=(valor-unidades)/base;

    switch (decenas){
        case 0 :   memset (num+corr,'0',1);
            break;
        case 1 :  memset (num+corr,'1',1);
            break;
        case 2 :  memset (num+corr,'2',1);
            break;
        case 3 :  memset (num+corr,'3',1);
            break;
        case 4 :  memset (num+corr,'4',1);
                    break;
        case 5 :  memset (num+corr,'5',1);
                    break;
        case 6 :  memset (num+corr,'6',1);
                    break;
        case 7 :  memset (num+corr,'7',1);
                            break;
        case 8 :  memset (num+corr,'8',1);
                                    break;
        case 9 :  memset (num+corr,'9',1);
                                            break;
        default:  memset (num+corr,'?',1);
            break;
    }

    switch (unidades){
           case 0 :  memset (num+1+corr,'0',1);
               break;
           case 1 :  memset (num+1+corr,'1',1);
               break;
           case 2 :   memset (num+1+corr,'2',1);
               break;
           case 3 :   memset (num+1+corr,'3',1);
               break;
           case 4 :   memset (num+1+corr,'4',1);
               break;
           case 5 :   memset (num+1+corr,'5',1);
                         break;
           case 6 :   memset (num+1+corr,'6',1);
                         break;
           case 7 :   memset (num+1+corr,'7',1);
                         break;
           case 8 :   memset (num+1+corr,'8',1);
                         break;
           case 9 :   memset (num+1+corr,'9',1);
                                    break;
           case 10 :
               memset (num+corr,'1',1);
               memset (num+1+corr,'0',1);
                break;
           case 11 :
               memset (num+corr,'1',1);
              memset (num+1+corr,'1',1);
                                    break;
           case 12 :
               memset (num+corr,'1',1);
               memset (num+1+corr,'2',1);
                                    break;
           case 13 :
               memset (num+corr,'1',1);
               memset (num+1+corr,'3',1);
                                    break;
           case 14 :
               memset (num+corr,'1',1);
               memset (num+1+corr,'4',1);
                                    break;
           case 15 :
               memset (num+corr,'1',1);
               memset (num+1+corr,'5',1);
                                    break;
                      default:  memset (num+1+corr,'?',1);
                       break;
       }
 }

/****Programa principal*****/
int main(void){

    I2C_Init(); //Función que inicializa los relojes, el GPIO y el I2C0
    //Inicializo Slave
    while(I2C0_MCS_R&0x00000001){}; // espera que el I2C esté listo

    //Para transmitir
    //segundos=0x00, minutos=0x00, horas=0x0, dia=0x01, fecha=0x00, mes=0x00, year=0x00;
    CargarFecha(0); // Función para configurar al esclavo RTC DS1307 y carga fecha que borra
    CargarFecha(1); // Función para configurar al esclavo RTC DS1307 y carga fecha

    //Interrupciones
    //EdgeCounter_Init();           // inicializa la interrupción en el puerto GPIO J

    do{

         LCD_init();

         conVal(fecha,0,16);
        conVal(mes,2,16);
        conVal(year,4,16);
        conVal(horas,8,16);
        conVal(minutos,11,16);
        conVal(segundos,14,16);

       //banderas de detecciOn del final del minuto
        if(segundos==89){
            segundosA=segundos;
        }else if(segundos==0){
            segundosB=segundos;
        }else{
            segundosB=1;
        }

         //evaluacion de Banderas para el final del minuto
        if((segundosB==0)&&(segundosA==89)){
            segundosA=0;
            segundosB=1;
            minutos++;
        }

        if(minutos==90){
           minutos=0;
        }


        const char sms[]="Fecha   Hr mi se";   // Variable para guardar el mensaje de texto
    int n=16;                                           // Variable para guardar el nUmero de caracteres que contiene el mensaje
    LCD_write(num,sms,n);                               // Escribir en la LCD

    leerFecha();

   /* //Secuencia de botones
    if((SW0>0)&&(SW0<=3)){
        minutos=0; //si el boton es presionado entre 1 y 3 veces se reinician los segundos
    }else if((SW0>3)&&(SW0<=6)){
        horas=horas-1; //si el boton es presionado entre 4 y 5 veces se reinician las horas
    }else{
        SW0=0; //de caso contrario el contador se reiniciará es decir cuando el boton sea presionado más de 7 veces
    }*/

        }while(error!=1);//el ciclo no se romperA a menos que exista un fallo en la comunicaciOn

}
