
#include <stdbool.h>
#include <stdint.h>
#include "inc/tm4c1294ncpdt.h"

#include "I2C_LCD.h"

volatile uint32_t Count= 0;
volatile uint32_t freq= 0;
volatile uint32_t inicio= 0;
volatile uint32_t fin= 0;

volatile uint32_t retlec=400000;//retraso de lecturas  100[ms]

volatile uint32_t Rojo_Frec;
volatile uint32_t Verde_Frec;
volatile uint32_t Azul_Frec;

//*********************************
//** RUTINA DE SERVICIO DE INTERRUPCIÓN ***
//**CONTADOR PARA LA FUNCION DE CUENTA POR FLANCO

void Timer03AIntHandler(void)
{
   TIMER3_ICR_R= 0X00000004 ; //LIMPIA BANDERA DE EVENTO DE CAPTURA
    Count = Count + 1;       // Cuenta el número de flancos de subida

 }

void pulseIn(){
    inicio=Count;
    SysTick_Wait(400000);//  400000 100[ms]
    fin=Count;
    freq=(fin-inicio)/1;
}


//************************************************************
// CONFIGURACIÓN PARA CAPTURA EN LA ENTRADA DEL TIMER 3A
 main(void) {

     Count = 0;
     SysTick_Init();

     //habilita PORTN, PORTF, PORTL PORTJ
           SYSCTL_RCGCGPIO_R |= 0X1F3A; // RELOJ PARA EL PUERTO N,L,M,K,J,F,E,D,B
           SYSCTL_RCGCTIMER_R |= 0X08; //RELOJ Y HABILITA TIMER 3 (p.380)

              //retardo para que el reloj alcance el PORTN Y TIMER 3
              while ((SYSCTL_PRGPIO_R & 0X1F3A) == 0){};  // reloj listo?
     //habilita PORTD
     // SYSCTL_RCGCGPIO_R = SYSCTL_RCGCGPIO_R3; //HABILITA PORTD

        // SYSCTL_RCGCTIMER_R |= 0X08; //HABILITA TIMER 3
         //while ((SYSCTL_PRGPIO_R & 0X0008) == 0){};  // reloj listo?

         //CONFIGURAR LA TERMINAL DE ENTRADA DEL MODULO T3CCP0
         //CORRESPONDIENTE A PD4
               // habilita el bit 4 como digital
               // configura como entrada
               //
               GPIO_PORTD_AHB_DIR_R &= ~(0x10); //bit 4 entrada
               GPIO_PORTD_AHB_DEN_R |= 0x10; //BIT 4 DIGITAL
               GPIO_PORTD_AHB_DATA_R = 0x00; //RESTO DE SALIDAS A 0
               GPIO_PORTD_AHB_AFSEL_R = 0x10; //FUNCION ALTERNA EN BIT 4
               GPIO_PORTD_AHB_PCTL_R = 0x00030000; //DIRIGIDO A T3CCP0


         TIMER3_TAILR_R= 0X000030D4; // VALOR DE RECARGA (p.1004)//VALOR 12500 (YA NO FUNCIONA)
         TIMER3_CTL_R=0X00000000; //DESHABILITA TIMER EN LA CONFIGURACION
         TIMER3_CFG_R= 0X00000004; //CONFIGURAR PARA 16 BITS
         TIMER3_TAMR_R= 0X00000007; //CONFIGURAR PARA MODO PERIODICO CUENTA HACIA ABAJO MODO CAPTURA
        // TIMER3_TAMR_R= 0X00000017; //CONFIGURAR PARA MODO PERIODICO CUENTA HACIA ARRIBA MODO CAPTURA
         TIMER3_CTL_R &=0XFFFFFFF3; // EVENTO FLANCO DE SUBIDA (CONSIDERANDO CONDICIONES INICIALES DE REGISTRO)
         TIMER3_ICR_R= 0X00000004 ; //LIMPIA POSIBLE BANDERA PENDIENTE DE CAPTURA
         TIMER3_IMR_R |= 0X00000004; //ACTIVA INTERRUPCION DE CAPTURA
         NVIC_EN1_R= 1<<(35-32); //HABILITA LA INTERRUPCION DE  TIMER 3
         TIMER3_CTL_R |= 0X00000001; //HABILITA TIMER EN LA CONFIGURACION

         GPIO_PORTL_DIR_R = 0x0F;
         GPIO_PORTL_DEN_R = 0x0F;

         GPIO_PORTK_DIR_R = 0x0F;
         GPIO_PORTK_DEN_R = 0x0F;

         for(;;){

            /* inicio=Count;
             SysTick_Wait(400000);//  400000 100[ms]
             fin=Count;
             freq=(fin-inicio)/1;*/
             //pulseIn();
                         SysTick_Wait(retlec);//  400000 100[ms]
                         GPIO_PORTK_DATA_R = 0x00;// S2 y S3 ceros
                         //GPIO_PORTL_DATA_R = 0x0F;// S2 y S3 ceros
                         SysTick_Wait(retlec);//  400000 100[ms]
                         pulseIn();
                          Rojo_Frec=freq;
                          SysTick_Wait(retlec);//  400000 100[ms]
                          GPIO_PORTK_DATA_R = 0x03;// S2 y S3 unos
                          SysTick_Wait(retlec);//  400000 100[ms]
                          pulseIn();
                          Verde_Frec=freq;
                          SysTick_Wait(retlec);//  400000 100[ms]
                          GPIO_PORTK_DATA_R = 0x02;// S2 cero y S3 uno
                          SysTick_Wait(retlec);//  400000 100[ms]
                          pulseIn();
                          Azul_Frec=freq;
                          SysTick_Wait(retlec);// 400000 100[ms]
         }



         //while(1);

}

