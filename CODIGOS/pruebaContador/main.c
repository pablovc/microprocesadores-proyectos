
/* This example code measure RPM of motor using TM4C123 Timer3A as a counter */
/* Timer1A is used to create one second delay*/

/*header files for TM4C123 device and sprintf library */
//#include "TM4C123GH6PM.h"
#include <stdio.h>


/*Function prototype for Timer0A and UART module initialization */
void timer1A_delaySec(int ttime);
int Timer3A_countCapture(void);
void Timer3ACounter_init(void);
void UART5_init(void);
void UART5_Transmitter(unsigned char data);
void printstring(char *str);

/* global variables to store and display pulse width or duration */
unsigned int counter;
char mesg[12];

/* main code to take RPM measurements and send data to UART terminal */

int main(void)
{
   Timer3ACounter_init();
   UART5_init();
  while(1)
    {
      TIMER3->CTL |= 1;   /* enable timer3A */
      timer1A_delaySec(1);
      counter = Timer3A_countCapture();
      TIMER3->CTL &= ~1; /* disable TIMER3A during setup */
      TIMER3->TAV = 0;
      TIMER3->TAR = 0;
      sprintf(mesg, "\r\nRPM = %d RPM", counter*60); /* convert float to string */
      printstring(mesg); /* print frequency on serial monitor*/
    }

}

/* Configure Timer3A in input-edge counter mode */
void Timer3ACounter_init(void)
{
SYSCTL->RCGCTIMER |= (1<<3);  /* enable clock to Timer Block 3  */
SYSCTL->RCGCGPIO |= (1<<1);      /* enable clock to PORTB  */

GPIOB->DIR &= ~(1<<2);        /* make PB2 an input pin */
GPIOB->DEN |= (1<<2);         /* make PB2 a digital pin */
GPIOB->AFSEL |= (1<<2);       /* enable alternate function on PB2 */
GPIOB->PCTL &= ~0x00000F00;  /* configure PB2 as T3CCP0 pin */
GPIOB->PCTL |= 0x00000700;

TIMER3->CTL &= ~(1<<0);  /* disable TIMER3A during setup */
TIMER3->CFG |= (1<<2);  /* configure as 16-bit timer mode */
TIMER3->TAMR = 0x13;        /* up-count, edge-count, capture mode */
TIMER3->TAMATCHR = 0xFFFF;  /* set the count limit */
TIMER3->TAPMR = 0xFF;         /* to 0xFFFFFF with prescaler */
TIMER3->CTL |= ~(1<<3)|~(1<<2); /* capture the rising edge */
}

/* this routine will execute after every one second */

int Timer3A_countCapture(void)
{
    return TIMER3->TAR;
}


/* Create one second delay using Timer block 1 and sub timer A */

void timer1A_delaySec(int ttime)
{
    int i;
    SYSCTL->RCGCTIMER |= 2;     /* enable clock to Timer Block 1 */

TIMER1->CTL = 0;            /* disable Timer before initialization */
    TIMER1->CFG = 0x04;         /* 16-bit option */
    TIMER1->TAMR = 0x02;        /* periodic mode and down-counter */
    TIMER1->TAILR = 64000 - 1;  /* TimerA interval load value reg */
    TIMER1->TAPR = 250 - 1;     /* TimerA Prescaler 16MHz/250=64000Hz */
    TIMER1->ICR = 0x1;          /* clear the TimerA timeout flag */
    TIMER1->CTL |= 0x01;        /* enable Timer A after initialization */

    for(i = 0; i < ttime; i++)
    {
        while ((TIMER1->RIS & 0x1) == 0) ;      /* wait for TimerA timeout flag */
        TIMER1->ICR = 0x1;      /* clear the TimerA timeout flag */
    }
}
void UART5_init(void)
{
    SYSCTL->RCGCUART |= 0x20;  /* enable clock to UART5 */
    SYSCTL->RCGCGPIO |= 0x10;  /* enable clock to PORTE for PE4/Rx and RE5/Tx */
    /* UART0 initialization */
    UART5->CTL = 0;         /* UART5 module disbable */
    UART5->IBRD = 104;      /* for 9600 baud rate, integer = 104 */
    UART5->FBRD = 11;       /* for 9600 baud rate, fractional = 11*/
    UART5->CC = 0;          /*select system clock*/
    UART5->LCRH = 0x60;     /* data lenght 8-bit, not parity bit, no FIFO */
    UART5->CTL = 0x301;     /* Enable UART5 module, Rx and Tx */

    /* UART5 TX5 and RX5 use PE4 and PE5. Configure them digital and enable alternate function */
    GPIOE->DEN = 0x30;      /* set PE4 and PE5 as digital */
    GPIOE->AFSEL = 0x30;    /* Use PE4,PE5 alternate function */
    GPIOE->AMSEL = 0;    /* Turn off analg function*/
    GPIOE->PCTL = 0x00110000;     /* configure PE4 and PE5 for UART */
}
void UART5_Transmitter(unsigned char data)
{
    while((UART5->FR & (1<<5)) != 0); /* wait until Tx buffer not full */
    UART5->DR = data;                  /* before giving it another byte */
}

void printstring(char *str)
{
  while(*str)
    {
        UART5_Transmitter(*(str++));
    }
}

/* This function is called by the startup assembly code to perform system specific initialization tasks. */
void SystemInit(void)
{
    SCB->CPACR |= 0x00F00000;
}
