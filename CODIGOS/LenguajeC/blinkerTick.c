// SysTick.c
//
//Librerías incluidas
#include <stdbool.h>// Librería para operaciones lógicas
#include <stdint.h> // Librería para operaciones aritméticas

// Definición de apuntadores para direccionar
#define NVIC_ST_CTRL_R          (*((volatile unsigned long *)0xE000E010))  //apuntador del registro que permite configurar las acciones del temporizador
#define NVIC_ST_RELOAD_R        (*((volatile unsigned long *)0xE000E014))  //apuntador del registro que contiene el valor de inicio del contador
#define NVIC_ST_CURRENT_R       (*((volatile unsigned long *)0xE000E018))  //apuntador del registro que presenta el estado de la cuenta actual

// Definición de constantes para operaciones
#define NVIC_ST_CTRL_COUNT      0x00010000  // bandera de cuenta flag
#define NVIC_ST_CTRL_CLK_SRC    0x00000004  // Clock Source
#define NVIC_ST_CTRL_INTEN      0x00000002  // Habilitador de interrupción
#define NVIC_ST_CTRL_ENABLE     0x00000001  // Modo del contador
#define NVIC_ST_RELOAD_M        0x00FFFFFF  // Valor de carga del contador


//**********FUNCION Inizializa el SysTick *********************
void SysTick_Init(void){
  NVIC_ST_CTRL_R = 0;                   // Desahabilita el SysTick durante la configuración
  NVIC_ST_RELOAD_R = NVIC_ST_RELOAD_M;  // Se establece el valor de cuenta deseado en RELOAD_R
  NVIC_ST_CURRENT_R = 0;                // Se escribe al registro current para limpiarlo

  NVIC_ST_CTRL_R = 0x00000001;         // Se Habilita el SysTick y se selecciona la fuente de reloj
}

//*************FUNCION Tiempo de retardo utilizando wait.***************
// El parametro de retardo esta en unidades del reloj interno/4 = 4 MHz (250 ns)
void SysTick_Wait(uint32_t retardo){
    NVIC_ST_RELOAD_R= retardo-1;   //número de cuentas por esperar
	NVIC_ST_CURRENT_R = 0;
	while((NVIC_ST_CTRL_R&0x00010000)==0){//espera hasta que la bandera COUNT sea valida
	}
   } //
	
// Tiempo de retardo utilizando wait
// 4000000 igual a 1 s
void SysTick_Wait_2s(int retardo){
    int i=0;
    for(i=0; i<retardo; i++){
    SysTick_Wait(4000000);  // Espera 1 s (asume reloj de  4 MHz)
  }
}

//******* CÓDIGO PRINCIPAL *******

// Definición de apuntadores para direccionar
#define GPIO_PORTN_DATA_R       (*((volatile unsigned long *)0x4006400C))
#define GPIO_PORTN_DIR_R        (*((volatile unsigned long *)0x40064400))
#define GPIO_PORTN_DEN_R        (*((volatile unsigned long *)0x4006451C))
#define SYSCTL_RCGC2_R          (*((volatile unsigned long *)0x400FE608))
#define SYSCTL_PRGPIO_R         (*((volatile unsigned long *)0x400FEA08))

// Definición de constantes para operaciones
#define SYSCTL_RCGC2_GPION      0x00001000  // bit de estado del reloj de puerto N

int main(void){
  SYSCTL_RCGC2_R |= SYSCTL_RCGC2_GPION; // activa reloj puerto N
  SysTick_Init();           // inicializo SysTick
  GPIO_PORTN_DIR_R |= 0x0F;    // puerto N de salida
  GPIO_PORTN_DEN_R |= 0x0F;    // habilita el puerto N
  GPIO_PORTN_DATA_R = 0x02;    // enciende 1 led

  while(1){
    GPIO_PORTN_DATA_R = GPIO_PORTN_DATA_R^0x03; // conmuta encendido de leds

    SysTick_Wait_2s(2);    // aproximadamente 2 s
  }
}
