//Frecuencias de LED

//recordar habilitar los manejadores del servicio de interrupciones (Handlers)

 // Interrupción por flanco de bajada en PJ0 = SW1
// La Rutina de Servicio de InterrupciOn incrementa a un  contador.

// LIBRERIAS QUE UTILIZA EL PROGRAMA
#include <stdbool.h>
#include <stdint.h>
#include "inc/tm4c1294ncpdt.h"
volatile uint32_t Count= 0;
volatile uint32_t Termino= 0;

volatile uint32_t SW0 = 0;
volatile uint32_t banderaTimer = 0;

// DefiniciOn de apuntadores para direccionar
#define GPIO_PORTN_DATA_R       (*((volatile unsigned long *)0x4006400C)) // Registro de Datos Puerto N
#define GPIO_PORTN_DIR_R        (*((volatile unsigned long *)0x40064400)) // Registro de Dirección Puerto N
#define GPIO_PORTN_DEN_R        (*((volatile unsigned long *)0x4006451C)) // Registro de Habilitación Puerto N
#define SYSCTL_RCGC2_R          (*((volatile unsigned long *)0x400FE608)) // Registro de Habilitación de Reloj de Puertos
#define SYSCTL_PRGPIO_R         (*((volatile unsigned long *)0x400FEA08)) // Registro de estatus de Reloj de Puerto

// DefiniciOn de constantes para operaciones
#define SYSCTL_RCGC2_GPION      0x00001000  // bit de estado del reloj de puerto N

#define GPIO_PORTJ_DIR_R        (*((volatile uint32_t *)0x40060000)) //Registro de Dirección PJ , todos los pines del puerto como entradas(p. 760)
#define GPIO_PORTJ_DEN_R        (*((volatile uint32_t *)0x4006051C)) //Registro de habilitación PJ (p. 781)
#define GPIO_PORTJ_PUR_R        (*((volatile uint32_t *)0x40060510)) //Registro de pull-up PJ
#define GPIO_PORTJ_DATA_R       (*((volatile uint32_t *)0x40060004)) //Registro de Datos J
#define GPIO_PORTJ_IS_R         (*((volatile uint32_t *)0x40060404)) //Registro de configuración de detección de nivel o flanco
#define GPIO_PORTJ_IBE_R        (*((volatile uint32_t *)0x40060408)) //Registro de configuración de interrupción por ambos flancos
#define GPIO_PORTJ_IEV_R        (*((volatile uint32_t *)0x4006040C)) //Registro de configuración de interrupción por un flanco
#define GPIO_PORTJ_ICR_R        (*((volatile uint32_t *)0x4006041C)) //Registro de limpieza de interrupcion de flanco en PJ
#define GPIO_PORTJ_IM_R         (*((volatile uint32_t *)0x40060410)) //Registro de mascara de interrupcion PJ (p.764)
#define NVIC_EN1_R              (*((volatile uint32_t *)0xE000E104)) // Registro de habilitación de interrupción PJ
#define NVIC_PRI12_R             (*((volatile uint32_t *)0xE000E430))//Registro de prioridad de interrupción

// RUTINA DE SERVICIO DE INTERRUPCIÓN
void Timer03AIntHandler(void)
{
    //LIMPIA BANDERA
    TIMER3_ICR_R= 0X00000001 ; //LIMPIA BANDERA DE TIMER3

    Termino = Termino + 1;

     if (Termino < 10000)//Limite en el timer
     {
    Count = Count + 0x01;

    switch (Count&0x0F) {
              case 0x01:
                GPIO_PORTN_DATA_R = 0x00; // LED APAGADO
                break;
              case 0x02:
                GPIO_PORTN_DATA_R = 0x01; // LED ENCENDIDO
                Count = 0x0;
                break;
       }
      }else{
          GPIO_PORTN_DATA_R=0x00; //CEROS
      }
    }

void EdgeCounter_Init(void){
  SYSCTL_RCGCGPIO_R |= 0x00000100; // activa el reloj para el puerto J
 SW0=0;

  GPIO_PORTJ_DIR_R &= ~0x03;       //  PJ0 dirección entrada - boton SW1
    GPIO_PORTJ_DEN_R |= 0x03;        //     PJ0 se habilita
    GPIO_PORTJ_PUR_R |= 0x03;        //     habilita weak pull-up on PJ1
    GPIO_PORTJ_IS_R &= ~0x03;        //     PJ1 es sensible por flanco (p.761)
    GPIO_PORTJ_IBE_R &= ~0x03;       //     PJ1 no es sensible a dos flancos (p. 762)
    GPIO_PORTJ_IEV_R &= ~0x03;       //     PJ1 detecta eventos de flanco de bajada (p.763)
    GPIO_PORTJ_ICR_R = 0x03;         //     limpia la bandera 0 (p.769)
    GPIO_PORTJ_IM_R |= 0x03;         //     Se desenmascara la interrupcion PJ0 y se envia al controlador de interrupciones (p.764)

  NVIC_PRI12_R = (NVIC_PRI12_R&0x00FFFFFF)|0x00000000; //   prioridad 0  (p. 159)
  NVIC_EN1_R= 1<<(51-32);          //  habilita la interrupción 51 en NVIC (p. 154)
}

void GPIOPortJ_Handler(void)
{
            GPIO_PORTJ_ICR_R = 0x01;
            SW0=SW0+1;
}

void Timer3_Init(uint32_t periodo,uint32_t modo){
    TIMER3_CTL_R=0X00000000; //DESHABILITA TIMER 3 PARA CONFIGURAR (p.986)
    TIMER3_CFG_R= 0X00000000; //CONFIGURA TIMER DE 32 BITS (p. 976)
    if(modo==1){
        TIMER3_TAMR_R= 0X00000012; //CONFIGURAR PARA MODO PERIODICO CUENTA HACIA ARRIBA (p. 977)
    }else if(modo==2){
        TIMER3_TAMR_R= 0X00000002; //CONFIGURAR PARA MODO PERIODICO CUENTA HACIA ABAJO (p. 977)
    }
    TIMER3_TAILR_R= periodo; // VALOR DE RECARGA (p.1004)
    TIMER3_TAPR_R= 0X00; // PRESCALADOR DE TIMER A, SOLO PARA MODOS DE 16 BITS (p.1008)
    TIMER3_ICR_R= 0X00000001 ; //LIMPIA POSIBLE BANDERA PENDIENTE DE TIMER3 (p.1002)
    TIMER3_IMR_R |= 0X00000001; //ACTIVA INTRRUPCION DE TIMEOUT (p.993)
    NVIC_PRI8_R = (NVIC_PRI8_R&0x00FFFFFF)|0x40000000; // Establecer la prioridad en el registro 2 de prioridades del NVIC correcto
    NVIC_EN1_R= 1<<(35-32); //HABILITA LA INTERRUPCION 35 (TIMER3 A)
    TIMER3_CTL_R |= 0X00000001; //HABILITA TIMER 3 (p.986)
}


//Programa principal
int main(void){

     SYSCTL_RCGCGPIO_R |= 0X1420; // RELOJ PARA EL PUERTO F, L y N
     EdgeCounter_Init();           // inicializa la interrupción en el puerto GPIO J
     SYSCTL_RCGCTIMER_R |= 0x0008; // 0) activate timer 3 RELOJ Y HABILITA TIMER 3 (p.380)

     while ((SYSCTL_PRGPIO_R & 0X1420) == 0){};  // reloj listo?

     //ConfiguraciOn del puerto N
     GPIO_PORTN_DIR_R = 0x0F;
     GPIO_PORTN_DEN_R = 0x0F;
     GPIO_PORTN_DATA_R = 0x09;

     while(1){

         switch(SW0){
            case 1:
              Timer3_Init(0X0003FFFF,1);
              banderaTimer=1;
               break;
            case 2:
               Timer3_Init(0X0006FFFF,1);
               banderaTimer=2;
                break;
            case 3:
              Timer3_Init(0X009FFFFF,1);
              banderaTimer=3;
               break;
            case 4:
                Timer3_Init(0X00FFFFFF,1);
                 banderaTimer=4;
                 break;
            break;
            case 5:
                 Timer3_Init(0XFFFFFFFF,2);
                  banderaTimer=5;
                  SW0=0;
                  break;
            default:
               banderaTimer=0;
            break;
         }//fin switch case

     }//fin while infinito
}//fin programa principal

