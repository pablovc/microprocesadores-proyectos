/*Asignatura: Microcontroladores y microprocesadores.   *Semestre: 2020-2.
*Ejercicio: Reloj Digital I2C
*Presenta Pablo Vivar Colina.
-------------------------------------------------------------------------------
*/

/******PUERTOS UTILIZADOS*****
*PORTB-> ComunicaciOn Serial RTC
*PORTG-> Manejo Renglones LCD 16x2
*PORTK-> Manejo Datos LCD
*PORTE-> Fotoresistencias
*PORTL-> Motor a Pasos
*PORTD->  Lectura Joysticks (ADC)
*PORTN-> Motor a Pasos (usar 1)
*/


/*****DESCRIPCION****
 * Seguidor Solar que se apoya de función de Reloj Digital, el cual usa el modulo SPI (i2c) RTC (real time clock) con el cual el microcontrolador TIVA se comunica escribiendo y leyendo
 * el dato de fecha y hora, Este dato es desplegado en una pantalla LCD
 *
 */


/****DIAGRAMA***
 *  _________  _________
 *  |        | |        |
 *  |BYJ_28  | |BYJ_28  |
 *  |________| |________|
 *    /\  /\     /\  /\
 *  __|___|__  __|___|__
 *  |        | |        |
 *  |ULN2003A| |ULN2003A|
 *  |________| |________|
 *    /\ /\       /\ /\       ______
 * ___|__|________|__|_       |     |
 * |                   |->SDA | RTC |
 * |  TIVA TM4C1294NCPD|<-SCL | i2c |
 * |___________________|      |_____|
 *    /\  /\    | | | |
 *   _|___|_   _V V V V_____Dato de 8 bit
 *   |JoyStc|  | LCD       |
 *   |______|  |___________|
 *
 */

/*****CONEXIONES******
 * PotenciOmetro: pin1->GND pin2-> V0 LCD pin3-> +5V
 * LCD 1602A a TIVA
 * VSS-> GND
 * VDD-> +5V
 * V0-2 pin2 potenciOmetro
 *RS-> PG0
 *E -> PG1
 *RW-> GND
 *D0->PK0
 *D1->PK1
 *D2->PK2
 *D3->PK3
 *D4->PK4
 *D5->PK5
 *D6->PK6
 *D7->PK7
 *A-> +5V resistor recomendable de 330 Ohm
 *K-> GND
 * RTC DS1307 a TIVA
 * GND-> GND
 * 5V->5V
 * SDA->PB3
 * SCL->PB2
 */

/***NOTAS DE STARTUP***
 * modificar startup para handler del puerto J con intecnion de que el servicio de interrupciones funcione adecuadamente
 *
 */




//5Motores
/*Asignatura: Microcontroladores y microprocesadores.   *Semestre: 2019-2.
*Proyecto final
*Temas:
*->Convertidor AnalOgico Digital
*->Motor a pasos unipolar.
*Presenta: Pablo Vivar Colina.
-------------------------------------------------------------------------------
*/

/***** PUERTOS UTILIZADOS *****
*PORTE-> Lectura Joysticks (ADC)
*PORTB-> Lectura 4 ADC adicionales
*PORTM-> Motor a paso
*PORTL-> Motor a paso
*PORTN-> Motor a paso
*PORTK-> Motor a paso
*/

//DESCRIPCION
/*
*El Programa utiliza al DAC para obtener el valor de la señal que entra a PE4,PE5,PE1,PE2
* Las lecturas de los puertos anteriores activan los puertos M,L,N y K
* para activar motores a pasos en grua cartesiana
*El disparo del DAC es por software
*/

// LIBRERIAS QUE UTILIZA EL PROGRAMA
#include <stdbool.h>
#include <stdint.h> // Biblioteca para aritmEtica
#include <math.h>
#include "inc/tm4c1294ncpdt.h" //Biblioteca Tiva Ware

//PUERTO E (Joysticks)
#define GPIO_PORTE_AHB_DIR_R    (*((volatile uint32_t *)0x4005C400))
#define GPIO_PORTE_AHB_AFSEL_R  (*((volatile uint32_t *)0x4005C420))
#define GPIO_PORTE_AHB_DEN_R    (*((volatile uint32_t *)0x4005C51C))
#define GPIO_PORTE_AHB_AMSEL_R  (*((volatile uint32_t *)0x4005C528))

//PUERTO D
#define GPIO_PORTD_AHB_DIR_R    (*((volatile uint32_t *)0x4005B400))
#define GPIO_PORTD_AHB_AFSEL_R  (*((volatile uint32_t *)0x4005B420))
#define GPIO_PORTD_AHB_DEN_R    (*((volatile uint32_t *)0x4005B51C))
#define GPIO_PORTD_AHB_AMSEL_R  (*((volatile uint32_t *)0x4005B528))

#define SYSCTL_RCGCGPIO_R       (*((volatile uint32_t *)0x400FE608)) //Registro para habilitar reloj de GPIO
#define SYSCTL_PRGPIO_R         (*((volatile uint32_t *)0x400FEA08)) // Registro para verificar si el reloj esta listo (p.499)
#define SYSCTL_RCGCADC_R        (*((volatile uint32_t *)0x400FE638)) // Registro para habilitar el reloj al ADC(p. 396)
#define SYSCTL_PRADC_R          (*((volatile uint32_t *)0x400FEA38)) // Registro para verificar si el ADC esta listo (p.515)
//REGISTROS ADC 0
#define ADC0_PC_R               (*((volatile uint32_t *)0x40038FC4)) // Registro para configurar tasa de muestreo (p.1159)
#define ADC0_SSPRI_R            (*((volatile uint32_t *)0x40038020)) // Registro para configurar la prioridad del secuenciador (p.1099)
#define ADC0_ACTSS_R            (*((volatile uint32_t *)0x40038000)) // Registro para controlar la activación del secuenciador (p. 1076)
#define ADC0_EMUX_R             (*((volatile uint32_t *)0x40038014)) // ACDEMUX Registro para seleccionar el evento (trigger) que inicia el muestreo en cada secuenciador (p.1091)
#define ADC0_IM_R               (*((volatile uint32_t *)0x40038008)) // Registro que controla la mascara de interrupciones en secuenciadores (p. 1081)
#define ADC0_ACTSS_R            (*((volatile uint32_t *)0x40038000)) // Registro que controla la activación de los secuenciadores (p.1077)
#define ADC0_ISC_R              (*((volatile uint32_t *)0x4003800C)) //Registro de estatus y para borrar las condiciones de interrupción del secuenciador (p. 1084)
#define ADC0_PSSI_R             (*((volatile uint32_t *)0x40038028)) //Registro que permite al software iniciar el muestreo en los secuenciadores (p. 1102)
#define ADC0_RIS_R              (*((volatile uint32_t *)0x40038004)) //Registro muestra el estado de la señal de interrupción de cada secuenciador (p.1079  )


//Registros ADC 1
#define ADC1_PC_R               (*((volatile uint32_t *)0x40039FC4)) // Registro para configurar tasa de muestreo (p.1159)
#define ADC1_SSPRI_R            (*((volatile uint32_t *)0x40039020)) // Regidtrp para configurar la prioridad del secuenciador (p.1099)
#define ADC1_ACTSS_R            (*((volatile uint32_t *)0x40039000)) // Registro para controlar la activación del secuenciador (p. 1076)
#define ADC1_EMUX_R             (*((volatile uint32_t *)0x40039014)) // ACDEMUX Registro para seleccionar el evento (trigger) que inicia el muestreo en cada secuenciador (p.1091)
#define ADC1_IM_R               (*((volatile uint32_t *)0x40039008)) // Registro que controla la mascara de interrupciones en secuenciadores (p. 1081)
#define ADC1_ACTSS_R            (*((volatile uint32_t *)0x40039000)) // Registro que controla la activación de los secuenciadores (p.1077)
#define ADC1_ISC_R              (*((volatile uint32_t *)0x4003900C)) //Registro de estatus y para borrar las condiciones de interrupción del secuenciador (p. 1084)
#define ADC1_PSSI_R             (*((volatile uint32_t *)0x40039028)) //Registro que permite al software iniciar el muestreo en los secuenciadores (p. 1102)
#define ADC1_RIS_R              (*((volatile uint32_t *)0x40039004)) //Registro muestra el estado de la señal de interrupción de cada secuenciador (p.1079  )

//ADC 0 input 1
#define ADC0_SSEMUX3_R          (*((volatile uint32_t *)0x400380B8)) // Registro que selecciona entre las entradas AIN[19:16] o AIN[15:0]  (p.1046)
#define ADC0_SSMUX3_R           (*((volatile uint32_t *)0x400380A0)) // Registro para configurar la entrada analógica para el Secuenciador 3 (p.1041)
#define ADC0_SSCTL3_R           (*((volatile uint32_t *)0x400380A4)) // Registro que configura la muestra ejecutada con el Secuenciador 3 (p.1142)
#define ADC0_SSFIFO3_R          (*((volatile uint32_t *)0x400380A8)) //Registro que contiene los resultados de conversión de las muestras recogidas con el secuenciador (p. 1118)
//ADC 0 input2
#define ADC0_SSEMUX2_R          (*((volatile uint32_t *)0x40038098)) // Registro que selecciona entre las entradas AIN[19:16] o AIN[15:0]  (p.37,1137)
#define ADC0_SSMUX2_R           (*((volatile uint32_t *)0x40038080)) // Registro para configurar la entrada analógica para el Secuenciador 2 (p.1041)
#define ADC0_SSCTL2_R           (*((volatile uint32_t *)0x40038084)) // Registro que configura la muestra ejecutada con el Secuenciador 2 (p.37)
#define ADC0_SSFIFO2_R          (*((volatile uint32_t *)0x40038088)) //Registro que contiene los resultados de conversión de las muestras recogidas con el secuenciador (p. 37)
//ADC 0 input3
#define ADC0_SSEMUX1_R          (*((volatile uint32_t *)0x40038078)) // Registro que selecciona entre las entradas AIN[19:16] o AIN[15:0]  (p.37)
#define ADC0_SSMUX1_R           (*((volatile uint32_t *)0x40038060)) // Registro para configurar la entrada analógica para el Secuenciador 1 (p.1074)
#define ADC0_SSCTL1_R           (*((volatile uint32_t *)0x40038064)) // Registro que configura la muestra ejecutada con el Secuenciador 1 (p.1074)
#define ADC0_SSFIFO1_R          (*((volatile uint32_t *)0x40038068)) //Registro que contiene los resultados de conversión de las muestras recogidas con el secuenciador (p. 1074)
//ADC 0 input4
#define ADC0_SSEMUX0_R          (*((volatile uint32_t *)0x40038058)) // Registro que selecciona entre las entradas AIN[19:16] o AIN[15:0]  (p.37)
#define ADC0_SSMUX0_R           (*((volatile uint32_t *)0x40038040)) // Registro para configurar la entrada analógica para el Secuenciador 0 (p.37)
#define ADC0_SSCTL0_R           (*((volatile uint32_t *)0x40038044)) // Registro que configura la muestra ejecutada con el Secuenciador 0 (p.37)
#define ADC0_SSFIFO0_R          (*((volatile uint32_t *)0x40038048)) //Registro que contiene los resultados de conversión de las muestras recogidas con el secuenciador (p. 37)


//ADC 1 input 1
#define ADC1_SSEMUX3_R          (*((volatile uint32_t *)0x400390B8)) // Registro que selecciona entre las entradas AIN[19:16] o AIN[15:0]  (p.1046)
#define ADC1_SSMUX3_R           (*((volatile uint32_t *)0x400390A0)) // Registro para configurar la entrada analógica para el Secuenciador 3 (p.1041)
#define ADC1_SSCTL3_R           (*((volatile uint32_t *)0x400390A4)) // Registro que configura la muestra ejecutada con el Secuenciador 3 (p.1142)
#define ADC1_SSFIFO3_R          (*((volatile uint32_t *)0x400390A8)) //Registro que contiene los resultados de conversión de las muestras recogidas con el secuenciador (p. 1118)
//ADC 1 input2
#define ADC1_SSEMUX2_R          (*((volatile uint32_t *)0x40039098)) // Registro que selecciona entre las entradas AIN[19:16] o AIN[15:0]  (p.37,1137)
#define ADC1_SSMUX2_R           (*((volatile uint32_t *)0x40039080)) // Registro para configurar la entrada analógica para el Secuenciador 2 (p.1041)
#define ADC1_SSCTL2_R           (*((volatile uint32_t *)0x40039084)) // Registro que configura la muestra ejecutada con el Secuenciador 2 (p.37)
#define ADC1_SSFIFO2_R          (*((volatile uint32_t *)0x40039088)) //Registro que contiene los resultados de conversión de las muestras recogidas con el secuenciador (p. 37)
//ADC 1 input3
#define ADC1_SSEMUX1_R          (*((volatile uint32_t *)0x40039078)) // Registro que selecciona entre las entradas AIN[19:16] o AIN[15:0]  (p.37)
#define ADC1_SSMUX1_R           (*((volatile uint32_t *)0x40039060)) // Registro para configurar la entrada analógica para el Secuenciador 1 (p.1074)
#define ADC1_SSCTL1_R           (*((volatile uint32_t *)0x40039064)) // Registro que configura la muestra ejecutada con el Secuenciador 1 (p.1074)
#define ADC1_SSFIFO1_R          (*((volatile uint32_t *)0x40039068)) //Registro que contiene los resultados de conversión de las muestras recogidas con el secuenciador (p. 1074)
//ADC 1 input4
#define ADC1_SSEMUX0_R          (*((volatile uint32_t *)0x40039058)) // Registro que selecciona entre las entradas AIN[19:16] o AIN[15:0]  (p.37)
#define ADC1_SSMUX0_R           (*((volatile uint32_t *)0x40039040)) // Registro para configurar la entrada analógica para el Secuenciador 0 (p.37)
#define ADC1_SSCTL0_R           (*((volatile uint32_t *)0x40039044)) // Registro que configura la muestra ejecutada con el Secuenciador 0 (p.37)
#define ADC1_SSFIFO0_R          (*((volatile uint32_t *)0x40039048)) //Registro que contiene los resultados de conversión de las muestras recogidas con el secuenciador (p. 37)

//PLL
#define SYSCTL_PLLFREQ0_R       (*((volatile uint32_t *)0x400FE160)) //Registro para configurar el PLL
#define SYSCTL_PLLSTAT_R        (*((volatile uint32_t *)0x400FE168)) //Registro muestra el estado de encendido del PLL
#define SYSCTL_PLLFREQ0_PLLPWR  0x00800000  // Valor para encender el PLL

//Valores posicion potenciOmetros fotoresistencias
volatile uint32_t LEC0;
volatile uint32_t LEC1;
volatile uint32_t LEC2;
volatile uint32_t LEC3;

//ADC 1
volatile uint32_t movX;
volatile uint32_t movY;

//variables contadores y secuencias
uint32_t volatile Num=0;
uint32_t volatile i=0;
uint32_t volatile j=0;
uint32_t volatile paso=0;

//PUERTO L (motor a pasos para movimiento angulo rho)
volatile uint32_t CountL= 0;
volatile uint32_t TerminoL= 0;
volatile uint32_t limiteL=800;
volatile uint32_t accionL=0;

//PUERTO N (motor a pasos para movimiento angulo phi)
volatile uint32_t CountN= 0;
volatile uint32_t TerminoN= 0;
volatile uint32_t limiteN=800;
volatile uint32_t accionN=0;


//MOTOR A PASOS
/*REGLA  32 * 64 = 2048
* sec grados
*   8  1.40625
*  16  2.8125
*  32  5.625
*  64 11.25
* 128 22.5
* 256 45
* 512 90
*/

enum secMotores {
    AB=0x03,
    ApB=0x06,
    ApBp=0x0C,
    ABp=0x09,
    NN=0x00,
    HIGH=0x01,
    DHIGH=0x02,
    LOW=0x00
};

// RUTINA DE SERVICIO DE INTERRUPCIÓN

void Timer03AIntHandler(void)
{

    //LIMPIA BANDERA
         TIMER3_ICR_R= 0X00000001 ; //LIMPIA BANDERA DE TIMER3
       //primer bloque de secuencias
         if(paso==0){
             if(accionN==1){
                TerminoN = TerminoN + 1;
                  if (TerminoN < limiteN){
                    CountL = (CountN+0x01) % 0x04;
                    switch (CountN) {
                       case 0x00:
                         GPIO_PORTN_DATA_R=AB; // A,B
                           break;
                       case 0x01:
                         GPIO_PORTN_DATA_R=ApB; // A',B
                           break;
                       case 0x02:
                         GPIO_PORTN_DATA_R=ApBp; // A', B'
                           break;
                       case 0x03:
                         GPIO_PORTN_DATA_R=ABp; // A,B'
                          break;
                       default:
                          break;
                   }
                 }
               }

               else if(accionN==2){
                TerminoN = TerminoN + 1;
                  if (TerminoN < limiteN){

                    CountN = (CountN+0x01)%0x04;

                switch (CountN) {
                           case 0x00:
                             GPIO_PORTN_DATA_R=ABp; // A,B'
                               break;
                           case 0x01:
                             GPIO_PORTN_DATA_R=ApBp; // A', B'
                               break;
                           case 0x02:
                             GPIO_PORTN_DATA_R=ApB; // A',B
                               break;
                           case 0x03:
                             GPIO_PORTN_DATA_R=AB; // A,B
                              break;
                   }
                  }

               }

               else if(accionN==0){
                   GPIO_PORTN_DATA_R=LOW;
               }//fin accionN

               if(TerminoN==limiteN){
                  GPIO_PORTN_DATA_R=LOW;
                  paso=1;
               }
          }else if(paso==1){
              //segundo paso de secuancias
              if(accionL==1){
                 TerminoL = TerminoL + 1;
                   if (TerminoL < limiteL){
                     CountL = (CountL+0x01) % 0x04;
                     switch (CountL) {
                        case 0x00:
                          GPIO_PORTL_DATA_R=AB; // A,B
                            break;
                        case 0x01:
                          GPIO_PORTL_DATA_R=ApB; // A',B
                            break;
                        case 0x02:
                          GPIO_PORTL_DATA_R=ApBp; // A', B'
                            break;
                        case 0x03:
                          GPIO_PORTL_DATA_R=ABp; // A,B'
                           break;
                        default:
                           break;
                    }
                  }
                }

                else if(accionL==2){
                 TerminoL = TerminoL + 1;
                   if (TerminoL < limiteL){

                     CountL = (CountL+0x01)%0x04;

                 switch (CountL) {
                            case 0x00:
                              GPIO_PORTL_DATA_R=ABp; // A,B'
                                break;
                            case 0x01:
                              GPIO_PORTL_DATA_R=ApBp; // A', B'
                                break;
                            case 0x02:
                              GPIO_PORTL_DATA_R=ApB; // A',B
                                break;
                            case 0x03:
                              GPIO_PORTL_DATA_R=AB; // A,B
                               break;
                    }
                   }

                }

                else if(accionL==0){
                    GPIO_PORTL_DATA_R=LOW;
                }//fin accionL

                if(TerminoL==limiteL){
                   GPIO_PORTL_DATA_R=LOW;
                   paso=2;
                }
          }else if(paso==2){
              //tercer paso de secuencias
              GPIO_PORTN_DATA_R=LOW;
              GPIO_PORTL_DATA_R=LOW;
          }
        //fin ciclo de paso

       }//FIN TIMER 3B

void main(void){

    //SYSCTL_RCGCGPIO_R |= 0X1E18; // 1) HABILITA RELOJ PARA EL PUERTOS D,E,M,K,L,N (p. 382)


    //SYSCTL_RCGCGPIO_R |= 0X1E12; // 1) HABILITA RELOJ PARA EL PUERTOS B,E,M,K,L,N (p. 382)
    //SYSCTL_RCGCGPIO_R |= 0X1E10; // 1) HABILITA RELOJ PARA EL PUERTOS E,M,K,L,N (p. 382)
    SYSCTL_RCGCGPIO_R |= 0X1E18; // 1) HABILITA RELOJ PARA EL PUERTOS B,E,M,K,L,N (p. 382)

    //MOTORES A PASOS
    SYSCTL_RCGCTIMER_R |= 0X08; //RELOJ Y HABILITA TIMER 3 (p.380)
    //retardo para que el reloj alcance los puertos E,M,K,L,N Y TIMER 3
    //while ((SYSCTL_PRGPIO_R & 0X1E12) == 0){};  // reloj listo?
    while ((SYSCTL_PRGPIO_R & 0X1E18) == 0){};  // reloj listo?

    TIMER3_CTL_R=0X00000000; //DESHABILITA TIMER 3 PARA CONFIGURAR (p.986)
    TIMER3_CFG_R= 0X00000000; //CONFIGURA TIMER DE 32 BITS (p. 976)
    //TIMER3_TAMR_R= 0X00000002; //CONFIGURAR PARA MODO PERIODICO CUENTA HACIA ABAJO (p. 977)
    TIMER3_TAILR_R= 0X00007530; // VALOR DE RECARGA (p.1004)
    TIMER3_TAPR_R= 0X00; // PRESCALADOR DE TIMER A, SOLO PARA MODOS DE 16 BITS (p.1008)
    TIMER3_ICR_R= 0X00000001 ; //LIMPIA POSIBLE BANDERA PENDIENTE DE TIMER3 (p.1002)
    TIMER3_IMR_R |= 0X00000001; //ACTIVA INTRRUPCION DE TIMEOUT (p.993)
    NVIC_EN1_R= 1<<(35-32); //HABILITA LA INTERRUPCION 35 (TIMER3 A)
    TIMER3_CTL_R |= 0X00000001; //HABILITA TIMER 3 (p.986)

    // habilita al Puerto L como salida digital para control de motor
    // PL0,...,PL3 como salidas hacia el ULN2003 (A,A´,B,B´)
    GPIO_PORTL_DIR_R = 0x0F;//->F habilita los primeros 4 bits del puerto
    GPIO_PORTL_DEN_R = 0x0F;//(F->1111)
    GPIO_PORTL_DATA_R = 0x09;//9->1001
    // habilita al Puerto F como salida digital para control de motor
    // PF0,...,PF3 como salidas hacia el ULN2003 (A,A´,B,B´)
    GPIO_PORTM_DIR_R = 0x0F;//->F habilita los primeros 4 bits del puerto
    GPIO_PORTM_DEN_R = 0x0F;//(F->1111)
    GPIO_PORTM_DATA_R = 0x09;//9->1001
    // habilita al Puerto N como salida digital para control de motor
    // PN0,...,PN3 como salidas hacia el ULN2003 (A,A´,B,B´)
    GPIO_PORTN_DIR_R = 0x0F;//->F habilita los primeros 4 bits del puerto
    GPIO_PORTN_DEN_R = 0x0F;//(F->1111)
    GPIO_PORTN_DATA_R = 0x09;//9->1001
    // habilita al Puerto K como salida digital para control de motor
    // PK0,...,PK3 como salidas hacia el ULN2003 (A,A´,B,B´)
    GPIO_PORTK_DIR_R = 0x0F;//->F habilita los primeros 4 bits del puerto
    GPIO_PORTK_DEN_R = 0x0F;//(F->1111)
    GPIO_PORTK_DATA_R = 0x09;//9->1001
    //Joysticks
    GPIO_PORTE_AHB_DIR_R = 0x00;    // 2) PE4 entrada (analógica) (Puerto E)
    GPIO_PORTE_AHB_AFSEL_R |= 0x10; // 3) Habilita Función Alterna de PE4 (Puerto E)
    GPIO_PORTE_AHB_DEN_R = 0x00;    // 4) Deshabilita Función Digital de PE4 (Puerto E)
    GPIO_PORTE_AHB_AMSEL_R |= 0x10; // 5) Habilita Función Analógica de PE4 (Puerto E)

    //Botones

        GPIO_PORTD_AHB_DIR_R = 0x00;    // 2) PE4 entrada (analógica) (Puerto E)
        GPIO_PORTD_AHB_AFSEL_R |= 0x10; // 3) Habilita Función Alterna de PE4 (Puerto E)
        //GPIO_PORTD_AHB_AFSEL_R |= 0x1F; // 3) Habilita Función Alterna de PE4 (Puerto E)
        GPIO_PORTD_AHB_DEN_R = 0x00;    // 4) Deshabilita Función Digital de PE4 (Puerto E)
        GPIO_PORTD_AHB_AMSEL_R |= 0x10; // 5) Habilita Función Analógica de PE4 (Puerto E)
        //GPIO_PORTD_AHB_AMSEL_R |= 0x1F; // 5) Habilita Función Analógica de PE4 (Puerto E)

/*
    GPIO_PORTB_AHB_DIR_R = 0x00;    // 2) PE4 entrada (analógica) (Puerto E)
    GPIO_PORTB_AHB_AFSEL_R |= 0x10; // 3) Habilita Función Alterna de PE4 (Puerto E)
    //GPIO_PORTD_AHB_AFSEL_R |= 0x1F; // 3) Habilita Función Alterna de PE4 (Puerto E)
    GPIO_PORTB_AHB_DEN_R = 0x00;    // 4) Deshabilita Función Digital de PE4 (Puerto E)
    GPIO_PORTB_AHB_AMSEL_R |= 0x10; // 5) Habilita Función Analógica de PE4 (Puerto E)
*/

    //SYSCTL_RCGCADC_R  = 0x01;   // 6) Habilita reloj para lógica de ADC0
    //while((SYSCTL_PRADC_R&0x01)==0);// Se espera a que el reloj se estabilice

      SYSCTL_RCGCADC_R  = 0x03;   // 6) Habilita reloj para lógica de ADC0 y ADC1
      while((SYSCTL_PRADC_R&0x01)==0);// Se espera a que el reloj se estabilice


    //ADC 0
    ADC0_PC_R = 0x01;       // 7) Configura para 125Ksamp/s
    ADC0_SSPRI_R = 0x0123;  // 8) SS3 con la más alta prioridad
    ADC0_ACTSS_R = 0x0000;  // 9) Deshabilita SS3 (SS2, SS1, SS0) antes de cambiar configuración de registros
    ADC0_EMUX_R = 0x0000;   // 10) Se configura SS3 (SS2, SS1, SS0) para iniciar muestreo por software
    ADC0_SSEMUX3_R = 0x00;  // 11)Entradas AIN(15:0)
    ADC0_SSEMUX2_R = 0x00;  // 11)Entradas AIN(15:0)
    ADC0_SSEMUX1_R = 0x00;  // 11)Entradas AIN(15:0)
    ADC0_SSEMUX0_R = 0x00;  // 11)Entradas AIN(15:0)

    //primer Lectura
    ADC0_SSMUX3_R = (ADC0_SSMUX3_R & 0xFFFFFFF0) + 8; // canal AIN8 (PE5)
    //segunda Lectura
    ADC0_SSMUX2_R = (ADC0_SSMUX2_R & 0xFFFFFFF0) + 9; // canal AIN9 (PE4)
    //tercer Lectura
    ADC0_SSMUX1_R = (ADC0_SSMUX1_R & 0xFFFFFFF0) + 2; // canal AIN2 (PE1)
    //cuarta Lectura
    ADC0_SSMUX0_R = (ADC0_SSMUX1_R & 0xFFFFFFF0) + 1; // canal AIN2 (PE2)

    ADC0_SSCTL3_R = 0x0006; // 12) SI: AIN, Habilitación de INR3, Fin de secuencia; No:muestra diferencial
    ADC0_SSCTL2_R = 0x0006; // 12) SI: AIN, Habilitación de INR2, Fin de secuencia; No:muestra diferencial
    ADC0_SSCTL1_R = 0x0006; // 12) SI: AIN, Habilitación de INR1, Fin de secuencia; No:muestra diferencial
    ADC0_SSCTL0_R = 0x0006; // 12) SI: AIN, Habilitación de INR1, Fin de secuencia; No:muestra diferencial
    ADC0_IM_R = 0x0000;     // 13) Deshabilita interrupciones de SS0,SS1,SS2,SS3
    ADC0_ACTSS_R |= 0x000F; // 14) Habilita SS3, SS2, SS1, SS0 (p. 1077)

    //ADC 1
    ADC1_PC_R = 0x01;       // 7) Configura para 125Ksamp/s
    ADC1_SSPRI_R = 0x0123;  // 8) SS3 con la más alta prioridad
    ADC1_ACTSS_R = 0x0000;  // 9) Deshabilita SS3 (SS2, SS1, SS0) antes de cambiar configuración de registros
    ADC1_EMUX_R = 0x0000;   // 10) Se configura SS3 (SS2, SS1, SS0) para iniciar muestreo por software
    ADC1_SSEMUX3_R = 0x00;  // 11)Entradas AIN(15:0)
    ADC1_SSEMUX2_R = 0x00;  // 11)Entradas AIN(15:0)
    ADC1_SSEMUX1_R = 0x00;  // 11)Entradas AIN(15:0)
    ADC1_SSEMUX0_R = 0x00;  // 11)Entradas AIN(15:0)

    //Tabla de Pines a seleccionar (p. 1794)

        //primer Lectura
        ADC1_SSMUX3_R = (ADC1_SSMUX3_R & 0xFFFFFFF0) + 14; // canal AIN14 (PD1)
        //segunda Lectura
        ADC1_SSMUX2_R = (ADC1_SSMUX2_R & 0xFFFFFFF0) + 15; // canal AIN15 (PD0)
        //tercer Lectura
        //ADC1_SSMUX1_R = (ADC1_SSMUX1_R & 0xFFFFFFF0) + algo; // canal AIN
        //cuarta Lectura
        //ADC1_SSMUX1_R = (ADC1_SSMUX1_R & 0xFFFFFFF0) + aglo; // canal AIN


    ADC1_SSCTL3_R = 0x0006; // 12) SI: AIN, Habilitación de INR3, Fin de secuencia; No:muestra diferencial
    ADC1_SSCTL2_R = 0x0006; // 12) SI: AIN, Habilitación de INR2, Fin de secuencia; No:muestra diferencial
    ADC1_SSCTL1_R = 0x0006; // 12) SI: AIN, Habilitación de INR1, Fin de secuencia; No:muestra diferencial
    ADC1_SSCTL0_R = 0x0006; // 12) SI: AIN, Habilitación de INR1, Fin de secuencia; No:muestra diferencial
    ADC1_IM_R = 0x0000;     // 13) Deshabilita interrupciones de SS0,SS1,SS2,SS3
    ADC1_ACTSS_R |= 0x000F; // 14) Habilita SS3, SS2, SS1, SS0 (p. 1077)


    SYSCTL_PLLFREQ0_R |= SYSCTL_PLLFREQ0_PLLPWR;    // encender PLL
    while((SYSCTL_PLLSTAT_R&0x01)==0);              // espera a que el PLL fije su frecuencia
    SYSCTL_PLLFREQ0_R &= ~SYSCTL_PLLFREQ0_PLLPWR;   // apagar PLL

    ADC0_ISC_R = 0x000F;                    // Se recomienda Limpiar la bandera RIS del ADC0
    ADC1_ISC_R = 0x000F;                    // Se recomienda Limpiar la bandera RIS del ADC1
    for(;;){
      ADC0_PSSI_R = 0x000F;             // Inicia conversión de SS0, SS1 SS2 y SS3
      ADC1_PSSI_R = 0x000F;             // Inicia conversión de SS0, SS1 SS2 y SS3

      while ((ADC0_RIS_R & 0x08)==0);   // Espera a que SS3 termine conversión (polling)
      LEC0 = (ADC0_SSFIFO3_R & 0xFFF); // Resultado en FIFO3 se asigna a variable "movXR"
      LEC1 = (ADC0_SSFIFO2_R & 0xFFF); // Resultado en FIFO2 se asigna a variable "movYR"
      LEC2 = (ADC0_SSFIFO1_R & 0xFFF); // Resultado en FIFO1 se asigna a variable "movYL"
      LEC3 = (ADC0_SSFIFO0_R & 0xFFF); // Resultado en FIFO0 se asigna a variable "movYL"
      movX = (ADC1_SSFIFO3_R & 0xFFF); // Resultado en FIFO1 se asigna a variable "movYL"
      movY = (ADC1_SSFIFO2_R & 0xFFF); // Resultado en FIFO0 se asigna a variable "movYL"
      ADC0_ISC_R = 0x000F;              // Limpia la bandera RIS del ADC0
      ADC1_ISC_R = 0x000F;              // Limpia la bandera RIS del ADC0

      //movimiento Rho
            if(((LEC3)>=(LEC2))&&((LEC1)>=(LEC0))){
                accionL=1;
            }
            else{
               accionL=2;
            }

            //movimiento Fi
             if(((LEC3)>=(LEC1))&&((LEC2)>=(LEC0))){
                 accionN=1;
             }
             else{
                accionN=2;
             }

    }//FOR INFINITO

}
