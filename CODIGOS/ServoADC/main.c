//
/*Asignatura: Microcontroladores y microprocesadores.   *Semestre: 2021-2.
*Proyecto final

/******REQUERIMENTOS DE PROYECTO****
 * Entradas digitales (al menos 4 bits)
* Manejar al menos una interrupciOn
* Entradas AnalOgicas (al menos 2)
* un dispositivo que se controle por PWM (modulaciOn de ancho de pulso)
*/

/******PUERTOS UTILIZADOS******
 * // 1) HABILITA RELOJ PARA EL PUERTOS E,M,N,J,A (p. 382)
*PORTE-> Lectura de potenciometros de Joystick
*PORTM-> Servomotores (1,2,3,4)
*PORTN-> led de luz testigo en el programa
*PORTJ-> 2 botones fIsicos integrados en la tarjeta de desarrollo
*PORTA-> 2 botones fIsicos para implentar 1 en el joystick y otro externo
*/

//DESCRIPCION
/*
*El Programa utiliza al ADC para obtener el valor de la seNYal que entra a PE1,PE2
*Para obtener la lectura anlogica del joystick con Esta lectura se mueven los servomotores 1 y 2
*a travEs de una relacion por conversiOn del Angulo
El aparato tiene 4 botones los cuales funcionan como entradas simples para los 4 servomotores

Se habilitan los servicios de interrupciones para la acciOn de los botones y los retrasos por reloj

los botones del puerto A/J dan la siguiente tabla de verdad:
boton 1 puerto A/J accionado valor 1
boton 2 puerto A/J accionado valor 2
ningUn boton puerto A/J accionado valor 3

boton1 puerto J controla servo 1
boton2 puerto J controla servo 2
boton1 puerto A controla servo 3
boton2 puerto A controla servo 4

Para cada acciOn del servomotor se encienden led como luz testigo para los motores
0 -> no encienden
1 -> led 1
2 -> led 2
3 -> led 1 y 2
El codigo para asignaciOn de los led es el siguiente:
valor 1 -> servo 1 o servo 3 en modo digital (botones)
valor 2 -> servo 2 o servo 3 en modo digital (botones)
valor 3 -> servo 1 y 2 en modo analOgico

*                  ____________________________
*                 |LED_1 LED_2   PJ0    PJ1    |
*                 |              (Bo1) (Bo2)   |
*     PA0 (Bo3)-->|   Tarjeta de Desarrollo    |<--LEC0 (Joystick)
*     PA1 (Bo4)-->|                            |<--LEC1 (Joystick)
*                 |___PM0___PM1___PM2___PM3____|
*                      |     |     |     |
*                      V     V     V     V
*                     Se1   Se2   Se3   Se4
*
*   El Programa hace uso de funciones adicionales (3)
*   void SysTick_Init(void)
*   void SysTick_Wait(uint32_t retardo)
*   void mueveServo(float segundos,int angulo,int servo) recibe como argumento la duracion angulo de servo y numero de servo a controlar
*
*   _________________________         _________________________________________
*  |Paso 1                   |        |Paso 2 (Ciclo Infinito)                 |
*  | Inicia programa:        |   ->   |Leer boton 1 (Puerto_J):                |
*  |-Declaracion de Puertos  |        | accion Servo1                          |
*  |  por interrupcion       |        |Leer boton 2 (Puerto_J):                |
*  | (SysTick) fuente reloj  |        | accion Servo2                          |
*  |-ConversiOn ADC          |        |Caso bajo para Puerto J:                |
*  | Declara y configura ADC |        | Lectura Analogica LEC0-> accion Servo1 |
*  | Secuenciadores 0 y 1    |        | Lectura Analogica LEC1-> accion Servo2 |
*  |_para dos lecturas_______|        |Leer boton 3 (Puerto_A):                |
*                                     |   accion Servo3                        |
*                                     |Leer boton 4 (Puerto_A):                |
*                                     |___accion Servo4________________________|
*
*    accionServo es el uso de la funciOn mueveServo();
*
*/

// LIBRERIAS QUE UTILIZA EL PROGRAMA
#include <stdbool.h>
#include <stdint.h> // Biblioteca para aritmEtica
#include <math.h>
#include "inc/tm4c1294ncpdt.h" //Biblioteca Tiva Ware

// ***** Variables globales ***** //
uint32_t dato0,dato1,d,fin,i;//variables para botones de accion de servo y otras
uint32_t cicloTrabajo;//para seNYal PWM
uint32_t retardo;//secuencia en estado bajo PWM
uint32_t periodo=80000;//valor en ciclo de reloj de ciclo de trabajo equivalente a 20 [ms]

// DefiniciOn de constantes para operaciones
#define NVIC_ST_CTRL_COUNT      0x00010000  // bandera de cuenta flag
#define NVIC_ST_CTRL_CLK_SRC    0x00000004  // Clock Source
#define NVIC_ST_CTRL_INTEN      0x00000002  // Habilitador de interrupción
#define NVIC_ST_CTRL_ENABLE     0x00000001  // Modo del contador
#define NVIC_ST_RELOAD_M        0x00FFFFFF  // Valor de carga del contador
#define SYSCTL_RCGC2_GPION      0x00001000  // bit de estado del reloj de puerto N
#define SYSCTL_RCGC2_R          (*((volatile unsigned long *)0x400FE608)) // Registro de Habilitación de Reloj de Puertos

//PUERTO E (Fotoresistencias)
#define GPIO_PORTE_AHB_DIR_R    (*((volatile uint32_t *)0x4005C400))
#define GPIO_PORTE_AHB_AFSEL_R  (*((volatile uint32_t *)0x4005C420))
#define GPIO_PORTE_AHB_DEN_R    (*((volatile uint32_t *)0x4005C51C))
#define GPIO_PORTE_AHB_AMSEL_R  (*((volatile uint32_t *)0x4005C528))

#define SYSCTL_RCGCGPIO_R       (*((volatile uint32_t *)0x400FE608)) //Registro para habilitar reloj de GPIO
#define SYSCTL_PRGPIO_R         (*((volatile uint32_t *)0x400FEA08)) // Registro para verificar si el reloj esta listo (p.499)
#define SYSCTL_RCGCADC_R        (*((volatile uint32_t *)0x400FE638)) // Registro para habilitar el reloj al ADC(p. 396)
#define SYSCTL_PRADC_R          (*((volatile uint32_t *)0x400FEA38)) // Registro para verificar si el ADC esta listo (p.515)

//REGISTROS ADC 0
#define ADC0_PC_R               (*((volatile uint32_t *)0x40038FC4)) // Registro para configurar tasa de muestreo (p.1159)
#define ADC0_SSPRI_R            (*((volatile uint32_t *)0x40038020)) // Registro para configurar la prioridad del secuenciador (p.1099)
#define ADC0_ACTSS_R            (*((volatile uint32_t *)0x40038000)) // Registro para controlar la activación del secuenciador (p. 1076)
#define ADC0_EMUX_R             (*((volatile uint32_t *)0x40038014)) // ACDEMUX Registro para seleccionar el evento (trigger) que inicia el muestreo en cada secuenciador (p.1091)
#define ADC0_IM_R               (*((volatile uint32_t *)0x40038008)) // Registro que controla la mascara de interrupciones en secuenciadores (p. 1081)
#define ADC0_ACTSS_R            (*((volatile uint32_t *)0x40038000)) // Registro que controla la activación de los secuenciadores (p.1077)
#define ADC0_ISC_R              (*((volatile uint32_t *)0x4003800C)) //Registro de estatus y para borrar las condiciones de interrupción del secuenciador (p. 1084)
#define ADC0_PSSI_R             (*((volatile uint32_t *)0x40038028)) //Registro que permite al software iniciar el muestreo en los secuenciadores (p. 1102)
#define ADC0_RIS_R              (*((volatile uint32_t *)0x40038004)) //Registro muestra el estado de la señal de interrupción de cada secuenciador (p.1079  )

//ADC 0 secuenciador 1
#define ADC0_SSEMUX1_R          (*((volatile uint32_t *)0x40038078)) // Registro que selecciona entre las entradas AIN[19:16] o AIN[15:0]  (p.37)
#define ADC0_SSMUX1_R           (*((volatile uint32_t *)0x40038060)) // Registro para configurar la entrada analógica para el Secuenciador 1 (p.1074)
#define ADC0_SSCTL1_R           (*((volatile uint32_t *)0x40038064)) // Registro que configura la muestra ejecutada con el Secuenciador 1 (p.1074)
#define ADC0_SSFIFO1_R          (*((volatile uint32_t *)0x40038068)) //Registro que contiene los resultados de conversión de las muestras recogidas con el secuenciador (p. 1074)
//ADC 0 secuenciador 0
#define ADC0_SSEMUX0_R          (*((volatile uint32_t *)0x40038058)) // Registro que selecciona entre las entradas AIN[19:16] o AIN[15:0]  (p.37)
#define ADC0_SSMUX0_R           (*((volatile uint32_t *)0x40038040)) // Registro para configurar la entrada analógica para el Secuenciador 0 (p.37)
#define ADC0_SSCTL0_R           (*((volatile uint32_t *)0x40038044)) // Registro que configura la muestra ejecutada con el Secuenciador 0 (p.37)
#define ADC0_SSFIFO0_R          (*((volatile uint32_t *)0x40038048)) //Registro que contiene los resultados de conversión de las muestras recogidas con el secuenciador (p. 37)

//Registro de conversiOn Analogica Digital
volatile uint32_t LEC0;
volatile uint32_t LEC1;

//Conversiones de Angulo para servo
volatile uint32_t convAngulo0;
volatile uint32_t convAngulo1;

//PLL
#define SYSCTL_PLLFREQ0_R       (*((volatile uint32_t *)0x400FE160)) //Registro para configurar el PLL
#define SYSCTL_PLLSTAT_R        (*((volatile uint32_t *)0x400FE168)) //Registro muestra el estado de encendido del PLL
#define SYSCTL_PLLFREQ0_PLLPWR  0x00800000  // Valor para encender el PLL

//**********FUNCION Inizializa el SysTick *********************
void SysTick_Init(void){
  NVIC_ST_CTRL_R = 0;                   // Desahabilita el SysTick durante la configuraciOn
  NVIC_ST_RELOAD_R = NVIC_ST_RELOAD_M;  // Se establece el valor de cuenta deseado en RELOAD_R
  NVIC_ST_CURRENT_R = 0;                // Se escribe al registro current para limpiarlo

  NVIC_ST_CTRL_R = 0x00000001;         // Se Habilita el SysTick y se selecciona la fuente de reloj
}

//*************FUNCION Tiempo de retardo utilizando wait.***************
// El parametro de retardo esta en unidades del reloj interno/4 = 4 MHz (250 ns)
void SysTick_Wait(uint32_t retardo){
    NVIC_ST_RELOAD_R= retardo-1;   //nUmero de cuentas por esperar
    NVIC_ST_CURRENT_R = 0;
    while((NVIC_ST_CTRL_R&0x00010000)==0){//espera hasta que la bandera COUNT sea valida
    }
   } //

//los ciclos de codificacion para el servo SG90
    //estAn dados por 20 [mS]
    //en el codigo 4000000 en el valor del SysTick es 1[s]
    //por lo que 80000 equivale a 20 [ms]

    //cada ciclo de servo consume 20 [mS]
    //esto es importante tomar en consideracion
    //para el tiempo de respuesta del mismo
    //50 ciclos de for equivale a 1 segundo
//La rutina siguiente hace una seNYal PWM con ciclo de 20 [ms]
//con ciclo de trabajo de MAX [2ms] o 10% que es lo
//especificado por el fabricante para la codificaciOn
//de 0 a 180 grados

void mueveServo(float segundos,int angulo,int servo){//recibe como argumento la duracion angulo de servo y numero de servo a controlar

    cicloTrabajo=45*angulo+4000;//conversion de angulo a variable de programa
    //Tomado de la ecuacion y=45*x+4000
    //resultante de la relacion de variable de programa con Angulo
    retardo=periodo-cicloTrabajo;//resta para compensaciOn del ciclo
    fin=50*segundos;//termino del fin para ciclo for
    //tomado de la ecuacion y=(50)X
    //resultante de la relacion de segundos con ciclo for de programa

    for(d=0;d<fin;d++){//repeticiOn del ciclo depende del valor en segundos ingresado
                 if(servo==1){
                    GPIO_PORTM_DATA_R |= 0x01;//dato para activar servo 1
                 }
                 else if(servo==2){
                    GPIO_PORTM_DATA_R |= 0x02;//dato para activar servo 2
                 }else if(servo==3){
                     GPIO_PORTM_DATA_R |= 0x04;//dato para activar servo 3
                 }else if(servo==4){
                     GPIO_PORTM_DATA_R |= 0x08;//dato para activar servo 4
                 }
                 SysTick_Wait(cicloTrabajo);//Alto (cOdigo de angulo)
                 GPIO_PORTM_DATA_R = 0x00;//dato para mandar bajo todos los bits del puerto
                 SysTick_Wait(retardo);//Bajo por el resto del ciclo para que cumpla 20[ms]
    }
}
//tiempo mInimo recomendado para mover servo 0.4[s]

//*** PROGRAMA PRINCIPAL ****

void main(){

    // ***** PrgoramaciOn de puertos ***** //

    SYSCTL_RCGCGPIO_R |= 0X1911; // 1) HABILITA RELOJ PARA EL PUERTOS E,M,N,J,A (p. 382)


      GPIO_PORTN_DIR_R = 0X03; // Se configura el pin 0 y 1 del puerto N como salida
      GPIO_PORTN_DEN_R = 0X03; // Se configura el pin 0 y 1 del puerto N como digitales

       GPIO_PORTA_AHB_DIR_R = 0; // Se configura el pin 0 y 1 del puerto A como entradas
       GPIO_PORTA_AHB_DEN_R = 0x03; // Se configura el pin 0 y 1 del puerto A como digitales
       GPIO_PORTA_AHB_PUR_R = 0x03; // Resistencia en modo Pull-Up

       GPIO_PORTJ_AHB_DIR_R = 0; // Se configura el pin 0 y 1 del puerto J como entradas
       GPIO_PORTJ_AHB_DEN_R = 0x03; // Se configura el pin 0 y 1 del puerto J como digitales
       GPIO_PORTJ_AHB_PUR_R = 0x03; // Resistencia en modo Pull-Up

    SysTick_Init();//inicializa servicio de retardos

        //Inicia programa y lecturas de ADC

    //SYSCTL_RCGCGPIO_R |= 0X7FFF; // 1) HABILITA RELOJ PARA Todos los puertos (p. 382)
        while ((SYSCTL_PRGPIO_R & 0X1000) == 0){};  // reloj listo?
            // habilita al Puerto M como salida digital por PWM para control de servos
            // PM0,...,PM3 como salidas hacia los servomotores

        GPIO_PORTM_DIR_R |= 0x0F;    // puerto M de salida
        GPIO_PORTM_DEN_R |= 0x0F;    // habilita el puerto M

            //Lecturas AnalOgicas
              GPIO_PORTE_AHB_DIR_R = 0x00;    // 2) PE4 entrada (analOgica) (Puerto E)
              GPIO_PORTE_AHB_AFSEL_R |= 0x10; // 3) Habilita Función Alterna de (Puerto E)
              GPIO_PORTE_AHB_DEN_R = 0x00;    // 4) Deshabilita FunciOn Digital de (Puerto E)
              GPIO_PORTE_AHB_AMSEL_R |= 0x10; // 5) Habilita Función Analógica de (Puerto E)

              SYSCTL_RCGCADC_R  = 0x01;   // 6) Habilita reloj para lOgica de ADC0
              while((SYSCTL_PRADC_R&0x01)==0);// Se espera a que el reloj se estabilice

            //ADC 0
            ADC0_PC_R = 0x01;       // 7) Configura para 125Ksamp/s
            ADC0_SSPRI_R = 0x0123;  // 8) SS3 con la mAs alta prioridad
            ADC0_ACTSS_R = 0x0000;  // 9) Deshabilita  (SS1, SS0) antes de cambiar configuraciOn de registros
            ADC0_EMUX_R = 0x0000;   // 10) Se configura (SS1, SS0) para iniciar muestreo por software
            ADC0_SSEMUX1_R = 0x00;  // 11)Entradas AIN(15:0)
            ADC0_SSEMUX0_R = 0x00;  // 11)Entradas AIN(15:0)

            ADC0_SSMUX1_R = (ADC0_SSMUX1_R & 0xFFFFFFF0) + 2; // canal AIN2 (PE1)
            ADC0_SSMUX0_R = (ADC0_SSMUX0_R & 0xFFFFFFF0) + 1; // canal AIN1 (PE2)

            ADC0_SSCTL3_R = 0x0006; // 12) SI: AIN, Habilitación de INR3, Fin de secuencia; No:muestra diferencial
            ADC0_SSCTL2_R = 0x0006; // 12) SI: AIN, Habilitación de INR2, Fin de secuencia; No:muestra diferencial
            ADC0_SSCTL1_R = 0x0006; // 12) SI: AIN, Habilitación de INR1, Fin de secuencia; No:muestra diferencial
            ADC0_SSCTL0_R = 0x0006; // 12) SI: AIN, Habilitación de INR1, Fin de secuencia; No:muestra diferencial
            ADC0_IM_R = 0x0000;     // 13) Deshabilita interrupciones de SS0,SS1
            ADC0_ACTSS_R |= 0x000F; // 14) Habilita SS3, SS2 (p. 1077)

            SYSCTL_PLLFREQ0_R |= SYSCTL_PLLFREQ0_PLLPWR;    // encender PLL
            while((SYSCTL_PLLSTAT_R&0x01)==0);              // espera a que el PLL fije su frecuencia
            SYSCTL_PLLFREQ0_R &= ~SYSCTL_PLLFREQ0_PLLPWR;   // apagar PLL

            ADC0_ISC_R = 0x000F;                    // Se recomienda Limpiar la bandera RIS del ADC0
            for(;;){
              ADC0_PSSI_R = 0x000F;             // Inicia conversión de SS0, SS1
               while ((ADC0_RIS_R & 0x06)==0);   // Espera a que secuenciadores termine conversión (polling)
              LEC1 = (ADC0_SSFIFO1_R & 0xFFF); // Resultado en FIFO1 se asigna a variable "LEC1"
              LEC0 = (ADC0_SSFIFO0_R & 0xFFF); // Resultado en FIFO0 se asigna a variable "LEC0"
              ADC0_ISC_R = 0x000F;              // Limpia la bandera RIS del ADC0
              //fin de lecturas analOgicas

              //Joystick
              //LEC 0 movimineto horizontal
              //LEC 1 movimiento vertical
              //RANGO de valores para posicion es
              //3030-3050 punto medio
              //4095 extremo superior
              //20 extremo inferior

              float refresca=0.15;

              dato0=GPIO_PORTJ_AHB_DATA_R;//lectura de botones en puerto J
              if(dato0==1){//boton 1 puerto J
                  mueveServo(refresca,0,1);//movimiento servo1
                  GPIO_PORTN_DATA_R |= 0x01;
              }
              else if(dato0==2){//boton 2 puerto J

                  mueveServo(refresca,0,2);//movimiento servo2
                  GPIO_PORTN_DATA_R |= 0x02;
              }else if(dato0==3){//caso ddOnde ningun boton es presionado

                  //la conversion del angulo viene dada por la proporcion a travEs de la ecuacion
                  //y=(x/30)
                  //para convertir la lectura analogica en valores tipo de Angulo

                  convAngulo0=LEC0/30;//conversion para el servo 1
                  mueveServo(refresca,convAngulo0,1);//movimiento servo1

                  convAngulo1=LEC1/30;//conversion para el servo 2
                  mueveServo(refresca,convAngulo1,2);//movimiento servo2
                  GPIO_PORTN_DATA_R |= 0x03;
              }else{//caso por defecto
                  mueveServo(refresca,135,1);//regreso a posicion inicial a 135 grados servo 1
                  mueveServo(refresca,135,2);//regreso a posicion inicial a 135 grados servo 2
                  GPIO_PORTN_DATA_R = 0x00;
              }

              dato1=GPIO_PORTA_AHB_DATA_R;//lectura de botones en puerto A
                  if(dato1==1){//boton 1 puerto A
                  mueveServo(refresca,0,3);//movimiento servo3
                  GPIO_PORTN_DATA_R |= 0x01;
               }
               else if(dato1==2){//boton 2 puerto A
                   mueveServo(refresca,0,4);//movimiento servo4
                   GPIO_PORTN_DATA_R |= 0x02;
               }else{//caso por defecto
                   mueveServo(refresca,135,3);//regreso a posicion inicial a 135 grados servo 3
                   mueveServo(refresca,135,4);//regreso a posicion inicial a 135 grados servo 4
                   GPIO_PORTN_DATA_R = 0x00;
               }

            }//fin FOR INFINITO

}//fin main
