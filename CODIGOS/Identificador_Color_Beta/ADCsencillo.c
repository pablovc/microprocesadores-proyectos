//
/*Asignatura: Microcontroladores y microprocesadores.   *Semestre: 2021-1.
*Proyecto final
*Temas:
*->Convertidor AnalOgico Digital
*->Motor a pasos unipolar.
*->Motor DC con Hbridge driver
*Presenta: Jesús
-------------------------------------------------------------------------------
*/

/******PUERTOS UTILIZADOS******
*PORTE-> CNY70 como sensor de luz
*PORTK-> Motor a Pasos
*PORTL-> Motor a Pasos
*PORTN-> Motor a Pasos
*PORTB-> LCD
*/

//DESCRIPCION
/*
*El Programa utiliza al DAC para obtener el valor de la señal que entra a PE4
*Para obtener la lectura anlogica del sensor de luz (colores y deteccion de objetos)


*
*/

// LIBRERIAS QUE UTILIZA EL PROGRAMA
#include <stdbool.h>
#include <stdint.h> // Biblioteca para aritmEtica
#include <math.h>
#include "inc/tm4c1294ncpdt.h" //Biblioteca Tiva Ware

#include "I2C_LCD.h"//biblioteca comunicaciOn I2C para LCD

//REGISTROS DE RELOJ PARA MANEJO DE LCD I2C
#define SYSCTL_RCGCGPIO_R       (*((volatile uint32_t *)0x400FE608)) //Reloj del puerto
#define SYSCTL_RCGCI2C_R        (*((volatile uint32_t *)0x400FE620)) //Reloj de I2C
#define SYSCTL_PRGPIO_R        (*((volatile uint32_t *)0x400FEA08)) //Bandera de "Peripherial Ready"

//REGISTROS DEL PUERTO B
#define GPIO_PORTB_DATA_R   (*((volatile uint32_t *)0x400593FC)) //Para los datos del puerto
#define GPIO_PORTB_DIR_R    (*((volatile uint32_t *)0x40059400)) //Para seleccionar funciOn
#define GPIO_PORTB_AFSEL_R  (*((volatile uint32_t *)0x40059420)) //Para seleccionar funciOn alterna
#define GPIO_PORTB_ODR_R    (*((volatile uint32_t *)0x4005950C)) //Para activar el Open Drain
#define GPIO_PORTB_DEN_R    (*((volatile uint32_t *)0x4005951C)) //Para activar función digital
#define GPIO_PORTB_PCTL_R   (*((volatile uint32_t *)0x4005952C)) //Para el control del puerto

//REGISTROS DEL MOUDLO I2C
#define I2C0_MSA_R              (*((volatile uint32_t *)0x40020000)) //I2C Master Slave Adress
#define I2C0_MCS_R              (*((volatile uint32_t *)0x40020004)) //I2C Master Control Status
#define I2C0_MDR_R              (*((volatile uint32_t *)0x40020008)) //I2C Master Data Register
#define I2C0_MTPR_R             (*((volatile uint32_t *)0x4002000C)) //I2C Master Time Period
#define I2C0_MCR_R              (*((volatile uint32_t *)0x40020020)) //I2C Master Configuration Register

//PUERTO E (Fotoresistencias)
#define GPIO_PORTE_AHB_DIR_R    (*((volatile uint32_t *)0x4005C400))
#define GPIO_PORTE_AHB_AFSEL_R  (*((volatile uint32_t *)0x4005C420))
#define GPIO_PORTE_AHB_DEN_R    (*((volatile uint32_t *)0x4005C51C))
#define GPIO_PORTE_AHB_AMSEL_R  (*((volatile uint32_t *)0x4005C528))

#define SYSCTL_RCGCGPIO_R       (*((volatile uint32_t *)0x400FE608)) //Registro para habilitar reloj de GPIO
#define SYSCTL_PRGPIO_R         (*((volatile uint32_t *)0x400FEA08)) // Registro para verificar si el reloj esta listo (p.499)
#define SYSCTL_RCGCADC_R        (*((volatile uint32_t *)0x400FE638)) // Registro para habilitar el reloj al ADC(p. 396)
#define SYSCTL_PRADC_R          (*((volatile uint32_t *)0x400FEA38)) // Registro para verificar si el ADC esta listo (p.515)

//REGISTROS ADC 0
#define ADC0_PC_R               (*((volatile uint32_t *)0x40038FC4)) // Registro para configurar tasa de muestreo (p.1159)
#define ADC0_SSPRI_R            (*((volatile uint32_t *)0x40038020)) // Registro para configurar la prioridad del secuenciador (p.1099)
#define ADC0_ACTSS_R            (*((volatile uint32_t *)0x40038000)) // Registro para controlar la activación del secuenciador (p. 1076)
#define ADC0_EMUX_R             (*((volatile uint32_t *)0x40038014)) // ACDEMUX Registro para seleccionar el evento (trigger) que inicia el muestreo en cada secuenciador (p.1091)
#define ADC0_IM_R               (*((volatile uint32_t *)0x40038008)) // Registro que controla la mascara de interrupciones en secuenciadores (p. 1081)
#define ADC0_ACTSS_R            (*((volatile uint32_t *)0x40038000)) // Registro que controla la activación de los secuenciadores (p.1077)
#define ADC0_ISC_R              (*((volatile uint32_t *)0x4003800C)) //Registro de estatus y para borrar las condiciones de interrupción del secuenciador (p. 1084)
#define ADC0_PSSI_R             (*((volatile uint32_t *)0x40038028)) //Registro que permite al software iniciar el muestreo en los secuenciadores (p. 1102)
#define ADC0_RIS_R              (*((volatile uint32_t *)0x40038004)) //Registro muestra el estado de la señal de interrupción de cada secuenciador (p.1079  )

//ADC 0 secuenciador 2
#define ADC0_SSEMUX2_R          (*((volatile uint32_t *)0x40038098)) // Registro que selecciona entre las entradas AIN[19:16] o AIN[15:0]  (p.37,1137)
#define ADC0_SSMUX2_R           (*((volatile uint32_t *)0x40038080)) // Registro para configurar la entrada analógica para el Secuenciador 2 (p.1041)
#define ADC0_SSCTL2_R           (*((volatile uint32_t *)0x40038084)) // Registro que configura la muestra ejecutada con el Secuenciador 2 (p.37)
#define ADC0_SSFIFO2_R          (*((volatile uint32_t *)0x40038088)) //Registro que contiene los resultados de conversión de las muestras recogidas con el secuenciador (p. 37)

//Variable de conversiOn Analogica Digital para detecciOn de COLOR
volatile uint32_t LECTURA;

//PLL
#define SYSCTL_PLLFREQ0_R       (*((volatile uint32_t *)0x400FE160)) //Registro para configurar el PLL
#define SYSCTL_PLLSTAT_R        (*((volatile uint32_t *)0x400FE168)) //Registro muestra el estado de encendido del PLL
#define SYSCTL_PLLFREQ0_PLLPWR  0x00800000  // Valor para encender el PLL






// ******* FUNCIONES DE PROGRAMA ********
void clearScreen(){
    //Borra la pantalla
            LCD_Set_Cursor(1, 1);//cursor a coordenada
            LCD_Write_String("                ");//16 espacios en blanco
            LCD_Set_Cursor(2, 1);//cursor a coordenada
            LCD_Write_String("                ");//16 espacios en blanco
}


//*** PROGRAMA PRINCIPAL ****

void main(){

    I2C_Init(); //FunciOn que inicializa los relojes, el GPIO y el I2C0

    //Inicializo Slave
    while(I2C0_MCS_R&0x00000001){}; // espera que el I2C estE listo
    LCD_Init();//inicializa pantalla LCD 16x2 con comunicaciOn I2C

    SysTick_Init();//inicia servicio de retraso por frecuencia

    LCD_Set_Cursor(1, 1);
    LCD_Write_String("Clasificacion de");
    LCD_Set_Cursor(2, 1);
    LCD_Write_String("----Colores----");

        SysTick_Wait(4000000);//espera 1S

        clearScreen();//funcion borra pantalla

        LCD_Set_Cursor(1, 1);
        LCD_Write_String("Fase1: Motores");
        LCD_Set_Cursor(2, 1);
        LCD_Write_String("En Movimiento");
        //Inicia programa timer y lecturas de ADC
        SYSCTL_RCGCGPIO_R |= 0X1E12; // 1) HABILITA RELOJ PARA EL PUERTOS B,E,M,K,L,N (p. 382)
            //MOTORES A PASOS
            SYSCTL_RCGCTIMER_R |= 0X08; //RELOJ Y HABILITA TIMER 3 (p.380)
            while ((SYSCTL_PRGPIO_R & 0X1E12) == 0){};  // reloj listo?
            TIMER3_CTL_R=0X00000000; //DESHABILITA TIMER 3 PARA CONFIGURAR (p.986)
            TIMER3_CFG_R= 0X00000000; //CONFIGURA TIMER DE 32 BITS (p. 976)
            TIMER3_TAILR_R= 0X00007530; // VALOR DE RECARGA (p.1004)
            TIMER3_TAPR_R= 0X00; // PRESCALADOR DE TIMER A, SOLO PARA MODOS DE 16 BITS (p.1008)
            TIMER3_ICR_R= 0X00000001 ; //LIMPIA POSIBLE BANDERA PENDIENTE DE TIMER3 (p.1002)
            TIMER3_IMR_R |= 0X00000001; //ACTIVA INTRRUPCION DE TIMEOUT (p.993)
            NVIC_EN1_R= 1<<(35-32); //HABILITA LA INTERRUPCION 35 (TIMER3 A)
            TIMER3_CTL_R |= 0X00000001; //HABILITA TIMER 3 (p.986)


            // habilita al Puerto N como salida digital para control de motor
             // PN0,...,PN3 como salidas hacia el ULN2003 (A,A´,B,B´)
             GPIO_PORTN_DIR_R = 0x0F;//->F habilita los primeros 4 bits del puerto
             GPIO_PORTN_DEN_R = 0x0F;//(F->1111)
             GPIO_PORTN_DATA_R = 0x09;//9->1001

            //Lecturas AnalOgicas
              GPIO_PORTE_AHB_DIR_R = 0x00;    // 2) PE4 entrada (analOgica) (Puerto E)
              GPIO_PORTE_AHB_AFSEL_R |= 0x10; // 3) Habilita FunciOn Alterna de PE4 (Puerto E)
              GPIO_PORTE_AHB_DEN_R = 0x00;    // 4) Deshabilita FunciOn Digital de PE4 (Puerto E)
              GPIO_PORTE_AHB_AMSEL_R |= 0x10; // 5) Habilita Función Analógica de PE4 (Puerto E)

              SYSCTL_RCGCADC_R  = 0x01;   // 6) Habilita reloj para lOgica de ADC0
              while((SYSCTL_PRADC_R&0x01)==0);// Se espera a que el reloj se estabilice

            //ADC 0
            ADC0_PC_R = 0x03;        // 7) Configura frecuencia de muestreo
            ADC0_SSPRI_R = 0x0123;  // 8) SS3 con la mAs alta prioridad
            ADC0_ACTSS_R = 0x0000;  // 9) Deshabilita SS3 (SS2, SS1, SS0) antes de cambiar configuraciOn de registros
            ADC0_EMUX_R = 0x0000;   // 10) Se configura SS3 (SS2, SS1, SS0) para iniciar muestreo por software
            ADC0_SSEMUX2_R = 0x00;  // 11)Entradas AIN(15:0)
            ADC0_SSMUX2_R = (ADC0_SSMUX2_R & 0xFFFFFFF0) + 9; // canal AIN9 (PE4)
            ADC0_SSCTL2_R = 0x0006; // 12) SI: AIN, HabilitaciOn de INR2, Fin de secuencia; No:muestra diferencial
            ADC0_IM_R = 0x0000;     // 13) Deshabilita interrupciones de SS0,SS1,SS2,SS3
            ADC0_ACTSS_R |= 0x000F; // 14) Habilita SS3, SS2, SS1, SS0 (p. 1077)

            SYSCTL_PLLFREQ0_R |= SYSCTL_PLLFREQ0_PLLPWR;    // encender PLL
            while((SYSCTL_PLLSTAT_R&0x01)==0);              // espera a que el PLL fije su frecuencia
            SYSCTL_PLLFREQ0_R &= ~SYSCTL_PLLFREQ0_PLLPWR;   // apagar PLL

            ADC0_ISC_R = 0x000F;                    // Se recomienda Limpiar la bandera RIS del ADC0

            SysTick_Init();//inicio de servicio de espera de tiempo por frecuencia

            for(;;){



                 ADC0_PSSI_R = 0x000F;             // Inicia conversión de SS0, SS1 SS2 y SS3
                  while ((ADC0_RIS_R & 0x06)==0);   // Espera a que secuenciadores termine conversión (polling)
                 LECTURA = (ADC0_SSFIFO2_R & 0xFFF); // Resultado en FIFO2 se asigna a variable "COLOR"
                 ADC0_ISC_R = 0x000F;              // Limpia la bandera RIS del ADC0

                  //SysTick_Wait(2000000);//espera 0.5 [s]



                //SysTick_Wait(4000000);//espera 1 [s]

            }//FIN FOR INFINITO

}
