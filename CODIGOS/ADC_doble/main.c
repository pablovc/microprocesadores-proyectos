//5Motores
/*Asignatura: Microcontroladores y microprocesadores.   *Semestre: 2019-2.
*Proyecto final
*Temas:
*->Convertidor AnalOgico Digital
*->Motor a pasos unipolar.
*Presenta: Pablo Vivar Colina.
-------------------------------------------------------------------------------
*/

/***** PUERTOS UTILIZADOS *****
*PORTE-> Lectura Joysticks (ADC)
*PORTB-> Lectura 4 ADC adicionales
*PORTM-> Motor a paso
*PORTL-> Motor a paso
*PORTN-> Motor a paso
*PORTK-> Motor a paso
*/

//DESCRIPCION
/*
*El Programa utiliza al DAC para obtener el valor de la señal que entra a PE4,PE5,PE1,PE2
* Las lecturas de los puertos anteriores activan los puertos M,L,N y K
* para activar motores a pasos en grua cartesiana
*El disparo del DAC es por software
*/

// LIBRERIAS QUE UTILIZA EL PROGRAMA
#include <stdbool.h>
#include <stdint.h> // Biblioteca para aritmEtica
#include <math.h>
#include "inc/tm4c1294ncpdt.h" //Biblioteca Tiva Ware

//PUERTO E (Joysticks)
#define GPIO_PORTE_AHB_DIR_R    (*((volatile uint32_t *)0x4005C400))
#define GPIO_PORTE_AHB_AFSEL_R  (*((volatile uint32_t *)0x4005C420))
#define GPIO_PORTE_AHB_DEN_R    (*((volatile uint32_t *)0x4005C51C))
#define GPIO_PORTE_AHB_AMSEL_R  (*((volatile uint32_t *)0x4005C528))


//PUERTO B (Botones)
/*
#define GPIO_PORTB_AHB_DIR_R    (*((volatile uint32_t *)0x40059400))
#define GPIO_PORTB_AHB_AFSEL_R  (*((volatile uint32_t *)0x40059420))
#define GPIO_PORTB_AHB_DEN_R    (*((volatile uint32_t *)0x4005951C))
#define GPIO_PORTB_AHB_AMSEL_R  (*((volatile uint32_t *)0x40059528))
*/

//PUERTO D
#define GPIO_PORTD_AHB_DIR_R    (*((volatile uint32_t *)0x4005B400))
#define GPIO_PORTD_AHB_AFSEL_R  (*((volatile uint32_t *)0x4005B420))
#define GPIO_PORTD_AHB_DEN_R    (*((volatile uint32_t *)0x4005B51C))
#define GPIO_PORTD_AHB_AMSEL_R  (*((volatile uint32_t *)0x4005B528))

#define SYSCTL_RCGCGPIO_R       (*((volatile uint32_t *)0x400FE608)) //Registro para habilitar reloj de GPIO
#define SYSCTL_PRGPIO_R         (*((volatile uint32_t *)0x400FEA08)) // Registro para verificar si el reloj esta listo (p.499)
#define SYSCTL_RCGCADC_R        (*((volatile uint32_t *)0x400FE638)) // Registro para habilitar el reloj al ADC(p. 396)
#define SYSCTL_PRADC_R          (*((volatile uint32_t *)0x400FEA38)) // Registro para verificar si el ADC esta listo (p.515)
//REGISTROS ADC 0
#define ADC0_PC_R               (*((volatile uint32_t *)0x40038FC4)) // Registro para configurar tasa de muestreo (p.1159)
#define ADC0_SSPRI_R            (*((volatile uint32_t *)0x40038020)) // Registro para configurar la prioridad del secuenciador (p.1099)
#define ADC0_ACTSS_R            (*((volatile uint32_t *)0x40038000)) // Registro para controlar la activación del secuenciador (p. 1076)
#define ADC0_EMUX_R             (*((volatile uint32_t *)0x40038014)) // ACDEMUX Registro para seleccionar el evento (trigger) que inicia el muestreo en cada secuenciador (p.1091)
#define ADC0_IM_R               (*((volatile uint32_t *)0x40038008)) // Registro que controla la mascara de interrupciones en secuenciadores (p. 1081)
#define ADC0_ACTSS_R            (*((volatile uint32_t *)0x40038000)) // Registro que controla la activación de los secuenciadores (p.1077)
#define ADC0_ISC_R              (*((volatile uint32_t *)0x4003800C)) //Registro de estatus y para borrar las condiciones de interrupción del secuenciador (p. 1084)
#define ADC0_PSSI_R             (*((volatile uint32_t *)0x40038028)) //Registro que permite al software iniciar el muestreo en los secuenciadores (p. 1102)
#define ADC0_RIS_R              (*((volatile uint32_t *)0x40038004)) //Registro muestra el estado de la señal de interrupción de cada secuenciador (p.1079  )


//Registros ADC 1
#define ADC1_PC_R               (*((volatile uint32_t *)0x40039FC4)) // Registro para configurar tasa de muestreo (p.1159)
#define ADC1_SSPRI_R            (*((volatile uint32_t *)0x40039020)) // Regidtrp para configurar la prioridad del secuenciador (p.1099)
#define ADC1_ACTSS_R            (*((volatile uint32_t *)0x40039000)) // Registro para controlar la activación del secuenciador (p. 1076)
#define ADC1_EMUX_R             (*((volatile uint32_t *)0x40039014)) // ACDEMUX Registro para seleccionar el evento (trigger) que inicia el muestreo en cada secuenciador (p.1091)
#define ADC1_IM_R               (*((volatile uint32_t *)0x40039008)) // Registro que controla la mascara de interrupciones en secuenciadores (p. 1081)
#define ADC1_ACTSS_R            (*((volatile uint32_t *)0x40039000)) // Registro que controla la activación de los secuenciadores (p.1077)
#define ADC1_ISC_R              (*((volatile uint32_t *)0x4003900C)) //Registro de estatus y para borrar las condiciones de interrupción del secuenciador (p. 1084)
#define ADC1_PSSI_R             (*((volatile uint32_t *)0x40039028)) //Registro que permite al software iniciar el muestreo en los secuenciadores (p. 1102)
#define ADC1_RIS_R              (*((volatile uint32_t *)0x40039004)) //Registro muestra el estado de la señal de interrupción de cada secuenciador (p.1079  )

//ADC 0 input 1
#define ADC0_SSEMUX3_R          (*((volatile uint32_t *)0x400380B8)) // Registro que selecciona entre las entradas AIN[19:16] o AIN[15:0]  (p.1046)
#define ADC0_SSMUX3_R           (*((volatile uint32_t *)0x400380A0)) // Registro para configurar la entrada analógica para el Secuenciador 3 (p.1041)
#define ADC0_SSCTL3_R           (*((volatile uint32_t *)0x400380A4)) // Registro que configura la muestra ejecutada con el Secuenciador 3 (p.1142)
#define ADC0_SSFIFO3_R          (*((volatile uint32_t *)0x400380A8)) //Registro que contiene los resultados de conversión de las muestras recogidas con el secuenciador (p. 1118)
//ADC 0 input2
#define ADC0_SSEMUX2_R          (*((volatile uint32_t *)0x40038098)) // Registro que selecciona entre las entradas AIN[19:16] o AIN[15:0]  (p.37,1137)
#define ADC0_SSMUX2_R           (*((volatile uint32_t *)0x40038080)) // Registro para configurar la entrada analógica para el Secuenciador 2 (p.1041)
#define ADC0_SSCTL2_R           (*((volatile uint32_t *)0x40038084)) // Registro que configura la muestra ejecutada con el Secuenciador 2 (p.37)
#define ADC0_SSFIFO2_R          (*((volatile uint32_t *)0x40038088)) //Registro que contiene los resultados de conversión de las muestras recogidas con el secuenciador (p. 37)
//ADC 0 input3
#define ADC0_SSEMUX1_R          (*((volatile uint32_t *)0x40038078)) // Registro que selecciona entre las entradas AIN[19:16] o AIN[15:0]  (p.37)
#define ADC0_SSMUX1_R           (*((volatile uint32_t *)0x40038060)) // Registro para configurar la entrada analógica para el Secuenciador 1 (p.1074)
#define ADC0_SSCTL1_R           (*((volatile uint32_t *)0x40038064)) // Registro que configura la muestra ejecutada con el Secuenciador 1 (p.1074)
#define ADC0_SSFIFO1_R          (*((volatile uint32_t *)0x40038068)) //Registro que contiene los resultados de conversión de las muestras recogidas con el secuenciador (p. 1074)
//ADC 0 input4
#define ADC0_SSEMUX0_R          (*((volatile uint32_t *)0x40038058)) // Registro que selecciona entre las entradas AIN[19:16] o AIN[15:0]  (p.37)
#define ADC0_SSMUX0_R           (*((volatile uint32_t *)0x40038040)) // Registro para configurar la entrada analógica para el Secuenciador 0 (p.37)
#define ADC0_SSCTL0_R           (*((volatile uint32_t *)0x40038044)) // Registro que configura la muestra ejecutada con el Secuenciador 0 (p.37)
#define ADC0_SSFIFO0_R          (*((volatile uint32_t *)0x40038048)) //Registro que contiene los resultados de conversión de las muestras recogidas con el secuenciador (p. 37)


//ADC 1 input 1
#define ADC1_SSEMUX3_R          (*((volatile uint32_t *)0x400390B8)) // Registro que selecciona entre las entradas AIN[19:16] o AIN[15:0]  (p.1046)
#define ADC1_SSMUX3_R           (*((volatile uint32_t *)0x400390A0)) // Registro para configurar la entrada analógica para el Secuenciador 3 (p.1041)
#define ADC1_SSCTL3_R           (*((volatile uint32_t *)0x400390A4)) // Registro que configura la muestra ejecutada con el Secuenciador 3 (p.1142)
#define ADC1_SSFIFO3_R          (*((volatile uint32_t *)0x400390A8)) //Registro que contiene los resultados de conversión de las muestras recogidas con el secuenciador (p. 1118)
//ADC 1 input2
#define ADC1_SSEMUX2_R          (*((volatile uint32_t *)0x40039098)) // Registro que selecciona entre las entradas AIN[19:16] o AIN[15:0]  (p.37,1137)
#define ADC1_SSMUX2_R           (*((volatile uint32_t *)0x40039080)) // Registro para configurar la entrada analógica para el Secuenciador 2 (p.1041)
#define ADC1_SSCTL2_R           (*((volatile uint32_t *)0x40039084)) // Registro que configura la muestra ejecutada con el Secuenciador 2 (p.37)
#define ADC1_SSFIFO2_R          (*((volatile uint32_t *)0x40039088)) //Registro que contiene los resultados de conversión de las muestras recogidas con el secuenciador (p. 37)
//ADC 1 input3
#define ADC1_SSEMUX1_R          (*((volatile uint32_t *)0x40039078)) // Registro que selecciona entre las entradas AIN[19:16] o AIN[15:0]  (p.37)
#define ADC1_SSMUX1_R           (*((volatile uint32_t *)0x40039060)) // Registro para configurar la entrada analógica para el Secuenciador 1 (p.1074)
#define ADC1_SSCTL1_R           (*((volatile uint32_t *)0x40039064)) // Registro que configura la muestra ejecutada con el Secuenciador 1 (p.1074)
#define ADC1_SSFIFO1_R          (*((volatile uint32_t *)0x40039068)) //Registro que contiene los resultados de conversión de las muestras recogidas con el secuenciador (p. 1074)
//ADC 1 input4
#define ADC1_SSEMUX0_R          (*((volatile uint32_t *)0x40039058)) // Registro que selecciona entre las entradas AIN[19:16] o AIN[15:0]  (p.37)
#define ADC1_SSMUX0_R           (*((volatile uint32_t *)0x40039040)) // Registro para configurar la entrada analógica para el Secuenciador 0 (p.37)
#define ADC1_SSCTL0_R           (*((volatile uint32_t *)0x40039044)) // Registro que configura la muestra ejecutada con el Secuenciador 0 (p.37)
#define ADC1_SSFIFO0_R          (*((volatile uint32_t *)0x40039048)) //Registro que contiene los resultados de conversión de las muestras recogidas con el secuenciador (p. 37)

//PLL
#define SYSCTL_PLLFREQ0_R       (*((volatile uint32_t *)0x400FE160)) //Registro para configurar el PLL
#define SYSCTL_PLLSTAT_R        (*((volatile uint32_t *)0x400FE168)) //Registro muestra el estado de encendido del PLL
#define SYSCTL_PLLFREQ0_PLLPWR  0x00800000  // Valor para encender el PLL


//Valores posicion potenciOmetros Joystick
volatile uint32_t movXR;
volatile uint32_t movYR;
volatile uint32_t movYL;
volatile uint32_t movXL;

//ADC 1
volatile uint32_t botR;
volatile uint32_t botL;

//delay botones
volatile uint32_t i;
volatile uint32_t j;

//estado del boton R
volatile uint32_t estadoBotonR;
volatile uint32_t ultEstadoBotonR;
volatile uint32_t contadorBotonR;

//estado del boton L
volatile uint32_t estadoBotonL;
volatile uint32_t ultEstadoBotonL;
volatile uint32_t contadorBotonL;


//PUERTO M
volatile uint32_t CountM= 0;
volatile uint32_t TerminoM= 0;
volatile uint32_t accionM=0;
volatile uint32_t ultAccionM=0;
volatile uint32_t limiteM=1000000000;
volatile uint32_t count1M=0;
volatile uint32_t count2M=0;

//PUERTO L
volatile uint32_t CountL= 0;
volatile uint32_t TerminoL= 0;
volatile uint32_t accionL=0;
volatile uint32_t ultAccionL=0;
volatile uint32_t limiteL=1000000000;
volatile uint32_t count1L=0;
volatile uint32_t count2L=0;


//PUERTO N
volatile uint32_t CountN= 0;
volatile uint32_t TerminoN= 0;
volatile uint32_t accionN=0;
volatile uint32_t ultAccionN=0;
volatile uint32_t limiteN=1000000000;
volatile uint32_t count1N=0;
volatile uint32_t count2N=0;

//PUERTO K
volatile uint32_t CountK= 0;
volatile uint32_t TerminoK= 0;
volatile uint32_t accionK=0;
volatile uint32_t ultAccionK=0;
volatile uint32_t limiteK=1000000000;
volatile uint32_t count1K=0;
volatile uint32_t count2K=0;


//Acciones programadas

volatile uint32_t iM;
volatile uint32_t iK;
volatile uint32_t iL;
volatile uint32_t iN;

//control automatico
volatile uint32_t accionAM=0;
volatile uint32_t accionAK=0;
volatile uint32_t accionAL=0;
volatile uint32_t accionAN=0;

//banderas de direccion
volatile uint32_t accionMtemp;
volatile uint32_t accionKtemp;
volatile uint32_t accionLtemp;
volatile uint32_t accionNtemp;

//Bandera programacion Global
volatile uint32_t automat=0;

//arreglos de acciones
volatile uint32_t TerminoM1[]={0,0,0,0,0};
volatile uint32_t TerminoK1[]={0,0,0,0,0};
volatile uint32_t TerminoL1[]={0,0,0,0,0};
volatile uint32_t TerminoN1[]={0,0,0,0,0};
volatile uint32_t accionM1[]={0,0,0,0,0};
volatile uint32_t accionK1[]={0,0,0,0,0};
volatile uint32_t accionL1[]={0,0,0,0,0};
volatile uint32_t accionN1[]={0,0,0,0,0};



//MOTOR A PASOS
/*REGLA  32 * 64 = 2048
* sec grados
*   8  1.40625
*  16  2.8125
*  32  5.625
*  64 11.25
* 128 22.5
* 256 45
* 512 90
*/

enum secMotores {
    AB=0x03,
    ApB=0x06,
    ApBp=0x0C,
    ABp=0x09,
    NN=0x00,
    HIGH=0x01,
    DHIGH=0x02,
    LOW=0x00
};

// RUTINA DE SERVICIO DE INTERRUPCIÓN

void Timer03AIntHandler(void)
{

    TIMER3_ICR_R= 0X00000001 ; //LIMPIA BANDERA DE TIMER3

    if((accionM==1)||(accionAM==1)){
    TerminoM = TerminoM + 1;

          if (TerminoM < limiteM){

            CountM = (CountM+0x01)%0x04;


        switch (CountM) {
               case 0x00:
                 GPIO_PORTM_DATA_R=AB; // A,B
                   break;
               case 0x01:
                 GPIO_PORTM_DATA_R=ApB; // A',B
                   break;
               case 0x02:
                 GPIO_PORTM_DATA_R=ApBp; // A', B'
                   break;
               case 0x03:
                 GPIO_PORTM_DATA_R=ABp; // A,B'
                  break;
           }
          }

   }//fin accionM 1

   else if((accionM==2)||(accionAM==2)){
    TerminoM = TerminoM + 1;

          if (TerminoM < limiteM){

            CountM = (CountM+0x01) %0x04;


        switch (CountM) {
               case 0x00:
                 GPIO_PORTM_DATA_R=ABp; // A,B'
                   break;
               case 0x01:
                 GPIO_PORTM_DATA_R=ApBp; // A', B'
                   break;
               case 0x02:
                 GPIO_PORTM_DATA_R=ApB; // A',B
                   break;
               case 0x03:
                 GPIO_PORTM_DATA_R=AB; // A,B
                  break;
           }
          }

   }else if((accionM==0)||(accionAM==0)){
       GPIO_PORTM_DATA_R=NN;
   } //fin accionM

 if((accionL==1)||(accionAL==1)){
    TerminoL = TerminoL + 1;


      if (TerminoL < limiteL){

        CountL = (CountL+0x01) % 0x04;

    switch (CountL) {
           case 0x00:
             GPIO_PORTL_DATA_R=AB; // A,B
               break;
           case 0x01:
             GPIO_PORTL_DATA_R=ApB; // A',B
               break;
           case 0x02:
             GPIO_PORTL_DATA_R=ApBp; // A', B'
               break;
           case 0x03:
             GPIO_PORTL_DATA_R=ABp; // A,B'
              break;
           default:
              break;
       }
      }

   }

   else if((accionL==2)||(accionAL==2)){
    TerminoL = TerminoL + 1;

      if (TerminoL < limiteL){

        CountL = (CountL+0x01)%0x04;


    switch (CountL) {
               case 0x00:
                 GPIO_PORTL_DATA_R=ABp; // A,B'
                   break;
               case 0x01:
                 GPIO_PORTL_DATA_R=ApBp; // A', B'
                   break;
               case 0x02:
                 GPIO_PORTL_DATA_R=ApB; // A',B
                   break;
               case 0x03:
                 GPIO_PORTL_DATA_R=AB; // A,B
                  break;
       }
      }

   }else if((accionL==0)||(accionAL==0)){
       GPIO_PORTL_DATA_R=NN;
   }//fin accionL

   if(accionN==1){
    TerminoN = TerminoN + 1;


            if (TerminoN < limiteN){

              CountN = (CountN+0x01)%0x04;


          switch (CountN) {
                 case 0x00:
                   GPIO_PORTN_DATA_R=AB; // A,B
                   break;
                 case 0x01:
                   GPIO_PORTN_DATA_R=ApB; // A',B
                   break;
                 case 0x02:
                   GPIO_PORTN_DATA_R=ApBp; // A', B'
                   break;
                 case 0x03:
                   GPIO_PORTN_DATA_R=ABp; // A, B'
                    break;
             }
            }

   }

   else if((accionN==2)||(accionAN==2)){
    TerminoN = TerminoN + 1;
            if (TerminoN < limiteN){

              CountN = (CountN+0x01)%0x04;

          switch (CountN) {
                 case 0x00:
                   GPIO_PORTN_DATA_R=ABp; //  A, B'
                   break;
                 case 0x01:
                   GPIO_PORTN_DATA_R=ApBp; //A',B
                   break;
                 case 0x02:
                   GPIO_PORTN_DATA_R=ApB; //  A', B'
                   break;
                 case 0x03:
                   GPIO_PORTN_DATA_R=AB; //A,B
                    break;
             }
            }

   }else if((accionN==0)||(accionAN==1)){
       GPIO_PORTN_DATA_R=NN;
   }       //fin accionN

   //secuencia apertura pinza
    if((accionK==1)||(accionAK==1)){
         TerminoK = TerminoK + 1;
         if (TerminoK < limiteK){

            CountK = (CountK+0x01)%0x04;


        switch (CountK) {
               case 0x00:
                 GPIO_PORTK_DATA_R=AB; // A,B
                 break;
               case 0x01:
                 GPIO_PORTK_DATA_R=ApB; // A',B
                 break;
               case 0x02:
                 GPIO_PORTK_DATA_R=ApBp; // A', B'
                 break;
               case 0x03:
                 GPIO_PORTK_DATA_R=ABp; // A, B'
                  break;
           }
          }

     }
    //secuencia cierre pinza
    else if((accionK==2)||(accionAK==2)){

             TerminoK = TerminoK + 1;
             //if (TerminoK < 512){
             if (TerminoK < limiteK){

                CountK = (CountK+0x01)%0x04;


            switch (CountK) {
                   case 0x00:
                       GPIO_PORTK_DATA_R=ABp; // A, B'
                     break;
                   case 0x01:
                     GPIO_PORTK_DATA_R=ApBp; // A', B'
                     break;
                   case 0x02:
                     GPIO_PORTK_DATA_R=ApB; // A',B
                     break;
                   case 0x03:
                     GPIO_PORTK_DATA_R=AB; // A,B
                      break;
               }
              }

         }
         else if((accionK==0)||(accionAK==0)){
             GPIO_PORTK_DATA_R=NN;
         }//FIN accionK

   }//FIN TIMER 3B

void main(void){

    //SYSCTL_RCGCGPIO_R |= 0X1E18; // 1) HABILITA RELOJ PARA EL PUERTOS D,E,M,K,L,N (p. 382)


//    SYSCTL_RCGCGPIO_R |= 0X1E12; // 1) HABILITA RELOJ PARA EL PUERTOS B,E,M,K,L,N (p. 382)
    SYSCTL_RCGCGPIO_R |= 0X1E18; // 1) HABILITA RELOJ PARA EL PUERTOS E,M,K,L,N (p. 382)
    //MOTORES A PASOS
    SYSCTL_RCGCTIMER_R |= 0X08; //RELOJ Y HABILITA TIMER 3 (p.380)
    //retardo para que el reloj alcance los puertos E,M,K,L,N Y TIMER 3
    //while ((SYSCTL_PRGPIO_R & 0X1E12) == 0){};  // reloj listo?
    while ((SYSCTL_PRGPIO_R & 0X1E18) == 0){};  // reloj listo?

    TIMER3_CTL_R=0X00000000; //DESHABILITA TIMER 3 PARA CONFIGURAR (p.986)
    TIMER3_CFG_R= 0X00000000; //CONFIGURA TIMER DE 32 BITS (p. 976)
    //TIMER3_TAMR_R= 0X00000002; //CONFIGURAR PARA MODO PERIODICO CUENTA HACIA ABAJO (p. 977)
    TIMER3_TAILR_R= 0X00007530; // VALOR DE RECARGA (p.1004)
    TIMER3_TAPR_R= 0X00; // PRESCALADOR DE TIMER A, SOLO PARA MODOS DE 16 BITS (p.1008)
    TIMER3_ICR_R= 0X00000001 ; //LIMPIA POSIBLE BANDERA PENDIENTE DE TIMER3 (p.1002)
    TIMER3_IMR_R |= 0X00000001; //ACTIVA INTRRUPCION DE TIMEOUT (p.993)
    NVIC_EN1_R= 1<<(35-32); //HABILITA LA INTERRUPCION 35 (TIMER3 A)
    TIMER3_CTL_R |= 0X00000001; //HABILITA TIMER 3 (p.986)

    // habilita al Puerto L como salida digital para control de motor
    // PL0,...,PL3 como salidas hacia el ULN2003 (A,A´,B,B´)
    GPIO_PORTL_DIR_R = 0x0F;//->F habilita los primeros 4 bits del puerto
    GPIO_PORTL_DEN_R = 0x0F;//(F->1111)
    GPIO_PORTL_DATA_R = 0x09;//9->1001
    // habilita al Puerto F como salida digital para control de motor
    // PF0,...,PF3 como salidas hacia el ULN2003 (A,A´,B,B´)
    GPIO_PORTM_DIR_R = 0x0F;//->F habilita los primeros 4 bits del puerto
    GPIO_PORTM_DEN_R = 0x0F;//(F->1111)
    GPIO_PORTM_DATA_R = 0x09;//9->1001
    // habilita al Puerto N como salida digital para control de motor
    // PN0,...,PN3 como salidas hacia el ULN2003 (A,A´,B,B´)
    GPIO_PORTN_DIR_R = 0x0F;//->F habilita los primeros 4 bits del puerto
    GPIO_PORTN_DEN_R = 0x0F;//(F->1111)
    GPIO_PORTN_DATA_R = 0x09;//9->1001
    // habilita al Puerto K como salida digital para control de motor
    // PK0,...,PK3 como salidas hacia el ULN2003 (A,A´,B,B´)
    GPIO_PORTK_DIR_R = 0x0F;//->F habilita los primeros 4 bits del puerto
    GPIO_PORTK_DEN_R = 0x0F;//(F->1111)
    GPIO_PORTK_DATA_R = 0x09;//9->1001
    //Joysticks
    GPIO_PORTE_AHB_DIR_R = 0x00;    // 2) PE4 entrada (analógica) (Puerto E)
    GPIO_PORTE_AHB_AFSEL_R |= 0x10; // 3) Habilita Función Alterna de PE4 (Puerto E)
    GPIO_PORTE_AHB_DEN_R = 0x00;    // 4) Deshabilita Función Digital de PE4 (Puerto E)
    GPIO_PORTE_AHB_AMSEL_R |= 0x10; // 5) Habilita Función Analógica de PE4 (Puerto E)

    //Botones

        GPIO_PORTD_AHB_DIR_R = 0x00;    // 2) PE4 entrada (analógica) (Puerto E)
        GPIO_PORTD_AHB_AFSEL_R |= 0x10; // 3) Habilita Función Alterna de PE4 (Puerto E)
        //GPIO_PORTD_AHB_AFSEL_R |= 0x1F; // 3) Habilita Función Alterna de PE4 (Puerto E)
        GPIO_PORTD_AHB_DEN_R = 0x00;    // 4) Deshabilita Función Digital de PE4 (Puerto E)
        GPIO_PORTD_AHB_AMSEL_R |= 0x10; // 5) Habilita Función Analógica de PE4 (Puerto E)
        //GPIO_PORTD_AHB_AMSEL_R |= 0x1F; // 5) Habilita Función Analógica de PE4 (Puerto E)

/*
    GPIO_PORTB_AHB_DIR_R = 0x00;    // 2) PE4 entrada (analógica) (Puerto E)
    GPIO_PORTB_AHB_AFSEL_R |= 0x10; // 3) Habilita Función Alterna de PE4 (Puerto E)
    //GPIO_PORTD_AHB_AFSEL_R |= 0x1F; // 3) Habilita Función Alterna de PE4 (Puerto E)
    GPIO_PORTB_AHB_DEN_R = 0x00;    // 4) Deshabilita Función Digital de PE4 (Puerto E)
    GPIO_PORTB_AHB_AMSEL_R |= 0x10; // 5) Habilita Función Analógica de PE4 (Puerto E)
*/
    //SYSCTL_RCGCADC_R  = 0x01;   // 6) Habilita reloj para lógica de ADC0
    //while((SYSCTL_PRADC_R&0x01)==0);// Se espera a que el reloj se estabilice

      SYSCTL_RCGCADC_R  = 0x03;   // 6) Habilita reloj para lógica de ADC0 y ADC1
      while((SYSCTL_PRADC_R&0x01)==0);// Se espera a que el reloj se estabilice


    //ADC 0
    ADC0_PC_R = 0x01;       // 7) Configura para 125Ksamp/s
    ADC0_SSPRI_R = 0x0123;  // 8) SS3 con la más alta prioridad
    ADC0_ACTSS_R = 0x0000;  // 9) Deshabilita SS3 (SS2, SS1, SS0) antes de cambiar configuración de registros
    ADC0_EMUX_R = 0x0000;   // 10) Se configura SS3 (SS2, SS1, SS0) para iniciar muestreo por software
    ADC0_SSEMUX3_R = 0x00;  // 11)Entradas AIN(15:0)
    ADC0_SSEMUX2_R = 0x00;  // 11)Entradas AIN(15:0)
    ADC0_SSEMUX1_R = 0x00;  // 11)Entradas AIN(15:0)
    ADC0_SSEMUX0_R = 0x00;  // 11)Entradas AIN(15:0)

    //primer Lectura
    ADC0_SSMUX3_R = (ADC0_SSMUX3_R & 0xFFFFFFF0) + 8; // canal AIN8 (PE5)
    //segunda Lectura
    ADC0_SSMUX2_R = (ADC0_SSMUX2_R & 0xFFFFFFF0) + 9; // canal AIN9 (PE4)
    //tercer Lectura
    ADC0_SSMUX1_R = (ADC0_SSMUX1_R & 0xFFFFFFF0) + 2; // canal AIN2 (PE1)
    //cuarta Lectura
    ADC0_SSMUX0_R = (ADC0_SSMUX1_R & 0xFFFFFFF0) + 1; // canal AIN2 (PE2)

    ADC0_SSCTL3_R = 0x0006; // 12) SI: AIN, Habilitación de INR3, Fin de secuencia; No:muestra diferencial
    ADC0_SSCTL2_R = 0x0006; // 12) SI: AIN, Habilitación de INR2, Fin de secuencia; No:muestra diferencial
    ADC0_SSCTL1_R = 0x0006; // 12) SI: AIN, Habilitación de INR1, Fin de secuencia; No:muestra diferencial
    ADC0_SSCTL0_R = 0x0006; // 12) SI: AIN, Habilitación de INR1, Fin de secuencia; No:muestra diferencial
    ADC0_IM_R = 0x0000;     // 13) Deshabilita interrupciones de SS0,SS1,SS2,SS3
    ADC0_ACTSS_R |= 0x000F; // 14) Habilita SS3, SS2, SS1, SS0 (p. 1077)

    //ADC 1
    ADC1_PC_R = 0x01;       // 7) Configura para 125Ksamp/s
    ADC1_SSPRI_R = 0x0123;  // 8) SS3 con la más alta prioridad
    ADC1_ACTSS_R = 0x0000;  // 9) Deshabilita SS3 (SS2, SS1, SS0) antes de cambiar configuración de registros
    ADC1_EMUX_R = 0x0000;   // 10) Se configura SS3 (SS2, SS1, SS0) para iniciar muestreo por software
    ADC1_SSEMUX3_R = 0x00;  // 11)Entradas AIN(15:0)
    ADC1_SSEMUX2_R = 0x00;  // 11)Entradas AIN(15:0)
    ADC1_SSEMUX1_R = 0x00;  // 11)Entradas AIN(15:0)
    ADC1_SSEMUX0_R = 0x00;  // 11)Entradas AIN(15:0)

    /*
    //primer Lectura
    //ADC1_SSMUX3_R = (ADC0_SSMUX3_R & 0xFFFFFFF0) + 14; // canal AIN14 (PD1)boton L
    ADC1_SSMUX3_R = (ADC0_SSMUX3_R & 0xFFFFFFF0) + 10; // canal AIN10 (PB4)boton L
    //segunda Lectura
    //ADC1_SSMUX2_R = (ADC0_SSMUX2_R & 0xFFFFFFF0) + 15; // canal AIN15 (PD0)boton L
    ADC1_SSMUX2_R = (ADC0_SSMUX2_R & 0xFFFFFFF0) + 11; // canal AIN11 (PB5)boton L

    //tercer Lectura
    //ADC1_SSMUX1_R = (ADC0_SSMUX1_R & 0xFFFFFFF0) + algo; // canal AIN
    //cuarta Lectura
    //ADC1_SSMUX0_R = (ADC0_SSMUX1_R & 0xFFFFFFF0) + algo; // canal AIN
*/

    //Tabla de Pines a seleccionar (p. 1794)

     //primer Lectura
     ADC1_SSMUX3_R = (ADC1_SSMUX3_R & 0xFFFFFFF0) + 14; // canal AIN14 (PD1)
     //segunda Lectura
     ADC1_SSMUX2_R = (ADC1_SSMUX2_R & 0xFFFFFFF0) + 15; // canal AIN15 (PD0)
     //tercer Lectura
     //ADC1_SSMUX1_R = (ADC1_SSMUX1_R & 0xFFFFFFF0) + algo; // canal AIN
     //cuarta Lectura
     //ADC1_SSMUX1_R = (ADC1_SSMUX1_R & 0xFFFFFFF0) + aglo; // canal AIN

    ADC1_SSCTL3_R = 0x0006; // 12) SI: AIN, Habilitación de INR3, Fin de secuencia; No:muestra diferencial
    ADC1_SSCTL2_R = 0x0006; // 12) SI: AIN, Habilitación de INR2, Fin de secuencia; No:muestra diferencial
    ADC1_SSCTL1_R = 0x0006; // 12) SI: AIN, Habilitación de INR1, Fin de secuencia; No:muestra diferencial
    ADC1_SSCTL0_R = 0x0006; // 12) SI: AIN, Habilitación de INR1, Fin de secuencia; No:muestra diferencial
    ADC1_IM_R = 0x0000;     // 13) Deshabilita interrupciones de SS0,SS1,SS2,SS3
    ADC1_ACTSS_R |= 0x000F; // 14) Habilita SS3, SS2, SS1, SS0 (p. 1077)


    SYSCTL_PLLFREQ0_R |= SYSCTL_PLLFREQ0_PLLPWR;    // encender PLL
    while((SYSCTL_PLLSTAT_R&0x01)==0);              // espera a que el PLL fije su frecuencia
    SYSCTL_PLLFREQ0_R &= ~SYSCTL_PLLFREQ0_PLLPWR;   // apagar PLL

    ADC0_ISC_R = 0x000F;                    // Se recomienda Limpiar la bandera RIS del ADC0
    ADC1_ISC_R = 0x000F;                    // Se recomienda Limpiar la bandera RIS del ADC1
    for(;;){
      ADC0_PSSI_R = 0x000F;             // Inicia conversión de SS0, SS1 SS2 y SS3
      ADC1_PSSI_R = 0x000F;             // Inicia conversión de SS0, SS1 SS2 y SS3

      while ((ADC0_RIS_R & 0x08)==0);   // Espera a que SS3 termine conversión (polling)
      movXR = (ADC0_SSFIFO3_R & 0xFFF); // Resultado en FIFO3 se asigna a variable "movXR"
      movYR = (ADC0_SSFIFO2_R & 0xFFF); // Resultado en FIFO2 se asigna a variable "movYR"
      movYL = (ADC0_SSFIFO1_R & 0xFFF); // Resultado en FIFO1 se asigna a variable "movYL"
      movXL = (ADC0_SSFIFO0_R & 0xFFF); // Resultado en FIFO0 se asigna a variable "movYL"
      botR = (ADC1_SSFIFO3_R & 0xFFF); // Resultado en FIFO1 se asigna a variable "movYL"
      botL = (ADC1_SSFIFO2_R & 0xFFF); // Resultado en FIFO0 se asigna a variable "movYL"
      ADC0_ISC_R = 0x000F;              // Limpia la bandera RIS del ADC0
      ADC1_ISC_R = 0x000F;              // Limpia la bandera RIS del ADC0

      //Banderas movimiento de motores

            //Mueve eje Y (PL) hacia atras
                if(movYR>3500){
                     if(movYR<=4095){
                            accionL=1;
                     }
                }

               //Mueve eje Y (PL) hacia adelante
                else if(movYR<2200){
                            accionL=2;
                }
                //regresa eje Y a punto muerto
                          else if(movYR<3800){
                              if(movYR>2200){
                                     accionL=0;
                              }
                          }
                ultAccionL=accionL;
                //Mueve eje X (PM) hacia izquierda
                if(movXR>3800){
                      if(movXR<=4095){
                            accionM=2;
                      }
                }

                //Mueve eje X (PM) hacia derecha
                else if(movXR<2200){
                                accionM=1;
                    }
                //regresa eje X a punto muerto
                     else if(movXR<3800){
                              if(movXR>2200){
                                     accionM=0;
                              }
                          }
                ultAccionM=accionM;
                //Mueve eje Z (PN) hacia abajo
                  if(movYL>3800){
                       if(movYL<=4095){
                                accionN=1;
                        }
                    }

                 //Mueve eje Z (PN) hacia arriba
                   else if(movYL<2200){
                                    accionN=2;
                       }
                  //regresa eje Z a punto muerto
                            else if(movYL<3800){
                                if(movYL>2200){
                                       accionN=0;
                                }
                            }
                  ultAccionN=accionN;
                  //Mueve pinza (PK) abre
                      if(movXL>3800){
                           if(movXL<=4095){
                                  accionK=1;
                            }
                        }

                   //Mueve pinza (PK) cierra
                      else if(movXL<2200){
                                  accionK=2;
                         }
                   //regresa pinza a punto muerto
                      else if(movXL<3800){
                          if(movXL>2200){
                                 accionK=0;
                          }
                      }
                      ultAccionK=accionK;
            //fin banderas de motores

                      if(botR<500){
                          estadoBotonR=1;
                         // ciclo for retarda toma de lectura 10 ms
                          for(i=0;i<4000;i++){
                          }
                      }else{
                          estadoBotonR=0;
                      }

                      if(botL<500){
                          estadoBotonL=1;
                          //ciclo for retarda toma de lectura
                          for(j=0;j<32000;j++){
                             }
                      }else{
                          estadoBotonL=0;
                      }

                      if(estadoBotonR!=ultEstadoBotonR){
                            if(estadoBotonR==1){
                                    contadorBotonR=contadorBotonR+1;
                            }
                      }
                      ultEstadoBotonR=estadoBotonR;

                      if(estadoBotonL!=ultEstadoBotonL){
                                  if(estadoBotonL==1){
                                            contadorBotonL=contadorBotonL+1;
                                   }
                      }
                      ultEstadoBotonL=estadoBotonL;

                      //resetea trayectorias;
                      if(contadorBotonL>0){
                          //limpia posiciones
                          TerminoM=0;
                          TerminoK=0;
                          TerminoL=0;
                          TerminoN=0;
                          //limpia boton
                          contadorBotonL=0;
                          //limpia contadores de direccion
                          count1M=0;
                          count2M=0;
                          count1K=0;
                          count2K=0;
                          count1L=0;
                          count2L=0;
                          count1N=0;
                          count2N=0;
                      }

                                  if(accionM==1){
                                            count1M=count1M+1;
                                   }else if(accionM==2){
                                            count2M=count2M+1;
                                   }

                                   if(accionK==1){
                                             count1K=count1K+1;
                                    }else if(accionK==2){
                                             count2K=count2K+1;
                                    }

                                    if(accionL==1){
                                              count1L=count1L+1;
                                     }else if(accionL==2){
                                              count2L=count2L+1;
                                     }

                                     if(accionN==1){
                                               count1N=count1N+1;
                                      }else if(accionN==2){
                                               count2N=count2N+1;
                                      }
//Banderas de direccion a almacenar
                                     if(count1M>count2M){
                                           accionMtemp=HIGH;
                                       }
                                       else if(count1M<count2M){
                                           accionMtemp=2;
                                       }
                                       if(count1K>count2K){
                                           accionKtemp=HIGH;
                                       }
                                       else if(count1K<count2K){
                                           accionKtemp=2;
                                       }
                                       if(count1L>count2L){
                                            accionLtemp=HIGH;
                                       }
                                       else if(count1L<count2L){
                                           accionLtemp=2;
                                       }
                                       if(count1N>count2N){
                                            accionNtemp=HIGH;
                                       }
                                       else if(count1N<count2N){
                                           accionN1[2]=2;
                                       }


                      switch (contadorBotonR) {
                             case 1:

                                 TerminoM1[0]=TerminoM;
                                 TerminoK1[0]=TerminoK;
                                 TerminoL1[0]=TerminoL;
                                 TerminoN1[0]=TerminoN;
                                     accionM1[0]=accionMtemp;
                                     accionK1[0]=accionKtemp;
                                     accionL1[0]=accionLtemp;
                                     accionN1[0]=accionNtemp;
                               break;
                             case 2:

                                 TerminoM1[1]=TerminoM;
                                 TerminoK1[1]=TerminoK;
                                 TerminoL1[1]=TerminoL;
                                 TerminoN1[1]=TerminoN;
                                 accionM1[1]=accionMtemp;
                                           accionK1[1]=accionKtemp;
                                                                    accionL1[1]=accionLtemp;
                                                                    accionN1[1]=accionNtemp;

                                 break;
                             case 3:

                                 TerminoM1[2]=TerminoM;
                                 TerminoK1[2]=TerminoK;
                                 TerminoL1[2]=TerminoL;
                                 TerminoN1[2]=TerminoN;
                                 accionM1[2]=accionMtemp;
                                                                    accionK1[2]=accionKtemp;
                                                                    accionL1[2]=accionLtemp;
                                                                    accionN1[2]=accionNtemp;

                               break;
                             case 4:
                                 TerminoM1[3]=TerminoM;
                                 TerminoK1[3]=TerminoK;
                                 TerminoL1[3]=TerminoL;
                                 TerminoN1[3]=TerminoN;
                                 accionM1[3]=accionMtemp;
                                                                    accionK1[3]=accionKtemp;
                                                                    accionL1[3]=accionLtemp;
                                                                    accionN1[3]=accionNtemp;

                                 break;
                             case 5:

                                 TerminoM1[4]=TerminoM;
                                 TerminoK1[4]=TerminoK;
                                 TerminoL1[4]=TerminoL;
                                 TerminoN1[4]=TerminoN;
                                 accionM1[4]=accionMtemp;
                                                                    accionK1[4]=accionKtemp;
                                                                    accionL1[4]=accionLtemp;
                                                                    accionN1[4]=accionNtemp;

                               break;
                             case 6:
                                 automat=HIGH;
                                break;
                             case 7:
                                 contadorBotonR=0;
                                 automat=LOW;
                             break;

                         }


                      if(automat==HIGH){
                          //BLOQUE 1
                          for(iM=0;iM<TerminoM1[0]*600;iM++){
                                accionAM=accionM1[0];
                          }
                          for(iK=0;iK<TerminoK1[0]*600;iK++){
                                accionAK=accionK1[0];
                          }
                          for(iL=0;iL<TerminoL1[0]*600;iL++){
                                accionAL=accionL1[0];
                          }
                          for(iN=0;iN<TerminoN1[0]*600;iN++){
                                accionAN=accionN1[0];
                          }
                          //BLOQUE 2
                          for(iM;iM<TerminoM1[1]*600;iM++){
                               accionAM=accionM1[1];
                          }
                          for(iK;iK<TerminoK1[1]*600;iK++){
                                accionAK=accionK1[1];
                          }
                          for(iL;iL<TerminoL1[1]*600;iL++){
                                accionAL=accionL1[1];
                          }
                          for(iN;iN<TerminoN1[1]*600;iN++){
                                accionAN=accionN1[1];
                          }
                          //BLOQUE 3
                          for(iM;iM<TerminoM1[2]*600;iM++){
                                accionAM=accionM1[2];
                          }
                          for(iK;iK<TerminoK1[2]*600;iK++){
                                accionAK=accionK1[2];
                          }
                          for(iL;iL<TerminoL1[2]*600;iL++){
                                accionAL=accionL1[2];
                          }
                          for(iN;iN<TerminoN1[2]*600;iN++){
                                accionAN=accionN1[2];
                          }
                          //BLOQUE 4
                          for(iM;iM<TerminoM1[3]*600;iM++){
                                accionAM=accionM1[3];
                          }
                          for(iK;iK<TerminoK1[3]*600;iK++){
                                accionAK=accionK1[3];
                          }
                          for(iL;iL<TerminoL1[3]*600;iL++){
                                accionAL=accionL1[3];
                          }
                          for(iN;iN<TerminoN1[3]*600;iN++){
                                accionAN=accionN1[3];
                          }
                          //BLOQUE 5
                          for(iM;iM<TerminoM1[4]*600;iM++){
                                accionAM=accionM1[4];
                          }
                          for(iK;iK<TerminoK1[4]*600;iK++){
                                accionAK=accionK1[4];
                          }
                          for(iL;iL<TerminoL1[4]*600;iL++){
                                accionAL=accionL1[4];
                          }
                          for(iN;iN<TerminoN1[4]*600;iN++){
                                accionAN=accionN1[4];
                          }
                          contadorBotonR=7;
                      }else{
                          accionAL=LOW;
                          accionAK=LOW;
                          accionAM=LOW;
                          accionAN=LOW;
                      }

    }//FOR INFINITO

}
