/*
 * I2C_LCD.h
 *
 *  Created on: 18 jun. 2020
 *      Author: root
 */

//#ifndef I2C_LCD_H_
//#define I2C_LCD_H_

//#endif /* I2C_LCD_H_ */

/*
 * I2C_LDC.h
 *
 *  Created on: 18 jun. 2020
 *      Author: root
 */

//#ifndef I2C_LDC_H_
//#define I2C_LDC_H_

//#endif /* I2C_LDC_H_ */



//*** PROGRAMA QUE CONFIGURA EL PERIFERICO I2C ***
//*** EN EL TIVA TM4C1294 COMO MAESTRO Y ESCLAVO PCF8574 para LCD 16x2***
void SysTick_Init(void);

void SysTick_Wait(uint32_t retardo);

void I2C_Init(void);

int I2C_Master_Wait();

void I2C_Master_Init(int SubAdd);

void I2C_Master_Write(int dato);

void I2C_Master_Stop();

void IO_Expander_Write(int dato);

void LCD_Write_4Bit(unsigned char Nibble);

void LCD_CMD(int CMD);

void LCD_Init();

void LCD_Set_Cursor(unsigned char ROW, unsigned char COL);

void LCD_Write_Char(char Data);

void LCD_Write_String(char* Str);

void LCD_Write_Digit(int dato);
