
/*Asignatura: Microcontroladores y microprocesadores.   *Semestre: 2019-2.
*Tema 9: Perifericos
*Punto 9.2: El temporizador y sus aplicaciones.
*Pr�ctica: Motor a pasos unipolar.
*Presentan: Dr. Sa�l de la Rosa Nieves,  Ing. Jose Eduardo Villa Herrera.
-------------------------------------------------------------------------------
*/
// LIBRERIAS QUE UTILIZA EL PROGRAMA
#include <stdbool.h>
#include <stdint.h>
#include "inc/tm4c1294ncpdt.h"

#include "I2C_LCD.h"

//REGISTROS DE RELOJ
#define SYSCTL_RCGCGPIO_R       (*((volatile uint32_t *)0x400FE608)) //Reloj del puerto
#define SYSCTL_RCGCI2C_R        (*((volatile uint32_t *)0x400FE620)) //Reloj de I2C
#define SYSCTL_PRGPIO_R        (*((volatile uint32_t *)0x400FEA08)) //Bandera de "Peripherial Ready"

//REGISTROS DEL PUERTO B
#define GPIO_PORTB_DATA_R   (*((volatile uint32_t *)0x400593FC)) //Para los datos del puerto
#define GPIO_PORTB_DIR_R    (*((volatile uint32_t *)0x40059400)) //Para seleccionar funciOn
#define GPIO_PORTB_AFSEL_R  (*((volatile uint32_t *)0x40059420)) //Para seleccionar funciOn alterna
#define GPIO_PORTB_ODR_R    (*((volatile uint32_t *)0x4005950C)) //Para activar el Open Drain
#define GPIO_PORTB_DEN_R    (*((volatile uint32_t *)0x4005951C)) //Para activar funci�n digital
#define GPIO_PORTB_PCTL_R   (*((volatile uint32_t *)0x4005952C)) //Para el control del puerto

//REGISTROS DEL MOUDLO I2C
#define I2C0_MSA_R              (*((volatile uint32_t *)0x40020000)) //I2C Master Slave Adress
#define I2C0_MCS_R              (*((volatile uint32_t *)0x40020004)) //I2C Master Control Status
#define I2C0_MDR_R              (*((volatile uint32_t *)0x40020008)) //I2C Master Data Register
#define I2C0_MTPR_R             (*((volatile uint32_t *)0x4002000C)) //I2C Master Time Period
#define I2C0_MCR_R              (*((volatile uint32_t *)0x40020020)) //I2C Master Configuration Register






// ***** Variables globales ***** //
volatile uint32_t i;
volatile uint32_t dato;
volatile uint32_t Count= 0;
volatile uint32_t Termino= 0;

volatile uint32_t buttonPushCounter = 0;  // counter for the number of button presses
volatile uint32_t buttonState = 0;        // current state of the button
volatile uint32_t lastButtonState = 0;    // previous state of the button

volatile uint32_t inicio=0;
volatile uint32_t fin=0;
//volatile uint32_t retlec=50000;//retraso de lecturas

volatile uint32_t retlec=400000;//retraso de lecturas

volatile uint32_t cuenta0=0;
volatile uint32_t cuenta1=0;
volatile uint32_t cuenta2=0;

volatile uint32_t estado;

volatile uint32_t Rojo_Frec;
volatile uint32_t Verde_Frec;
volatile uint32_t Azul_Frec;



// DefiniciOn de constantes para operaciones
#define NVIC_ST_CTRL_COUNT      0x00010000  // bandera de cuenta flag
#define NVIC_ST_CTRL_CLK_SRC    0x00000004  // Clock Source
#define NVIC_ST_CTRL_INTEN      0x00000002  // Habilitador de interrupci�n
#define NVIC_ST_CTRL_ENABLE     0x00000001  // Modo del contador
#define NVIC_ST_RELOAD_M        0x00FFFFFF  // Valor de carga del contador




//**********FUNCION Inizializa el SysTick *********************
/*void SysTick_Init(void){
  NVIC_ST_CTRL_R = 0;                   // Desahabilita el SysTick durante la configuraci�n
  NVIC_ST_RELOAD_R = NVIC_ST_RELOAD_M;  // Se establece el valor de cuenta deseado en RELOAD_R
  NVIC_ST_CURRENT_R = 0;                // Se escribe al registro current para limpiarlo

  NVIC_ST_CTRL_R = 0x00000001;         // Se Habilita el SysTick y se selecciona la fuente de reloj
}*/

//*************FUNCION Tiempo de retardo utilizando wait.***************
// El parametro de retardo esta en unidades del reloj interno/4 = 4 MHz (250 ns)
/*void SysTick_Wait(uint32_t retardo){
    NVIC_ST_RELOAD_R= retardo-1;   //nUmero de cuentas por esperar
    NVIC_ST_CURRENT_R = 0;
    while((NVIC_ST_CTRL_R&0x00010000)==0){//espera hasta que la bandera COUNT sea valida
    }
   } */

int pulseIn(){
    // read the pushbutton input pin:
                   buttonState = GPIO_PORTD_AHB_DATA_R;

                   // compare the buttonState to its previous state
                   if (buttonState != lastButtonState) {
                     // if the state has changed, increment the counter
                     if (buttonState == 16) {
                       // if the current state is HIGH then the button went from off to on:
                       //buttonPushCounter++;
                       inicio=Termino;
                       //GPIO_PORTD_AHB_DATA_R==0x000;
                     } else if (buttonState == 0) {
                       // if the current state is LOW then the button went from on to off:
                         fin=Termino;
                     }
                     // Delay a little bit to avoid bouncing
                     //SysTick_Wait(200000);// 0.05[s]


                   }
                   // save the current state as the last state, for next time through the loop
                   lastButtonState = buttonState;

                   //punto anterior limpia entrada digital sensor
                   //GPIO_PORTD_AHB_DATA_R==0x010;
                   return fin-inicio;
}

// RUTINA DE SERVICIO DE INTERRUPCI�N
void Timer03AIntHandler(void)
{
    //LIMPIA BANDERA
    TIMER3_ICR_R= 0X00000001 ; //LIMPIA BANDERA DE TIMER3
    Termino = Termino + 1;


    // 32 * 64 = 2048
     if (Termino < 1024)
     {
    Count = Count + 0x01;

    switch (Count&0x0F) {
           case 0x01:
             GPIO_PORTL_DATA_R=0x03; // A,B
             GPIO_PORTN_DATA_R = 0x03; // A,B
            // GPIO_PORTF_AHB_DATA_R = 0x00; //
             break;
           case 0x02:
             GPIO_PORTL_DATA_R=0x06; // A',B
             GPIO_PORTN_DATA_R = 0x01; // B
            // GPIO_PORTF_AHB_DATA_R = 0x10; // A'
             break;
           case 0x03:
             GPIO_PORTL_DATA_R=0x0C; // A', B'
             GPIO_PORTN_DATA_R = 0x00; //
            // GPIO_PORTF_AHB_DATA_R = 0x11; //A', B'
             break;
           case 0x04:
             GPIO_PORTL_DATA_R=0x09; // A, B'
             GPIO_PORTN_DATA_R = 0x02; // A
             //GPIO_PORTF_AHB_DATA_R = 0x01; // B'
             Count = 0x0;
              break;
       }
      }else{
          GPIO_PORTL_DATA_R=0x00; //CEROS
      }


     //estado=GPIO_PORTD_AHB_DATA_R;

    }//fin timer




/*********************************************
* SE GENERAN LAS SE�ALES DE CONTROL PARA UN MOTOR UNIPOLAR
* PARA LA SINCRONIZACI�N SE UTILIZA EL TIMER3 EN SU CONFIGURACI�N DE 32 BITS
*
* LAS SE�ALES DE CONTROL SE MANEJAN POR EL PUERTO L
*
* SE UTILIZAN LOS LEDs CONTECTADOS A LOS PUERTOS F y N
* PARA VISUALIZAR LAS SE�ALES GENERADAS
* PF0, PF4 como salidas (Leds).
* PN0, PN1 como salidas (Leds)
*/
 main(void) {

     I2C_Init(); //FunciOn que inicializa los relojes, el GPIO y el I2C0

       //Inicializo Slave
       while(I2C0_MCS_R&0x00000001){}; // espera que el I2C estE listo
       LCD_Init();

       SysTick_Init();

       LCD_Set_Cursor(1, 1);
          LCD_Write_String("Hola mundos :D!!");
          //LCD_Write_Char('a');
          LCD_Set_Cursor(2, 1);
          LCD_Write_String("PabloVivarColina");


     //habilita PORTN, PORTF, PORTL PORTJ
      SYSCTL_RCGCGPIO_R |= 0X1F3A; // RELOJ PARA EL PUERTO N,L,M,K,J,F,E,D,B
      SYSCTL_RCGCTIMER_R |= 0X08; //RELOJ Y HABILITA TIMER 3 (p.380)

         //retardo para que el reloj alcance el PORTN Y TIMER 3
         while ((SYSCTL_PRGPIO_R & 0X1F3A) == 0){};  // reloj listo?
         TIMER3_CTL_R=0X00000000; //DESHABILITA TIMER 3 PARA CONFIGURAR (p.986)
         TIMER3_CFG_R= 0X00000000; //CONFIGURA TIMER DE 32 BITS (p. 976)
         //TIMER3_TAMR_R= 0X00000002; //CONFIGURAR PARA MODO PERIODICO CUENTA HACIA ABAJO (p. 977)
        TIMER3_TAMR_R= 0X00000012; //CONFIGURAR PARA MODO PERIODICO CUENTA HACIA ARRIBA (p. 977)
        //TIMER3_TAILR_R= 0X000F7100; // VALOR DE RECARGA (p.1004)
        //TIMER3_TAILR_R= 0X0004E200; // VALOR DE RECARGA (p.1004)
         //TIMER3_TAILR_R= 0X00027100; // VALOR DE RECARGA (p.1004)
        //TIMER3_TAILR_R= 0X00013780; // VALOR DE RECARGA (p.1004)
       //TIMER3_TAILR_R= 0X00009C40; // VALOR DE RECARGA (p.1004)

      // TIMER3_TAILR_R= 0X00004E20; // VALOR DE RECARGA (p.1004) //VALOR 20000


      //TIMER3_TAILR_R= 0X0000445C; // VALOR DE RECARGA (p.1004)//VALOR 17500 (NO ESTABLE)
       //TIMER3_TAILR_R= 0X00003A98; // VALOR DE RECARGA (p.1004)//VALOR 15000 (NO ESTABLE)
       //TIMER3_TAILR_R= 0X000038A4; // VALOR DE RECARGA (p.1004)//VALOR 14500 (YA NO FUNCIONA)
      //TIMER3_TAILR_R= 0X000036B0; // VALOR DE RECARGA (p.1004)//VALOR 14000 (YA NO FUNCIONA)
      //TIMER3_TAILR_R= 0X000030D4; // VALOR DE RECARGA (p.1004)//VALOR 12500 (YA NO FUNCIONA)

      TIMER3_TAILR_R= 0X000000C8; // VALOR DE RECARGA (p.1004)//VALOR 200 (YA NO FUNCIONA)

        //TIMER3_TAILR_R= 0X00002710; // VALOR DE RECARGA (p.1004)//VALOR 10000 (YA NO FUNCIONA)
        TIMER3_TAPR_R= 0X00; // PRESCALADOR DE TIMER A, SOLO PARA MODOS DE 16 BITS (p.1008)
         TIMER3_ICR_R= 0X00000001 ; //LIMPIA POSIBLE BANDERA PENDIENTE DE TIMER3 (p.1002)
         TIMER3_IMR_R |= 0X00000001; //ACTIVA INTRRUPCION DE TIMEOUT (p.993)
         NVIC_EN1_R= 1<<(35-32); //HABILITA LA INTERRUPCION 35 (TIMER3 A)
         TIMER3_CTL_R |= 0X00000001; //HABILITA TIMER 3 (p.986)

         //inserta 1


             // ***** PrgoramaciOn de puertos ***** //
             //SYSCTL_RCGCGPIO_R = 0x1100; // Habilita el reloj del puerto N y puerto J
             i = SYSCTL_RCGCGPIO_R; // Tiempo de retardo para estabilizar el reloj de los puertos

             GPIO_PORTN_DIR_R = 0X03; // Se configura el pin 0 y 1 del puerto N como salida
             GPIO_PORTN_DEN_R = 0X03; // Se configura el pin 0 y 1 del puerto N como digitales

             GPIO_PORTK_DIR_R = 0x0F; // Se configuran todos los pines  K como salida
             GPIO_PORTK_DEN_R = 0x0F;// Se configuran todos los pines puerto k como digitales

             GPIO_PORTJ_AHB_DIR_R = 0; // Se configura el pin 0 y 1 del puerto J como entradas
             GPIO_PORTJ_AHB_DEN_R = 0x03; // Se configura el pin 0 y 1 del puerto J como digitales
             GPIO_PORTJ_AHB_PUR_R = 0x03; // Resistencia en modo Pull-Up


             GPIO_PORTD_AHB_DIR_R &= ~(0x10); //bit 4 entrada
                           GPIO_PORTD_AHB_DEN_R |= 0x10; //BIT 4 DIGITAL
                           GPIO_PORTD_AHB_DATA_R = 0x00; //RESTO DE SALIDAS A 0
                           GPIO_PORTD_AHB_AFSEL_R = 0x10; //FUNCION ALTERNA EN BIT 4
                           GPIO_PORTD_AHB_PCTL_R = 0x00030000; //DIRIGIDO A T3CCP0

             GPIO_PORTN_DATA_R = 0x00; // Pin 0 y 1 del puerto N como apagados


             SysTick_Init();//inicializa servicio de interrupciones por reloj

             // ***** Comienza Loop Principal ***** //

             for(;;){


                 /*switch (buttonPushCounter){
                                      case 1:
                                          cuenta0=fin-inicio;
                                          break;
                                      case 2:
                                          cuenta1=fin-inicio;
                                          break;
                                      case 3:
                                          cuenta2=fin-inicio;
                                          break;
                                      default:
                                          buttonPushCounter=0;
                                          break;
                                 }*/



                 GPIO_PORTK_DATA_R = 0x00;// S2 y S3 ceros
                 SysTick_Wait(retlec);//  400000 100[ms]

                 //cuenta0=pulseIn();
                estado = pulseIn();
                 SysTick_Wait(retlec);//  400000 100[ms]

                 /*GPIO_PORTK_DATA_R = 0x03;// S2 y S3 unos
                 SysTick_Wait(retlec);//  400000 100[ms]

                 cuenta1=pulseIn();
                 SysTick_Wait(retlec);//  400000 100[ms]

                 GPIO_PORTK_DATA_R = 0x02;// S2 cero y S3 uno
                 SysTick_Wait(retlec);//  400000 100[ms]

                 cuenta2=pulseIn();
                 SysTick_Wait(retlec);// 400000 100[ms]*/


                 //dato=GPIO_PORTD_AHB_DATA_R;
                 //if(dato==0){
                     // Al presionar el push button comienza secuencia de encedido y apagado del led
                     /*for(i=0; i<5; i++){
                         GPIO_PORTN_DATA_R |= 0x02; // El led cambia de estado, a encendido
                         SysTick_Wait(2000000);//Funcion de retardo para pasar al siguiente estado del led, 0.5[s]
                         GPIO_PORTN_DATA_R &= ~0x02; // El led cambia de estado, a apagado
                         SysTick_Wait(2000000);//Funcion de retardo para pasar al siguiente estado del led, 0.5[s]

                     }*/
                 //}

                 //else if(dato==16){

                  //   dato=GPIO_PORTD_AHB_DATA_R;
                        // Al presionar el push button comienza secuencia de encedido y apagado del led
                    //    for(i=0; i<3; i++){
                      //      GPIO_PORTN_DATA_R |= 0x02; // El led cambia de estado, a encendido
                        //    SysTick_Wait(2000000);//Funcion de retardo para pasar al siguiente estado del led, 0.5[s]
                          //  GPIO_PORTN_DATA_R &= ~0x02; // El led cambia de estado, a apagado
                            //SysTick_Wait(2000000);//Funcion de retardo para pasar al siguiente estado del led, 0.5[s]

             //           }
               //  }

                 //if (buttonPushCounter % 4 == 0) {
                   //  GPIO_PORTN_DATA_R=0x02;
                  //} else {
                  //    GPIO_PORTN_DATA_R=0x00;
                 // }






                 if (cuenta0 > 0  && cuenta0 < 20){
                                                      LCD_Set_Cursor(1, 1);
                                                              LCD_Write_String("Color:          ");
                                                              //LCD_Write_Char('a');
                                                              LCD_Set_Cursor(2, 1);
                                                              LCD_Write_String("0-20           ");


                                                  } else if (cuenta0 > 20  && cuenta0 < 40){
                                                      LCD_Set_Cursor(1, 1);
                                                              LCD_Write_String("Color:          ");
                                                              //LCD_Write_Char('a');
                                                              LCD_Set_Cursor(2, 1);
                                                              LCD_Write_String("20-40         ");


                                                  }else if (cuenta0 > 40  && cuenta0 < 60){
                                                      LCD_Set_Cursor(1, 1);
                                                              LCD_Write_String("Color:          ");
                                                              //LCD_Write_Char('a');
                                                              LCD_Set_Cursor(2, 1);
                                                              LCD_Write_String("40-60         ");


                                                  }else if (cuenta0 > 60  && cuenta0 < 80){
                                                      LCD_Set_Cursor(1, 1);
                                                              LCD_Write_String("Color:          ");
                                                              //LCD_Write_Char('a');
                                                              LCD_Set_Cursor(2, 1);
                                                              LCD_Write_String("80-100         ");


                                                  }else if (cuenta0 > 100  && cuenta0 < 120){
                                                      LCD_Set_Cursor(1, 1);
                                                              LCD_Write_String("Color:          ");
                                                              //LCD_Write_Char('a');
                                                              LCD_Set_Cursor(2, 1);
                                                              LCD_Write_String("120-140         ");


                                                  }else if (cuenta0 > 140  && cuenta0 < 160){
                                                      LCD_Set_Cursor(1, 1);
                                                              LCD_Write_String("Color:          ");
                                                              //LCD_Write_Char('a');
                                                              LCD_Set_Cursor(2, 1);
                                                              LCD_Write_String("140-160         ");


                                                  }else if (cuenta0 > 160  && cuenta0 < 180){
                                                      LCD_Set_Cursor(1, 1);
                                                              LCD_Write_String("Color:          ");
                                                              //LCD_Write_Char('a');
                                                              LCD_Set_Cursor(2, 1);
                                                              LCD_Write_String("160-180         ");


                                                  }else if (cuenta0 > 180  && cuenta0 < 200){
                                                      LCD_Set_Cursor(1, 1);
                                                              LCD_Write_String("Color:          ");
                                                              //LCD_Write_Char('a');
                                                              LCD_Set_Cursor(2, 1);
                                                              LCD_Write_String("180-200         ");


                                                  }
                                                  /*else if (cuenta0 > 800  && cuenta0 < 900){
                                                      LCD_Set_Cursor(1, 1);
                                                              LCD_Write_String("Color:          ");
                                                              //LCD_Write_Char('a');
                                                              LCD_Set_Cursor(2, 1);
                                                              LCD_Write_String("800-900         ");


                                                  }else if (cuenta0 > 900  && cuenta0 < 1000){
                                                      LCD_Set_Cursor(1, 1);
                                                              LCD_Write_String("Color:          ");
                                                              //LCD_Write_Char('a');
                                                              LCD_Set_Cursor(2, 1);
                                                              LCD_Write_String("900-1000         ");


                                                  }*/





                 /*if (cuenta0 < 1176 && cuenta1 > 1120 && cuenta2 > 1120){
                     LCD_Set_Cursor(1, 1);
                             LCD_Write_String("Color:          ");
                             //LCD_Write_Char('a');
                             LCD_Set_Cursor(2, 1);
                             LCD_Write_String("Rojo            ");


                 }
                  if (cuenta0 > 1120 && cuenta1 > 1400 && cuenta2 < 1120){
                      LCD_Set_Cursor(1, 1);
                                                  LCD_Write_String("Color:          ");
                                                  //LCD_Write_Char('a');
                                                  LCD_Set_Cursor(2, 1);
                                                  LCD_Write_String("Azul            ");
                  }
                  if (cuenta0 > 2590 && cuenta1 < 1960 && cuenta2 < 1820) {
                      LCD_Set_Cursor(1, 1);
                                                  LCD_Write_String("Color:          ");
                                                  //LCD_Write_Char('a');
                                                  LCD_Set_Cursor(2, 1);
                                                  LCD_Write_String("Verde          ");
                  }
                  if (cuenta0 < 490 && cuenta1 < 490 && cuenta2 < 490){
                      LCD_Set_Cursor(1, 1);
                                                  LCD_Write_String("Color:          ");
                                                  //LCD_Write_Char('a');
                                                  LCD_Set_Cursor(2, 1);
                                                  LCD_Write_String("Blanco         ");
                  }
                  if (cuenta0 > 1820 && cuenta1 > 2800 && cuenta2 >2100){
                      LCD_Set_Cursor(1, 1);
                                                  LCD_Write_String("Color:          ");
                                                  //LCD_Write_Char('a');
                                                  LCD_Set_Cursor(2, 1);
                                                  LCD_Write_String("Negro           ");
                  }*/

                  //SysTick_Wait(400000);// 200[ms]   RAPIDEZ GENERAL DEL PROGRAMA


                  //punto anterior limpia entrada digital sensor
                 //GPIO_PORTD_AHB_DATA_R==0x00;

             }//fin for infinito


         //fin inserta 2




         // habilita al Puerto L como salida digital para control de motor
         // PL0,...,PL3 como salidas hacia el ULN2003 (A,A�,B,B�)
         //GPIO_PORTL_DIR_R = 0x0F;
         //GPIO_PORTL_DEN_R = 0x0F;
         //GPIO_PORTL_DATA_R = 0x09;

         // habilita PN0 y PN1 como salida digital para monitoreo del programa
         //
        // GPIO_PORTN_DIR_R = 0x03;
         //GPIO_PORTN_DEN_R = 0x03;

         // habilita PF0 y PF4 como salida digital para monitoreo del programa
         //
         //GPIO_PORTF_AHB_DIR_R = 0x11;
         //GPIO_PORTF_AHB_DEN_R = 0x11;

         while(1);

}
