################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../tm4c1294ncpdt.cmd 

C_SRCS += \
../I2C_LCD.c \
../main.c \
../tm4c1294ncpdt_startup_ccs.c 

C_DEPS += \
./I2C_LCD.d \
./main.d \
./tm4c1294ncpdt_startup_ccs.d 

OBJS += \
./I2C_LCD.obj \
./main.obj \
./tm4c1294ncpdt_startup_ccs.obj 

OBJS__QUOTED += \
"I2C_LCD.obj" \
"main.obj" \
"tm4c1294ncpdt_startup_ccs.obj" 

C_DEPS__QUOTED += \
"I2C_LCD.d" \
"main.d" \
"tm4c1294ncpdt_startup_ccs.d" 

C_SRCS__QUOTED += \
"../I2C_LCD.c" \
"../main.c" \
"../tm4c1294ncpdt_startup_ccs.c" 


