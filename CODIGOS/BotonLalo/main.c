// ***** Librerias estandar del propio lenguaje ***** //
#include <stdbool.h> // Libreria para operaciones booleanas
#include <stdint.h> // Libreria estandar del tipo de datos

// ***** Libreria para manejar los puertos, direcciones y bits del micro ***** //
#include "inc/tm4c1294ncpdt.h"

// ***** Variables globales ***** //
uint32_t i;
uint32_t dato;

// DefiniciOn de constantes para operaciones
#define NVIC_ST_CTRL_COUNT      0x00010000  // bandera de cuenta flag
#define NVIC_ST_CTRL_CLK_SRC    0x00000004  // Clock Source
#define NVIC_ST_CTRL_INTEN      0x00000002  // Habilitador de interrupción
#define NVIC_ST_CTRL_ENABLE     0x00000001  // Modo del contador
#define NVIC_ST_RELOAD_M        0x00FFFFFF  // Valor de carga del contador

//**********FUNCION Inizializa el SysTick *********************
void SysTick_Init(void){
  NVIC_ST_CTRL_R = 0;                   // Desahabilita el SysTick durante la configuración
  NVIC_ST_RELOAD_R = NVIC_ST_RELOAD_M;  // Se establece el valor de cuenta deseado en RELOAD_R
  NVIC_ST_CURRENT_R = 0;                // Se escribe al registro current para limpiarlo

  NVIC_ST_CTRL_R = 0x00000001;         // Se Habilita el SysTick y se selecciona la fuente de reloj
}

//*************FUNCION Tiempo de retardo utilizando wait.***************
// El parametro de retardo esta en unidades del reloj interno/4 = 4 MHz (250 ns)
void SysTick_Wait(uint32_t retardo){
    NVIC_ST_RELOAD_R= retardo-1;   //nUmero de cuentas por esperar
    NVIC_ST_CURRENT_R = 0;
    while((NVIC_ST_CTRL_R&0x00010000)==0){//espera hasta que la bandera COUNT sea valida
    }
   } //

// ***** Inicia programa principal ***** //
int main(void){

    // ***** PrgoramaciOn de puertos ***** //
    SYSCTL_RCGCGPIO_R = 0x1100; // Habilita el reloj del puerto N y puerto J
    i = SYSCTL_RCGCGPIO_R; // Tiempo de retardo para estabilizar el reloj de los puertos

    GPIO_PORTN_DIR_R = 0X03; // Se configura el pin 0 y 1 del puerto N como salida
    GPIO_PORTN_DEN_R = 0X03; // Se configura el pin 0 y 1 del puerto N como digitales

    GPIO_PORTJ_AHB_DIR_R = 0; // Se configura el pin 0 y 1 del puerto J como entradas
    GPIO_PORTJ_AHB_DEN_R = 0x03; // Se configura el pin 0 y 1 del puerto J como digitales
    GPIO_PORTJ_AHB_PUR_R = 0x03; // Resistencia en modo Pull-Up

    GPIO_PORTN_DATA_R = 0x00; // Pin 0 y 1 del puerto N como apagados


    SysTick_Init();//inicializa servicio de interrupciones por reloj

    // ***** Comienza Loop Principal ***** //

    for(;;){


        //while((GPIO_PORTJ_AHB_DATA_R & 0X02)==0X02){} // En espara de que el push button sea presionado
        dato=GPIO_PORTJ_AHB_DATA_R;
        if(dato==2){
            // Al presionar el push button comienza secuencia de encedido y apagado del led
            for(i=0; i<5; i++){
                GPIO_PORTN_DATA_R |= 0x02; // El led cambia de estado, a encendido
                SysTick_Wait(2000000);//Funcion de retardo para pasar al siguiente estado del led, 0.5[s]
                GPIO_PORTN_DATA_R &= ~0x02; // El led cambia de estado, a apagado
                SysTick_Wait(2000000);//Funcion de retardo para pasar al siguiente estado del led, 0.5[s]

            }
        }

        else if(dato==1){
        //while((GPIO_PORTJ_AHB_DATA_R & 0X01)==0X01){} // En espara de que el push button sea presionado
            dato=GPIO_PORTJ_AHB_DATA_R;
               // Al presionar el push button comienza secuencia de encedido y apagado del led
               for(i=0; i<3; i++){
                   GPIO_PORTN_DATA_R |= 0x02; // El led cambia de estado, a encendido
                   SysTick_Wait(2000000);//Funcion de retardo para pasar al siguiente estado del led, 0.5[s]
                   GPIO_PORTN_DATA_R &= ~0x02; // El led cambia de estado, a apagado
                   SysTick_Wait(2000000);//Funcion de retardo para pasar al siguiente estado del led, 0.5[s]

               }
        }
        GPIO_PORTJ_AHB_DATA_R==0x00;

    }//fin for infinito
}
