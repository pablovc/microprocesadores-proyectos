
//igualar cadena de texto con un valor numerico
#define GPIO_PORTF_DATA_BITS_R 0x4005D000
//las direcciones son de 32 bits de ancho
//tipo de dato entero es 32 bits
#define GPIO_PORTF_DIR_R   (*(volatile unsigned int *)0x4005D400)
							//cast de apuntador entero no signado volatil (siempre acceso de lectura)
#define GPIO_PORTF_DEN_R   (*(volatile unsigned int *)0x4005D51C)
#define SYSCTL_RCGCGPIO_R  (*(volatile unsigned int *)0x400FE608)
#define SYSCTL_PRGPIO_R    (*(volatile unsigned int *)0x400FEA08)
#define PORTbit4 0x40
#define PORTbit0 0x04
#define PORTFbits (GPIO_PORTF_DATA_BITS_R|PORTbit4|PORTbit0)
#define PF40 (*(volatile unsigned int *)PORTFbits)   //#define PF21 (*(volatile uint32)0x4005D044)

//RELOJ DE LA TARJETA 16MHz

void main(void){
	// Habilita reloj en Puerto F
	SYSCTL_RCGCGPIO_R |= 0x20;      // (a)
    //BIT 5 -> 100000 -> 0x20
 int i=0;

	// Espera a que estE listo el puerto
	while((SYSCTL_PRGPIO_R & 0x20)==0);

	// Bits 4 y 0 del PTO F como salidas, los demas bits entradas
	//GPIO_PORTF_DIR_R |= 0xFF;	 	// (b)
	GPIO_PORTF_DIR_R |= 0x11;	 	// (b)

	//Operacion |= enciende a traves de un valor de mascara


	// Habilita el modo Digital de los pines 4 y 0
	//GPIO_PORTF_DEN_R |= 0xFF;           // (c)
	GPIO_PORTF_DIR_R |= 0x11;	 	// (b)

	// valor inicial de los pines (hay Leds conectados en la tarjeta)
	PF40 = 0x01;	// PF4 = 0, PF0 = 1

	while(1) {
		// insertar un BREAKPOINT EN LA SIG. LINEA Y EJECUTAR (TECLA F8)
		PF40 ^= 0x11;		// cambia el estado lOgico de los pines 4 y 0
		// insertar aquI un retardo
		for(i=0;i<16000000;i++){

		}
	}


}
