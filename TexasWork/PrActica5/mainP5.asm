
suma 	.equ 0
resta	.equ 1
mult	.equ 2
and	.equ 3
mor	.equ 4
xor	.equ 5
 
;...
		.data
		.retain

Op1		.word 1
Op2		.word 2
Res 	.word 0


         	  	.global main
                .text
ptrOp1	        .field Op1
ptrOp2          .field Op1
ptrRes          .field Res


main:		; MENU
		; prueba si es suma
		CMP R0, #suma
		BEQ sumaOp 	; si es suma, va a esa secciOn
		CMP R0,#resta 	; si no, prueba si es resta
		BEQ restaOp
		CMP R0,#mult 	; si no, prueba si es multiplicacion
		BEQ multOp
		CMP R0,#and 	; si no, prueba si es and
		BEQ andOp
		CMP R0,#mor 	; si no, prueba si es or
		BEQ orOp
		CMP R0,#xor 	; si no, prueba si es xor
		BEQ xorOp

    	        B main


sumaOp:
		BL cargaOp_func	; llama a subrutina para cargar dato
		ADDS R3,R1,R2	; escriba su cOdigo
				
		BL   guardaRes_func		; llama a subrutina para guardar resultado
		B main	; termina suma y regresa al inicio


restaOp:

		BL cargaOp_func	; llama a subrutina para cargar dato
		SUBS R3,R1,R2	; escriba su cOdigo
		BL   guardaRes_func		; llama a subrutina para guardar resultado
		B main  ; termina resta y regresa al inicio

multOp:

		BL cargaOp_func	; llama a subrutina para cargar dato
		MUL R3,R1,R2	; escriba su cOdigo
		BL   guardaRes_func		; llama a subrutina para guardar resultado
        B main	; termina multiplicacion y regresa al inicio

andOp:

		BL cargaOp_func	; llama a subrutina para cargar dato
		AND R3,R1,R2	; escriba su cOdigo
		BL   guardaRes_func		; llama a subrutina para guardar resultado
	    B main   ; termina AND y regresa al inicio

orOp:

		BL cargaOp_func	; llama a subrutina para cargar dato
		ORR R3,R1,R2	; escriba su cOdigo
		BL   guardaRes_func		; llama a subrutina para guardar resultado
		B main   ; termina OR y regresa al inicio

xorOp:


		BL cargaOp_func	; llama a subrutina para cargar dato
		EOR R3,R1,R2	; escriba su cOdigo
		BL   guardaRes_func		; llama a subrutina para guardar resultado
		B main   ; termina EOR y regresa al inicio


cargaOp_func:
		LDR R4, ptrOp1 ; cargar operandos en R1 y R2
		LDR R1, [R4]
		LDR R5, ptrOp2
		LDR R2, [R5]
		BX LR
		
				

guardaRes_func:
		LDR R6, ptrRes
		STR R3, [R6]	; guarda resultado en R3
		BX  LR


stop 		B stop
		.end









