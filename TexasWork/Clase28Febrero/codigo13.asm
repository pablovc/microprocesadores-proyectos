;** EL PROGRAMA INICIALIZA EL PUERTO N COMO SALIDA y
;** CONMUTA SU VALOR

 .global main

       .text                 ;define codigo del programa y lo ubica en flash

SYSCTL_RCGCGPIO_R  .field 0x400FE608,32    ; REGISTRO DEL RELOJ

GPIO_PORTN_DIR_N     .field 0x40064400,32    ; Registro de Dirección N
GPIO_PORTN_DEN_N     .field 0x4006451C,32    ; Registro de habilitación N
GPIO_PORTN_DATA_N    .field 0x40064004,32    ; Registro de Datos N

;GPIO_PORTN_DATA_N    .field 0x4006400C,32    ; Registro de Datos N
;C -> 8 y 4
;4 -> solo4

LEDS   .EQU 0x03     ; se asigna el valor 0x20 a el simbolo LEDS
LEDS2  .EQU 0X00     ; se asigna el valor 0x00 a el simbolo LEDS2


main


      LDR R1,SYSCTL_RCGCGPIO_R   ; 1) activar el reloj del puerto N
      LDR R0, [R1]
      ORR R0, R0, #0x1000        ; se valida el bit 12 para habilitar el reloj
      STR R0, [R1]
      NOP
      NOP                        ; se da tiempo para que el reloj se habilite


     LDR R1,GPIO_PORTN_DIR_N     ; Determina la dirección del puerto
      MOV R0, #0Xff              ; PN0 PN1 salidas
      STR R0,[R1]

      LDR R1,GPIO_PORTN_DEN_N    ; Habilita al puerto digital N
      MOV R0, #0XFF
      STR R0,[R1]


;R5=0x7A1200


      LDR R1,GPIO_PORTN_DATA_N   ; apunta al Puerto de datos N
      AND R0,#LEDS
salto STR R0,[R1]                ; Escribe en el registro del datos del Puerto N
      MOV R2, #LEDS2
      STR R2,[R1]
      B salto
; -----------------


      .end
