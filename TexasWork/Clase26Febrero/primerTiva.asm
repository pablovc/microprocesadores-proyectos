 .global main
 ;      .global coco
       .asg "main", coco    ; se asigna un nuevo nombre a main
 ;      .thumb                  ; se usara instrucciones thumb
       .data                    ; define variables globales

       .text                 ;define código del programa y lo ubica en flash

APUNTADOR  .field 0x20000000,32  ; se definen un objeto de 32 bits y se inicializa como apuntador en 0x20000000
BIT5   .equ 0x20                 ; se asigna el valor 0x20 a el simbolo BIT5 Se ocupa 20 cero representado 001000000 en binario
AUX_R1 .equ R1
APUNT1 .equ 0X20000010

coco
;main

    MOV R0,#BIT5

     LDR R1,APUNTADOR    ; carga EN R1 el valor indicado por el apuntador

     STR R0,[R1,#4]      ;el valor de 32 bits de R0 se almacena  en la dirección R1+4

  ; -----------------
      MOV R4,#0XFF
;     AND R4,R4,APUNTADOR   ; operación no valida
      AND R4,R4,#BIT5



      LDR R2,[AUX_R1]

      STR R2,[R1,#BIT5]

      MOV AUX_R1, #56h

FS    NOP
      B FS
      .end

