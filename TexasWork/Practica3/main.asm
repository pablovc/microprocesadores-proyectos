.global main

main


      MOV R1,#0X9999      ;mover #0x9999 a R1

      LSR R2, R1, #9  ;R2 = R1 >> 9 (similar a R2 = R1/512)

      MOV R0,#0xAAAA      ;mover #0xAAAA a R0

      LSRS R3, R0, #3  ;R3 = R0 >> 3 (similar a R3 = R0/8) con bandera actualizada

      MOV R5,#0xAAAA      ;mover #0xAAAA a R0
      MOV R6,#4           ;mover #4 a R6

      LSR R4, R5, R6   ;R4 = R5 >> R6 (similar a R4 = R5/(2**R6))

     ;DESPLAZAMIENTO ARITMETICO

      MOV R1,#0X9999      ;mover #0x9999 a R1

      ASR R2, R1, #9   ;R2 = R1 >> 9, signado,  (similar a R2 = R1/512)

      MOV R0,#0xAAAA      ;mover #0xAAAA a R0

      ASRS R3, R0, #3  ;R3 = R0 >> 3, signado, con bandera actualizada

      MOV R5,#0xAAAAAAAA      ;mover #0xAAAA a R0
      MOV R6,#4           ;mover #4 a R6

      ASR R4, R5, R6   ;R4 = R5 >> R6, signado,  (similar a R4 = R5/(2**R6))

desp  B  desp
      .end
