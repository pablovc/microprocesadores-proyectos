// Ejercicio de Interrupcion con GPIO
//Interrupción por flanco de bajada en PJ0 = SW1
//Interrupción por flanco de bajada em PE1= SW2
// La Interrupción PJ0 enciende de manera intermitente (1Hz) D1-D2  por 10 seg
// La Interrupción PE0- enciende de manera intermitente (5Hz) D1-D2 por 3 seg
// PE0 tiene la más alta prioridad
#include <stdbool.h>// Librería para operaciones lógicas
#include <stdint.h>

#define SYSCTL_RCGCGPIO_R       (*((volatile uint32_t *)0x400FE608))
#define SYSCTL_PRGPIO_R         (*((volatile unsigned long *)0x400FEA08))
#define GPIO_PORTJ_DIR_R        (*((volatile uint32_t *)0x40060400)) //Registro de Dirección PJ
#define GPIO_PORTJ_DEN_R        (*((volatile uint32_t *)0x4006051C)) //Registro de habilitación PJ
#define GPIO_PORTJ_PUR_R        (*((volatile uint32_t *)0x40060510)) //Registro de pull-up PJ
#define GPIO_PORTJ_DATA_R       (*((volatile uint32_t *)0x40060004)) //Registro de Datos J
#define GPIO_PORTJ_IS_R         (*((volatile uint32_t *)0x40060404)) //Registro de configuración de detección de nivel o flanco
#define GPIO_PORTJ_IBE_R        (*((volatile uint32_t *)0x40060408)) //Registro de configuración de interrupción por ambos flancos
#define GPIO_PORTJ_IEV_R        (*((volatile uint32_t *)0x4006040C)) //Registro de configuración de interrupción por un flanco
#define GPIO_PORTJ_ICR_R        (*((volatile uint32_t *)0x4006041C)) //Registro de limpieza de interrupcion de flanco en PJ
#define GPIO_PORTJ_IM_R         (*((volatile uint32_t *)0x40060410)) //Registro de mascara de interrupcion PJ
#define NVIC_EN1_R              (*((volatile uint32_t *)0xE000E104)) // Registro de habilitación de interrupción PJ
#define NVIC_PRI12_R             (*((volatile uint32_t *)0xE000E430))//Registro de prioridad de interrupción

#define GPIO_PORTE_DIR_R        (*((volatile uint32_t *)0x4005C400)) //Registro de Dirección PJ
#define GPIO_PORTE_DEN_R        (*((volatile uint32_t *)0x4005C51C)) //Registro de habilitación PJ
#define GPIO_PORTE_PUR_R        (*((volatile uint32_t *)0x4005C510)) //Registro de pull-up PJ
#define GPIO_PORTE_DATA_R       (*((volatile uint32_t *)0x4005C004)) //Registro de Datos J
#define GPIO_PORTE_IS_R         (*((volatile uint32_t *)0x4005C404)) //Registro de configuración de detección de nivel o flanco
#define GPIO_PORTE_IBE_R        (*((volatile uint32_t *)0x4005C408)) //Registro de configuración de interrupción por ambos flancos
#define GPIO_PORTE_IEV_R        (*((volatile uint32_t *)0x4005C40C)) //Registro de configuración de interrupción por un flanco
#define GPIO_PORTE_ICR_R        (*((volatile uint32_t *)0x4005C41C)) //Registro de limpieza de interrupcion de flanco en PJ
#define GPIO_PORTE_IM_R         (*((volatile uint32_t *)0x4005C410)) //Registro de mascara de interrupcion PJ
#define NVIC_EN0_R              (*((volatile uint32_t *)0xE000E100)) // Registro de habilitación de interrupción PJ
#define NVIC_PRI1_R             (*((volatile uint32_t *)0xE000E404))//Registro de prioridad de interrupción

#define GPIO_PORTN_DATA_R       (*((volatile unsigned long *)0x4006400C)) // Registro de Datos Puerto N
#define GPIO_PORTN_DIR_R        (*((volatile unsigned long *)0x40064400)) // Registro de Dirección Puerto N
#define GPIO_PORTN_DEN_R        (*((volatile unsigned long *)0x4006451C)) // Registro de Habilitación Puerto N

// Definición de apuntadores para direccionar
#define NVIC_ST_CTRL_R          (*((volatile unsigned long *)0xE000E010))  //apuntador del registro que permite configurar las acciones del temporizador
#define NVIC_ST_RELOAD_R        (*((volatile unsigned long *)0xE000E014))  //apuntador del registro que contiene el valor de inicio del contador
#define NVIC_ST_CURRENT_R       (*((volatile unsigned long *)0xE000E018))  //apuntador del registro que presenta el estado de la cuenta actual

// Definición de constantes para operaciones
#define NVIC_ST_CTRL_COUNT      0x00010000  // bandera de cuenta flag
#define NVIC_ST_CTRL_CLK_SRC    0x00000004  // Clock Source
#define NVIC_ST_CTRL_INTEN      0x00000002  // Habilitador de interrupción
#define NVIC_ST_CTRL_ENABLE     0x00000001  // Modo del contador
#define NVIC_ST_RELOAD_M        0x00FFFFFF  // Valor de carga del contador



volatile uint32_t Flancosdebajada = 0;


//******* Función de inicialización de Puertos J,E,N *******
//******* e inialización de interrupciones de J y E *******
void GPIO_Init(void){
  SYSCTL_RCGCGPIO_R |= 0x00001110; // (a) activa el reloj para el puerto J,E,N
//  Flancosdebajada = 0;                // (b) inicializa el contador
//******* Puerto J *******
  GPIO_PORTJ_DIR_R &= ~0x01;       // (c) PJ0 dirección entrada - boton SW1
  GPIO_PORTJ_DEN_R |= 0x01;        //     PJ0 se habilita
  GPIO_PORTJ_PUR_R |= 0x01;        //     habilita weak pull-up on PJ1
  GPIO_PORTJ_IS_R &= ~0x01;        // (d) PJ1 es sensible por flanco
  GPIO_PORTJ_IBE_R &= ~0x01;       //     PJ1 no es sensible a dos flancos
  GPIO_PORTJ_IEV_R &= ~0x01;       //     PJ1 detecta eventos de flanco de bajada
  GPIO_PORTJ_ICR_R = 0x01;         // (e) limpia la bandera 0
  GPIO_PORTJ_IM_R |= 0x01;         // (f) Se desenmascara la interrupcion PJ0 y se envia al controlador de interrupciones
  NVIC_PRI12_R = (NVIC_PRI12_R&0x00FFFFFF)|0x00000000; // (g) prioridad 0  (pag 159)
  NVIC_EN1_R= 1<<(51-32);          //(h) habilita la interrupción 51 en NVIC (Pag. 154)
//******* Puerto E *******
  GPIO_PORTE_DIR_R &= ~0x01;       // (c) PE0 dirección entrada - boton SW1
  GPIO_PORTE_DEN_R |= 0x01;        //     PE0 se habilita
  GPIO_PORTE_PUR_R |= 0x01;        //     habilita weak pull-up on PJ1
  GPIO_PORTE_IS_R &= ~0x01;        // (d) PE0 es sensible por flanco
  GPIO_PORTE_IBE_R &= ~0x01;       //     PE0 no es sensible a dos flancos
  GPIO_PORTE_IEV_R &= ~0x01;       //     PE0 detecta eventos de flanco de bajada
  GPIO_PORTE_ICR_R = 0x01;         // (e) limpia la bandera 0
  GPIO_PORTE_IM_R |= 0x01;         // (f) Se desenmascara la interrupcion PJ0 y se envia al controlador de interrupciones
  NVIC_PRI1_R = (NVIC_PRI1_R&0xFFFFFF00)|0x00000020; // (g) prioridad 1  (pag 159)
  NVIC_EN0_R= 1<<(4-0);          //(h) habilita la interrupción 4 en NVIC (Pag. 154)
//******* Puerto N *******
  GPIO_PORTN_DIR_R |= 0x0F;    // puerto N de salida
  GPIO_PORTN_DEN_R |= 0x0F;    // puerto N habilitado

  GPIO_PORTN_DATA_R = 0x02;
}


//**********FUNCION Inizializa el SysTick *********************
  void SysTick_Init(void){
    NVIC_ST_CTRL_R = 0;                   // Desahabilita el SysTick durante la configuración
    NVIC_ST_RELOAD_R = NVIC_ST_RELOAD_M;  // Se establece el valor de cuenta deseado en RELOAD_R
    NVIC_ST_CURRENT_R = 0;                // Se escribe al registro current para limpiarlo
    NVIC_ST_CTRL_R = 0x00000001;         // Se Habilita el SysTick y se selecciona la fuente de reloj
  }

  //*************FUNCION Tiempo de retardo utilizando wait.***************
  // El parametro de retardo esta en unidades del reloj interno/4 = 4 MHz (250 ns)
  void SysTick_Wait(uint32_t retardo){
      NVIC_ST_RELOAD_R= retardo-1;   //número de cuentas por esperar
      NVIC_ST_CURRENT_R = 0;
      while((NVIC_ST_CTRL_R&0x00010000)==0){//espera hasta que la bandera COUNT sea valida
      }
     } //

//******* Rutina de Servicio de Interrupción Puerto J*******
 void GPIOPortJ_Handler(void)
 {
  GPIO_PORTJ_ICR_R = 0x01;      // bandera0 de confirmación
  // Tiempo de retardo utilizando wait igual a 3 s
      int i=0;
      for(i=0; i<5; i++){
      SysTick_Wait(800000);  // Espera 200 ms (asume reloj de  4 MHz)
      GPIO_PORTN_DATA_R = GPIO_PORTN_DATA_R^0x03; // conmuta encendido de leds
}
    }

//******* Rutina de Servicio de Interrupción Puerto E *******
 void GPIOPortE_Handler(void)
{
     GPIO_PORTE_ICR_R = 0x01;      // bandera0 de confirmación
     // Tiempo de retardo utilizando wait igual a 10 s
         int i=0;
         for(i=0; i<10; i++){
         SysTick_Wait(4000000);  // Espera 1s (asume reloj de  4 MHz)
         GPIO_PORTN_DATA_R = GPIO_PORTN_DATA_R^0x03; // conmuta encendido de leds
}
  }

//Programa principal
int main(void){
    SysTick_Init(); // Inicializa SysTick
    GPIO_Init();  // Inicializa puertos e interrupciones en J y E

  while(1);
}

