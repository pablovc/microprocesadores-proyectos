################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../tm4c1294ncpdt.cmd 

ASM_SRCS += \
../blink.asm \
../mainV2P4.asm \
../simpleIO.asm 

C_SRCS += \
../tm4c1294ncpdt_startup_ccs.c 

S_SRCS += \
../gpio_reg.s \
../macros.s 

OBJS += \
./blink.obj \
./gpio_reg.obj \
./macros.obj \
./mainV2P4.obj \
./simpleIO.obj \
./tm4c1294ncpdt_startup_ccs.obj 

S_DEPS += \
./gpio_reg.d \
./macros.d 

ASM_DEPS += \
./blink.d \
./mainV2P4.d \
./simpleIO.d 

C_DEPS += \
./tm4c1294ncpdt_startup_ccs.d 

C_DEPS__QUOTED += \
"tm4c1294ncpdt_startup_ccs.d" 

S_DEPS__QUOTED += \
"gpio_reg.d" \
"macros.d" 

OBJS__QUOTED += \
"blink.obj" \
"gpio_reg.obj" \
"macros.obj" \
"mainV2P4.obj" \
"simpleIO.obj" \
"tm4c1294ncpdt_startup_ccs.obj" 

ASM_DEPS__QUOTED += \
"blink.d" \
"mainV2P4.d" \
"simpleIO.d" 

ASM_SRCS__QUOTED += \
"../blink.asm" \
"../mainV2P4.asm" \
"../simpleIO.asm" 

S_SRCS__QUOTED += \
"../gpio_reg.s" \
"../macros.s" 

C_SRCS__QUOTED += \
"../tm4c1294ncpdt_startup_ccs.c" 


