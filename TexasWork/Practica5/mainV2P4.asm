N     .equ   10            ; variables no inicializadas
;N     .equ   100            ; variables no inicializadas

R     .equ   5
S     .equ   10
F     .equ   5


      .bss   lista, N       ; reserva 100 localidades de 8 bits para lista
      .bss   data, N        ; reserva 100 localidades de 8 bits para data

      .bss   listaR, R       ; reserva 10 localidades de 8 bits para lista
      .bss   dataR, R        ; reserva 10 localidades de 8 bits para data

      .bss   listaS, S       ; reserva 10 localidades de 8 bits para lista
      .bss   dataS, S        ; reserva 10 localidades de 8 bits para data

      .bss   listaF, F       ; reserva 10 localidades de 8 bits para lista
      .bss   dataF, F        ; reserva 10 localidades de 8 bits para data


      .global main
      .text                 ; programa principal
                            ; estos punteros son constantes colocados en ROM
ptLista .field lista, 32    ; ptXXXX = dirección de 32 bits
ptData  .field data, 32

ptListaR .field listaR, 32    ; ptXXXX = dirección de 32 bits
ptDataR  .field dataR, 32

ptListaS .field listaS, 32    ; ptXXXX = dirección de 32 bits
ptDataS  .field dataS, 32

                            ; inicio del programa
main:

; Llenar las 100 localidades de 'lista' con la secuencia 0-99
; usar R2 como la fuente del dato a guardar
; escribir su código aqui

        MOVW R1, #N
        MOVW R2, #0
        LDR  R4, ptLista   ; R4 apunta a inicio de lista

;#####LOOP DE PRUEBA####

loop1   STRB R2, [R4]      ; guarda R2 en direcciOn apuntada por R4
       ADDS R2, #1        ; incrementa valor
       ADDS R4, #1        ; incrementa apuntador
       SUBS R1, #1        ; siguiente elemento
       BNE loop1

  ADDS R2, #1        ; incrementa valor

;#### Reinicio de loop ####

        MOVW R5, #F
        MOVW R3, #0
        LDR  R6, ptListaR   ; R4 apunta a inicio de lista

;#####loop fibinacci

loop2   STRB R3, [R4]      ; guarda R2 en direcciOn apuntada por R4
       ADDS R3, #1        ; incrementa valor
       ADDS R6, #1        ; incrementa apuntador
       SUBS R5, #1        ; siguiente elemento
       BNE loop2

       ADDS R3, R3, R2

;     MOVW R1, #N
;     MOVW R2, #0

;    LDR   R4, ptLista       ; R4 apunta a inicio de lista
;    STR   R2, [R4]          ; guarda R2 en dirección apuntada por R4
;________________________________________________________________________________

;sumas parciales
   ;  LDR R3, ptData         ; R3 apunta a inicio de data
   ;  STR R2, [R3]           ; guarda R2 en dirección apuntada por R3

; LLenar las 100 localidades de ‘data’ con la suma parcial de las localidades
; usar R2 como la fuente del dato a guardar
; escribir su código aqui
stop  B  stop
