; CÓDIGO 15

;  SysTick
;  Configuración del SisTick


  .global main

  .thumb               ; se usara instrucciones thumb
;  .data               ; define variables globales

       .text           ;define codigo del programa y lo ubica en flash
;*** REGISTROS DE SYSTICK  *****
NVIC_ST_CTRL_R        .field 0xE000E010, 32 ;(p.150)apuntador del registro que permite configurar las acciones del temporizador
NVIC_ST_RELOAD_R      .field 0xE000E014, 32 ;(p.152)apuntador del registro que contiene el valor de inicio del contador
NVIC_ST_CURRENT_R     .field 0xE000E018, 32 ;(p.153) apuntador del registro que presenta el estado de la cuenta actual

;*** REGISTROS PARA CONFIGURAR PUERTO N ***

SYSCTL_RCGCGPIO_R  .field 0x400FE608,32    ; REGISTRO DEL RELOJ

GPIO_PORTN_DIR_N     .field 0x40064400,32    ; Registro de Dirección N (p.760)
GPIO_PORTN_DEN_N     .field 0x4006451C,32    ; Registro de habilitación N (p.781)
GPIO_PORTN_DATA_N    .field 0x4006400C,32    ; Registro de Datos N (p. 759 + direccionamiento de cada bit)

;**** CONSTANTES PARA COMPARARCARGAR O  EN REGISTROS
NVIC_ST_RELOAD_M      .field 0x00FFFFFF,32     ; Valor a cargar en el contador
;NVIC_ST_RELOAD_M      .field 0x01707840,32     ; Valor a cargar en el contador


main

;---------------------------------------------------------------
;                    CONFIGURACION PUERTO N - Para la aplicaión del programa
;---------------------------------------------------------------
      LDR R1,SYSCTL_RCGCGPIO_R      ; 1) activar el reloj del puerto N
      LDR R0, [R1]
      ORR R0, R0, #0x1000           ; se valida el bit 12 para habilitar el reloj N
      STR R0, [R1]
      NOP
      NOP                           ; se da tiempo para que el reloj se habilite

      LDR R1,GPIO_PORTN_DIR_N       ; Configura la dirección del registro
      MOV R0, #0X03                ; PN0 PN1 salidas
      STR R0,[R1]

      LDR R1,GPIO_PORTN_DEN_N       ; Habilita al puerto digital N
      MOV R0, #0XFF                 ; 1 significa que habilita E/S
      STR R0,[R1]


;------------ Inizialización del SysTick------------

; Desahabilita el SysTick durante la configuración
    LDR R1, NVIC_ST_CTRL_R         ; R1 = &NVIC_ST_CTRL_R (p.150)
    MOV R0, #0                     ; R0 = 0
    STR R0, [R1]                   ; [R1] = R0 = 0

; Se establece el valor de cuenta deseado en RELOAD_R
    LDR R1,  NVIC_ST_RELOAD_R      ; R1 = &NVIC_ST_RELOAD_R (p.152)
    LDR R0, NVIC_ST_RELOAD_M;      ; R0 = NVIC_ST_RELOAD_M
    STR R0, [R1]                   ; [R1] = R0 = NVIC_ST_RELOAD_M

;Se escribe al registro current para limpiarlo
    LDR R1, NVIC_ST_CURRENT_R      ; R1 = &NVIC_ST_CURRENT_R (p.153)
    MOV R0, #0                     ; R0 = 0
    STR R0, [R1]                   ; [R1] = R0 = 0

; Se Habilita el SysTick y se selecciona la fuente de reloj
    LDR R1, NVIC_ST_CTRL_R         ; R1 = &NVIC_ST_CTRL_R (p.150)

    MOV R0, #0x01                 ; R0 = ENABLE and CLK_SRC= reloj interno/4 = 4 MHz
;    MOV R0, #0x05                  ; R0 = ENABLE and CLK_SRC =  bits set
    STR R0, [R1]                   ; habilita bits ENABLE y CLK_SCR

; Conmutación en el puerto N
      MOV R6, #0X03
led   MVN  R5, R6
      MOV R6, R5
      LDR R1,GPIO_PORTN_DATA_N    ; apunta al Puerto de datos N

      STR R5,[R1]                 ; Escribe en el registro del datos del Puerto N el valor de J

;------------Retardo con SysTick------------

    LDR R1, NVIC_ST_CTRL_R
SysTick_Wait_loop
    LDR R3, [R1]                    ; R3 = &NVIC_ST_CTRL_R
    ANDS R3, R3, #0X00010000        ;la bandera COUNT es válida?
    BEQ SysTick_Wait_loop           ; Si no es válida regresa a SysTick_Wait_loop

    B led


    .end
