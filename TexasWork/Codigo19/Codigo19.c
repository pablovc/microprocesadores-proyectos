//CODIGO 19


//############################################INTERRUPCIONES###############################################

//1. Se debe habilitar la fuente de interrupcion de un dispositivo

//para activar una fuente de iterrupcion se requier
//1 Fijar su prioridad en NVIC (Control de interrupciones Vectorizado Anidado)
//2 Habilitar se fuente en el NVIC

//.-Para habilitar la prioridad se utilizan los registros de prioridad del NVIC
//.-Cada registro contiene campos de 8 bits de prioridad para cuatro dispositivos
//.-En los microcointroladres TM4, solo los 3 bits superiores del campo se utilizan
//.-Esto nos permite especificar el nivel de prioridad desde 0 hasta 7, donde 0 es la maxima prioridad

//Hay 5 registros de habilitacion NVIC_EN0_R al NVIC_EN4_R
//Los 32 bits en el registro NVIC_EN0_R controlan los numeros IRQ 0 al 31 (numeros de interrupcion 16-47)

//Despues del reset, todas las interrupciones estan deshabilitadas y tienen un valor de nivel de prioridad igual a 0. Antes de uti
//lizar cualquier interrupcion se ncesita:

//Establecer el nivel de prioridad de la interrupcion requerida (este es un paso opcional)

//HABILITAR

//Despues de dar prioridad y habilitar la interupcion en el NVIC es necesario configurar la tabla vectores de interrupcion:

//Es necesario indicar la direcion de inicio de la rutina de servicio de interrupcion ISR

//El nombre de la ISR  se puede encontrar dentro de la tabla de vectores dentro del condigo "startup", el cual es provisto por el fabricante del microcontrolador.

//El nombre del ISR  necesita coincidir con el nombre utilizado en la tabla de vectores para que el enlazador pueda poner correctamente la direccion de inicio
// de la ISR dentro de la tabla de vectores

//Una vez que la interrupcion tenga configurada su prioridad, este habilitada (en el ENVIC y en el periferico)

//Cuando el procesador Cortex-M acepta una peticion excepcion-iterrupcion:

//a) El procesador necesita determinar la direccion de inicio del manejador

//2.La tabla de vectores esta normalmnte definida en el codigo "startup" proporcionado por el fabricante del mocrocontrolador.

//3. La tabla de vectores utilizada en "startup" tambien contiene el valor inicial

//Los vectores se dfinene en el archivo tm4c1294ncpdf_startup_css.c

//Interrupciones de 16 en adelante

//Pagina  116 (Manual 1800 paginas) LISTA DE INTERRUPCIONES


//UNA interrupcion causa la siguiente secuencia de 5 eventos :

//1. La instruccion acual se finaliza

//2. La ejecucion del programa que se esta corriendo se suspende se ponen 8 registros en el stack (R0,R1,R2,R3,R12,LR,PC y PSR con R0 arriba),

//ISR (RUTINA DE SERVICIO DE INTERRUPCIONES)

//#####################FIN APUNTE#############################

 // Interrupción por flanco de bajada en PJ0 = SW1
// La Rutina de Servicio de Interrupción incrementa a un  contador.


#include <stdint.h>

#define SYSCTL_RCGCGPIO_R       (*((volatile uint32_t *)0x400FE608))
#define SYSCTL_PRGPIO_R         (*((volatile unsigned long *)0x400FEA08))
#define GPIO_PORTJ_DIR_R        (*((volatile uint32_t *)0x40060400)) //Registro de Dirección PJ
#define GPIO_PORTJ_DEN_R        (*((volatile uint32_t *)0x4006051C)) //Registro de habilitación PJ
#define GPIO_PORTJ_PUR_R        (*((volatile uint32_t *)0x40060510)) //Registro de pull-up PJ
#define GPIO_PORTJ_DATA_R       (*((volatile uint32_t *)0x40060004)) //Registro de Datos J
#define GPIO_PORTJ_IS_R         (*((volatile uint32_t *)0x40060404)) //Registro de configuración de detección de nivel o flanco
#define GPIO_PORTJ_IBE_R        (*((volatile uint32_t *)0x40060408)) //Registro de configuración de interrupción por ambos flancos
#define GPIO_PORTJ_IEV_R        (*((volatile uint32_t *)0x4006040C)) //Registro de configuración de interrupción por un flanco
#define GPIO_PORTJ_ICR_R        (*((volatile uint32_t *)0x4006041C)) //Registro de limpieza de interrupcion de flanco en PJ
#define GPIO_PORTJ_IM_R         (*((volatile uint32_t *)0x40060410)) //Registro de mascara de interrupcion PJ (p.764)
#define NVIC_EN1_R              (*((volatile uint32_t *)0xE000E104)) // Registro de habilitación de interrupción PJ
#define NVIC_PRI12_R             (*((volatile uint32_t *)0xE000E430))//Registro de prioridad de interrupción

int i;

// Incrementa la variable una vez cada que se presiona SW1 -Interrupcion PJ => #51 (p. 115)
volatile uint32_t Flancosdebajada = 0;
void EdgeCounter_Init(void){
  SYSCTL_RCGCGPIO_R |= 0x00000100; // (a) activa el reloj para el puerto J
  Flancosdebajada = 0;                // (b) inicializa el contador
  //GPIO_PORTJ_DIR_R &= ~0x01;       // (c) PJ0 dirección entrada - boton SW1
    GPIO_PORTJ_DIR_R &= ~0x03;
  //GPIO_PORTJ_DEN_R |= 0x01;        //     PJ0 se habilita
    GPIO_PORTJ_DEN_R |= 0x03;
  //GPIO_PORTJ_PUR_R |= 0x01;        //     habilita weak pull-up on PJ1
    GPIO_PORTJ_PUR_R |= 0x03;
  //GPIO_PORTJ_IS_R &= ~0x01;        // (d) PJ1 es sensible por flanco (p.761)
    GPIO_PORTJ_IS_R &= ~0x03;
  //GPIO_PORTJ_IBE_R &= ~0x01;       //     PJ1 no es sensible a dos flancos (p. 762)
    GPIO_PORTJ_IBE_R &= ~0x03;
  //GPIO_PORTJ_IEV_R &= ~0x01;       //     PJ1 detecta eventos de flanco de bajada (p.763)
    GPIO_PORTJ_IEV_R &= ~0x03;
  //GPIO_PORTJ_ICR_R = 0x01;         // (e) limpia la bandera 0 (p.769)
    GPIO_PORTJ_ICR_R = 0x03;
  //GPIO_PORTJ_IM_R |= 0x01;         // (f) Se desenmascara la interrupcion PJ0 y se envia al controlador de interrupciones (p.764)
    GPIO_PORTJ_IM_R |= 0x03;
    //Limpiando el byte mas alto
  NVIC_PRI12_R = (NVIC_PRI12_R&0x00FFFFFF)|0x00000000; // (g) prioridad 0  (p. 159)
  NVIC_EN1_R= 1<<(51-32);          //(h) habilita la interrupción 51 en NVIC (p. 154)

}

void GPIOPortJ_Handler(void)
{
  GPIO_PORTJ_ICR_R = 0x01;      // bandera0 de confirmación
  Flancosdebajada = Flancosdebajada + 1;
  for(i=0;i<8000;i++){

  }
 }


//Programa principal
int main(void){
  EdgeCounter_Init();           // inicializa la interrupción en el puerto GPIO J
  while(1);
}
